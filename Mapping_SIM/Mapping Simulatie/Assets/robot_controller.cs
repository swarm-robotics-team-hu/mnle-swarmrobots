﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class robot_controller : MonoBehaviour
{

    public GameObject sensorObject;
    public GameObject pointMarker;
    public GameObject positionMarker;
    public float RaycastDistance = 3.0f;
    public string robotName = "Robot";
    public float movementSpeed = 2.0f;
    public float rotationSpeed = 90.0f;

    public enum distanceVariationType { None, Distance/*, DistanceAndRotation*/};
    public enum positionVariationType { None, Random, System};
    //public enum rotationVariationType { None, Random, System};
    public distanceVariationType distanceVariation = distanceVariationType.None;
    //public rotationVariationType rotationVariation = rotationVariationType.None;
    public int percentSensorVariation = 1;
    //public int rotationSensorVariation = 3;
    public positionVariationType positionVariation = positionVariationType.None;
    public int mmRandomPositionVariation = 200;
    public float accelSystemPositionVariation = 1;
    public float gyroSystemVariation = 1;



    private Rigidbody2D rigi;

    private int outputTeller = 1;
    private float worldRotation;

    void Start()
    {
        rigi = GetComponent<Rigidbody2D>();
        accelerationVariation = new Vector2(Random.Range(-accelSystemPositionVariation,accelSystemPositionVariation),Random.Range(-accelSystemPositionVariation,accelSystemPositionVariation));
        rotationSpeedVariation = Random.Range(-gyroSystemVariation,gyroSystemVariation);
    }

    void Update()
    {
        if (Input.GetButtonDown("Scan"))
        {
            ScanRoom();
        }
    }

    // Update is called once per frame
    private Vector2 previousVelocity = new Vector2(0,0);
    private Vector2 newVelocity = new Vector2(0,0);
    private Vector2 robotPosition = new Vector2(0,0);
    private Vector2 accelerationVariation;
    private float rotationSpeedVariation;
    private float previousRotation = 0;
    private float robotRotation = 0;
    void FixedUpdate()
    {
        float rotateTank = Input.GetAxis("Horizontal");
        float moveTank = Input.GetAxis("Vertical");

        rigi.velocity = new Vector2(transform.up.x,transform.up.y) * moveTank * movementSpeed;
        float tempRotateSpeed = rotationSpeed * rotateTank;
        transform.Rotate(new Vector3(0,0,-1) * tempRotateSpeed * Time.fixedDeltaTime);

        if(positionVariation == positionVariationType.System)
        {
            previousVelocity = newVelocity;
            previousRotation = robotRotation;

            newVelocity = transform.InverseTransformDirection(rigi.velocity); //get new speed

            float rotationSpeed = tempRotateSpeed + rotationSpeedVariation;
            Vector2 acceleration = (newVelocity-previousVelocity) / Time.fixedDeltaTime + accelerationVariation; //get inaccurate accel from speed
            
            robotRotation = previousRotation + rotationSpeed*Time.fixedDeltaTime;
            newVelocity = previousVelocity + acceleration*Time.fixedDeltaTime; //change new speed with accel error

            robotPosition = new Vector2(
            robotPosition.x + Mathf.Sin(robotRotation/180*Mathf.PI)*(newVelocity.y*Time.fixedDeltaTime),
            robotPosition.y + Mathf.Cos(robotRotation/180*Mathf.PI)*(newVelocity.y*Time.fixedDeltaTime));
        }
    }

    string outputPath= "Assets/Output/output_";

    void ScanRoom()
    {
        StreamWriter writer = new StreamWriter(outputPath + outputTeller.ToString() + ".csv", true);
        Debug.Log("Aantal Files: "+outputTeller.ToString());
        outputTeller++;

        writer.WriteLine(robotName);

        if(positionVariation == positionVariationType.Random)
            robotPosition = new Vector2(transform.position.x + Random.Range(-mmRandomPositionVariation,mmRandomPositionVariation)/1000.0f, transform.position.y + Random.Range(-mmRandomPositionVariation,mmRandomPositionVariation)/1000.0f);
        else if(positionVariation == positionVariationType.None)
            robotPosition = new Vector2(transform.position.x, transform.position.y);


        writer.WriteLine(robotPosition.x);
        writer.WriteLine(robotPosition.y);

        Instantiate(positionMarker, new Vector3(robotPosition.x,robotPosition.y,-1), Quaternion.identity);

        if(positionVariation == positionVariationType.System)
            worldRotation = Mathf.Round((robotRotation)*100f)/100f;
        else
            worldRotation = Mathf.Round((-transform.eulerAngles.z)*100f)/100f;
            if (worldRotation < 0) worldRotation += 360;

        writer.WriteLine(worldRotation);

        for (int i = 0; i < 360; i++)
        {
            writer.WriteLine(GetDistance(i));
            sensorObject.transform.Rotate(new Vector3(0, 0, -1));
        }

        writer.Close();
    }


    float GetDistance(int sensorRotation)
    {
        float localWorldRotation = worldRotation + sensorRotation;
        RaycastHit2D hitInfo = Physics2D.Raycast(sensorObject.transform.position, sensorObject.transform.up, RaycastDistance);
        if(hitInfo)
        {

            float distanceAbsolute = Vector3.Distance(hitInfo.point, sensorObject.transform.position);
            if(distanceVariation != distanceVariationType.None)
            {
                distanceAbsolute *= (1+Random.Range(-percentSensorVariation/200.0f,percentSensorVariation/200.0f));
                //if(distanceVariation == distanceVariationType.DistanceAndRotation)
                //    worldRotation += Random.Range(-rotationSensorVariation,rotationSensorVariation);
                hitInfo.point = new Vector2(sensorObject.transform.position.x + Mathf.Sin(localWorldRotation/180*Mathf.PI)*distanceAbsolute,    sensorObject.transform.position.y + Mathf.Cos(localWorldRotation/180*Mathf.PI)*distanceAbsolute);
            }
            Instantiate(pointMarker, new Vector3(hitInfo.point.x,hitInfo.point.y,-1), Quaternion.identity);
            return(distanceAbsolute*1000.0f); //+ Random.Range(-mmVariation,mmVariation)
        }
        return(RaycastDistance * -1000);
    }
}
