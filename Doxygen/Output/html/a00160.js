var a00160 =
[
    [ "dir_bit", "a00160.html#ab236208f8ba057f4910f6698950ced27", null ],
    [ "dir_change", "a00160.html#acf6aa312f100484fafa548c7453e73f4", null ],
    [ "distance", "a00160.html#a1b5503c421425aad8bb08702ccb20438", null ],
    [ "distance_old", "a00160.html#aed7b99b501fcad439c3c8944b696e0ab", null ],
    [ "pulse_width_us", "a00160.html#ad33074b284b380aaab902e3ec8c0f22e", null ],
    [ "RE_dir_buff", "a00160.html#aa7b5f7a2502333e9da24b8d34cee0aec", null ],
    [ "RE_dt", "a00160.html#aa07231c858bb7da5ad324829ca0e3674", null ],
    [ "RE_dval", "a00160.html#a929b3536b5c3d4107a83138b23a86dce", null ],
    [ "RE_mp_s", "a00160.html#ac86f6801a763b7166dc33204ed35c680", null ],
    [ "RE_rp_m", "a00160.html#a4bbb69d9aebd73f8adbadf1790a119c6", null ],
    [ "RE_rp_s", "a00160.html#a60e0b1b22ea1e9c5d2115f77427d6382", null ],
    [ "RE_rp_us", "a00160.html#adb3e24a0e35da8df9481a9c23ec1a2c4", null ],
    [ "REtime_new", "a00160.html#a9263f4131a958db9b04a5b2caca2062a", null ],
    [ "REtime_old", "a00160.html#a6340d8ee80591d3ebed2ac83474e2a6c", null ],
    [ "REvalue", "a00160.html#a6acadfb81f1f36ca510fd63bb79c0424", null ],
    [ "servo_ID", "a00160.html#a0b774004c6eecb38da54dabcfaf1bb48", null ],
    [ "status", "a00160.html#a2f58b30a9c050bab972fc1cbc5ae52f1", null ]
];