var searchData=
[
  ['eth_5frx_5fbuf_5fsize_1291',['ETH_RX_BUF_SIZE',['../a00278.html#a0cdaf687f7a7f2dba570d5a722990786',1,'ETH_RX_BUF_SIZE():&#160;stm32f4xx_hal_conf.h'],['../a00281.html#a0cdaf687f7a7f2dba570d5a722990786',1,'ETH_RX_BUF_SIZE():&#160;stm32f4xx_hal_conf.h']]],
  ['eth_5frxbufnb_1292',['ETH_RXBUFNB',['../a00278.html#a62b0f224fa9c4f2e5574c9e52526f751',1,'ETH_RXBUFNB():&#160;stm32f4xx_hal_conf.h'],['../a00281.html#a62b0f224fa9c4f2e5574c9e52526f751',1,'ETH_RXBUFNB():&#160;stm32f4xx_hal_conf.h']]],
  ['eth_5ftx_5fbuf_5fsize_1293',['ETH_TX_BUF_SIZE',['../a00278.html#af83956dfc1b135c3c92ac409758b6cf4',1,'ETH_TX_BUF_SIZE():&#160;stm32f4xx_hal_conf.h'],['../a00281.html#af83956dfc1b135c3c92ac409758b6cf4',1,'ETH_TX_BUF_SIZE():&#160;stm32f4xx_hal_conf.h']]],
  ['eth_5ftxbufnb_1294',['ETH_TXBUFNB',['../a00278.html#a4ad07ad8fa6f8639ab8ef362390d86c7',1,'ETH_TXBUFNB():&#160;stm32f4xx_hal_conf.h'],['../a00281.html#a4ad07ad8fa6f8639ab8ef362390d86c7',1,'ETH_TXBUFNB():&#160;stm32f4xx_hal_conf.h']]],
  ['external_5fclock_5fvalue_1295',['EXTERNAL_CLOCK_VALUE',['../a00278.html#a8c47c935e91e70569098b41718558648',1,'EXTERNAL_CLOCK_VALUE():&#160;stm32f4xx_hal_conf.h'],['../a00281.html#a8c47c935e91e70569098b41718558648',1,'EXTERNAL_CLOCK_VALUE():&#160;stm32f4xx_hal_conf.h']]]
];
