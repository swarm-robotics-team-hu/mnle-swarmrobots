var searchData=
[
  ['data_5fcache_5fenable_135',['DATA_CACHE_ENABLE',['../a00278.html#a5b4c32a40cf49b06c0d761e385949a6b',1,'DATA_CACHE_ENABLE():&#160;stm32f4xx_hal_conf.h'],['../a00281.html#a5b4c32a40cf49b06c0d761e385949a6b',1,'DATA_CACHE_ENABLE():&#160;stm32f4xx_hal_conf.h']]],
  ['dataready_136',['dataReady',['../a00156.html#a3e59c240b01431770190782a79ace629',1,'DualSensorModuleTypeDef']]],
  ['debugmon_5fhandler_137',['DebugMon_Handler',['../a00287.html#adbdfb05858cc36fc520974df37ec3cb0',1,'DebugMon_Handler(void):&#160;stm32f4xx_it.c'],['../a00329.html#adbdfb05858cc36fc520974df37ec3cb0',1,'DebugMon_Handler(void):&#160;stm32f4xx_it.c']]],
  ['default_5fvalue_5fin_5fgrid_138',['DEFAULT_VALUE_IN_GRID',['../a00124.html#ga21e896d3a84df97020f3791a7e62a9bc',1,'HUB_Software']]],
  ['degree_139',['degree',['../a00131.html#gae7e9d34ef6dcd7a77f2189dbba9814cb',1,'sensor_fusion.h']]],
  ['delaymicroseconds_140',['DelayMicroSeconds',['../a00287.html#a5f9192bdcc4bbd513f250cf8268be98d',1,'DelayMicroSeconds(uint32_t u_sec):&#160;stm32f4xx_it.c'],['../a00329.html#a5f9192bdcc4bbd513f250cf8268be98d',1,'DelayMicroSeconds(uint32_t u_sec):&#160;stm32f4xx_it.c']]],
  ['delta_5ft_141',['delta_t',['../a00168.html#a080ec19b50ba0ed1202b775d72fe7d3b',1,'robot_data']]],
  ['delta_5ft_5fre_142',['delta_t_RE',['../a00168.html#aeb8a88a3f1725ecd8a59f17cbf22975c',1,'robot_data']]],
  ['dev_5fdefault_143',['Dev_Default',['../a00138.html#ga2e51714eea19371221cf1b2a26f5d181',1,'VL53L1X.c']]],
  ['dev_5fsensor_144',['Dev_Sensor',['../a00138.html#ga510038c0f28669b435ce01a91d93fe85',1,'VL53L1X.c']]],
  ['dir_5fbit_145',['dir_bit',['../a00160.html#ab236208f8ba057f4910f6698950ced27',1,'wheelservo']]],
  ['dir_5fchange_146',['dir_change',['../a00160.html#acf6aa312f100484fafa548c7453e73f4',1,'wheelservo']]],
  ['dircheck_147',['DirCheck',['../a00136.html#ga7834b67938d273d6ca790568285f6ed1',1,'rotary_encoder_lib.c']]],
  ['distance_148',['Distance',['../a00156.html#a08e003344f1b4adc2b04f8a7c850bd16',1,'DualSensorModuleTypeDef::Distance()'],['../a00160.html#a1b5503c421425aad8bb08702ccb20438',1,'wheelservo::distance()']]],
  ['distance_5foffset_149',['Distance_offset',['../a00138.html#gabe69995839c37c972ba649d2446ce3f5',1,'VL53L1X.h']]],
  ['distance_5fold_150',['distance_old',['../a00160.html#aed7b99b501fcad439c3c8944b696e0ab',1,'wheelservo']]],
  ['distance_5franges_151',['Distance_ranges',['../a00138.html#ga16f42b739cfe6471ba8937d4a98b4bfd',1,'VL53L1X.h']]],
  ['dp83848_5fphy_5faddress_152',['DP83848_PHY_ADDRESS',['../a00278.html#a25f014091aaba92bdd9d95d0b2f00503',1,'DP83848_PHY_ADDRESS():&#160;stm32f4xx_hal_conf.h'],['../a00281.html#a25f014091aaba92bdd9d95d0b2f00503',1,'DP83848_PHY_ADDRESS():&#160;stm32f4xx_hal_conf.h']]],
  ['drive_153',['DRIVE',['../a00132.html#ggab88951ff8944772d53c3b9e6f98cb726af7b6d6d8e5e14633d388ef9cc7a941b7',1,'DRIVE():&#160;interpreter.h'],['../a00132.html#ggab88951ff8944772d53c3b9e6f98cb726af7b6d6d8e5e14633d388ef9cc7a941b7',1,'DRIVE():&#160;interpreter.h']]],
  ['drive_5fcontrol_154',['drive_control',['../a00168.html#a25470980849fee5cda28f9439988dbac',1,'robot_data']]],
  ['dualsensormoduletypedef_155',['DualSensorModuleTypeDef',['../a00156.html',1,'']]],
  ['duration_156',['duration',['../a00152.html',1,'']]],
  ['dynamic_5fpayloads_5fenabled_157',['dynamic_payloads_enabled',['../a00127.html#gadbc721157566487ec7a7e37cd3bde3af',1,'dynamic_payloads_enabled():&#160;nrf24_lib.c'],['../a00135.html#gadbc721157566487ec7a7e37cd3bde3af',1,'dynamic_payloads_enabled():&#160;nrf24_lib.c']]]
];
