var searchData=
[
  ['tim_5fnew_1143',['tim_new',['../a00168.html#a5d3e38de8011bf8c1d33e0f3eafeb466',1,'robot_data']]],
  ['tim_5fnew_5fre_1144',['tim_new_RE',['../a00168.html#add868495a0b1b9be085941ad5a01394f',1,'robot_data']]],
  ['tim_5fold_1145',['tim_old',['../a00168.html#ac13bdcc8e0a66336cbe0a3e3fa85a266',1,'robot_data']]],
  ['tim_5fold_5fre_1146',['tim_old_RE',['../a00168.html#a8afa29328dcbca7f6d450465a1b70f55',1,'robot_data']]],
  ['total_5fms_1147',['total_ms',['../a00176.html#a23daf368a77dd88b2038c1ec2fb312ae',1,'runtime']]],
  ['total_5fus_1148',['total_us',['../a00176.html#a07a27983cc182a32539fd10338399a49',1,'runtime']]],
  ['travel_5fdis_1149',['travel_dis',['../a00168.html#a863f41976e5dcb63de3ce0a82ba8c87f',1,'robot_data']]],
  ['travel_5fori_1150',['travel_ori',['../a00168.html#ada28de6b94cb11e6e82e4743c6fac766',1,'robot_data']]],
  ['traveled_5fdistance_1151',['traveled_distance',['../a00168.html#ada346ab61f3a5167b003a789befd8591',1,'robot_data']]]
];
