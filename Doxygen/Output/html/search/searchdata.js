var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxyz",
  1: "bdeiprw",
  2: "h",
  3: "ghimnprstuv",
  4: "_abcdeghikmnoprstuvw",
  5: "abcdefghiklmnopqrstuvwxy",
  6: "acgprs",
  7: "abdgilmnprsxyz",
  8: "_abcdehilmnprstuv",
  9: "himprsvw",
  10: "i"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

