var searchData=
[
  ['p_5fvariant_1091',['p_variant',['../a00127.html#ga52644b257bcd6b29e18e3855c0f4c7c8',1,'p_variant():&#160;nrf24_lib.c'],['../a00135.html#ga52644b257bcd6b29e18e3855c0f4c7c8',1,'p_variant():&#160;nrf24_lib.c']]],
  ['pathfinding_5funknown_5fvalue_1092',['PATHFINDING_UNKNOWN_VALUE',['../a00124.html#ga0adcaedc5fda5aa391c89871ee52f560',1,'HUB_Software']]],
  ['pathfinding_5fwall_5fthickness_1093',['PATHFINDING_WALL_THICKNESS',['../a00124.html#gaa70ff5ec4c832d1ad1788680ea1890c6',1,'HUB_Software']]],
  ['pathfinding_5fwall_5fvalue_1094',['PATHFINDING_WALL_VALUE',['../a00124.html#ga1ea1a5ddb88f41943ec5cbdc1239da69',1,'HUB_Software']]],
  ['payload_5fsize_1095',['payload_size',['../a00127.html#ga65f255eac5ac95eac87265229309fa60',1,'payload_size():&#160;nrf24_lib.c'],['../a00135.html#ga65f255eac5ac95eac87265229309fa60',1,'payload_size():&#160;nrf24_lib.c']]],
  ['phi_5fre_1096',['phi_RE',['../a00168.html#afe952da07635cca851be81ef9ac289d8',1,'robot_data']]],
  ['pipe0_5freading_5faddress_1097',['pipe0_reading_address',['../a00127.html#ga5347967bca1ce5369ad98a2380e47ad5',1,'pipe0_reading_address():&#160;nrf24_lib.c'],['../a00135.html#ga5347967bca1ce5369ad98a2380e47ad5',1,'pipe0_reading_address():&#160;nrf24_lib.c']]],
  ['pos_1098',['pos',['../a00168.html#a87be7e5fe6831cf3e0290c0aded9dc87',1,'robot_data']]],
  ['pos_5fbuff_1099',['pos_buff',['../a00168.html#a43cf3b10e40938d1dcf5a779ca71cc34',1,'robot_data']]],
  ['pos_5fimu_1100',['pos_IMU',['../a00168.html#aa246af61f4ae336effe88645650f40bb',1,'robot_data']]],
  ['pos_5fre_1101',['pos_RE',['../a00168.html#ae1bdd8fdfb7cd9eb9ece2bf17655293b',1,'robot_data']]],
  ['pulse_5fwidth_5fus_1102',['pulse_width_us',['../a00160.html#ad33074b284b380aaab902e3ec8c0f22e',1,'wheelservo']]]
];
