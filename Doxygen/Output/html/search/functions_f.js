var searchData=
[
  ['searchunkownpoints_963',['SearchUnkownPoints',['../a00124.html#ga0a037c16ae66c4aa7043d62315b23afb',1,'HUB_Software']]],
  ['sensorfusioninit_964',['SensorFusionInit',['../a00131.html#ga555cab3ce2e539e1f671ec19cbc889d4',1,'sensor_fusion.c']]],
  ['sensormodule_5f360_5fprint_965',['SensorModule_360_Print',['../a00138.html#gad4b7f9c93345bfabea9ee0a1d5a775cb',1,'VL53L1X.c']]],
  ['sensormoduleread_966',['SensorModuleRead',['../a00138.html#ga0ea69f28d88975429e7c8a05173e2bc8',1,'VL53L1X.c']]],
  ['sensormodulesetangleread_967',['SensorModuleSetAngleRead',['../a00138.html#gaed21da318d6215521362b1da1ddc6ac3',1,'VL53L1X.c']]],
  ['sensormodulesweep_968',['SensorModuleSweep',['../a00138.html#ga6befc967c1ef822f7e9318dddbbf2b13',1,'VL53L1X.c']]],
  ['servo_5ftask_969',['SERVO_TASK',['../a00137.html#gaa38900fb138796cd8ee8ff13b004d974',1,'servo.c']]],
  ['servoconfigurepwm_970',['ServoConfigurePWM',['../a00137.html#ga0f7c60c07f48890b853ebf2506ae3d8f',1,'servo.c']]],
  ['servoinit_971',['ServoInit',['../a00137.html#ga2fcc18241c2efe5f19358a73fd4cdd18',1,'servo.c']]],
  ['servosetangle_972',['ServoSetAngle',['../a00137.html#ga8b04ce0dda4d1cb05da5360d09e04705',1,'servo.c']]],
  ['servosetspeed_973',['ServoSetSpeed',['../a00137.html#gaea58e76d02e72e8236fd41580538a384',1,'servo.c']]],
  ['showgridmapimagepil_974',['ShowGridMapImagePIL',['../a00124.html#ga18d38f735af0bba1ea867bbefe641765',1,'HUB_Software']]],
  ['showgridmapopencv_975',['ShowGridMapOpenCV',['../a00124.html#ga8398019eb0a9e4bac58ffae7ced1151a',1,'HUB_Software']]],
  ['showpathfindingpil_976',['ShowPathfindingPIL',['../a00124.html#gad0700e533f73167c34c93f052c365c3d',1,'HUB_Software']]],
  ['simulatetravel_977',['SimulateTravel',['../a00139.html#gaa81fcae9cef65857803e22dbf465191a',1,'main.c']]],
  ['slipcheck_978',['SlipCheck',['../a00136.html#ga8baa0c440fffbdea5d0b0bab5d84ad46',1,'rotary_encoder_lib.c']]],
  ['spi_5fperipheral_5fswitch_979',['SPI_peripheral_switch',['../a00272.html#ab5c421fbf90f2c8dd0858f8b3812bde5',1,'SPI_peripheral_switch(SPI_PERIPHERALS per):&#160;spi.c'],['../a00275.html#ab5c421fbf90f2c8dd0858f8b3812bde5',1,'SPI_peripheral_switch(SPI_PERIPHERALS per):&#160;spi.c'],['../a00317.html#ab5c421fbf90f2c8dd0858f8b3812bde5',1,'SPI_peripheral_switch(SPI_PERIPHERALS per):&#160;spi.c']]],
  ['svc_5fhandler_980',['SVC_Handler',['../a00287.html#a3e5ddb3df0d62f2dc357e64a3f04a6ce',1,'SVC_Handler(void):&#160;stm32f4xx_it.c'],['../a00329.html#a3e5ddb3df0d62f2dc357e64a3f04a6ce',1,'SVC_Handler(void):&#160;stm32f4xx_it.c']]],
  ['systemclock_5fconfig_981',['SystemClock_Config',['../a00139.html#ga70af21c671abfcc773614a9a4f63d920',1,'main.c']]],
  ['systick_5fhandler_982',['SysTick_Handler',['../a00287.html#ab5e09814056d617c521549e542639b7e',1,'SysTick_Handler(void):&#160;stm32f4xx_it.c'],['../a00329.html#ab5e09814056d617c521549e542639b7e',1,'SysTick_Handler(void):&#160;stm32f4xx_it.c']]]
];
