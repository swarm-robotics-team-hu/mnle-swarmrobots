var searchData=
[
  ['dataready_1021',['dataReady',['../a00156.html#a3e59c240b01431770190782a79ace629',1,'DualSensorModuleTypeDef']]],
  ['default_5fvalue_5fin_5fgrid_1022',['DEFAULT_VALUE_IN_GRID',['../a00124.html#ga21e896d3a84df97020f3791a7e62a9bc',1,'HUB_Software']]],
  ['delta_5ft_1023',['delta_t',['../a00168.html#a080ec19b50ba0ed1202b775d72fe7d3b',1,'robot_data']]],
  ['delta_5ft_5fre_1024',['delta_t_RE',['../a00168.html#aeb8a88a3f1725ecd8a59f17cbf22975c',1,'robot_data']]],
  ['dev_5fdefault_1025',['Dev_Default',['../a00138.html#ga2e51714eea19371221cf1b2a26f5d181',1,'VL53L1X.c']]],
  ['dev_5fsensor_1026',['Dev_Sensor',['../a00138.html#ga510038c0f28669b435ce01a91d93fe85',1,'VL53L1X.c']]],
  ['dir_5fbit_1027',['dir_bit',['../a00160.html#ab236208f8ba057f4910f6698950ced27',1,'wheelservo']]],
  ['dir_5fchange_1028',['dir_change',['../a00160.html#acf6aa312f100484fafa548c7453e73f4',1,'wheelservo']]],
  ['distance_1029',['Distance',['../a00156.html#a08e003344f1b4adc2b04f8a7c850bd16',1,'DualSensorModuleTypeDef::Distance()'],['../a00160.html#a1b5503c421425aad8bb08702ccb20438',1,'wheelservo::distance()']]],
  ['distance_5foffset_1030',['Distance_offset',['../a00138.html#gabe69995839c37c972ba649d2446ce3f5',1,'VL53L1X.h']]],
  ['distance_5fold_1031',['distance_old',['../a00160.html#aed7b99b501fcad439c3c8944b696e0ab',1,'wheelservo']]],
  ['distance_5franges_1032',['Distance_ranges',['../a00138.html#ga16f42b739cfe6471ba8937d4a98b4bfd',1,'VL53L1X.h']]],
  ['drive_5fcontrol_1033',['drive_control',['../a00168.html#a25470980849fee5cda28f9439988dbac',1,'robot_data']]],
  ['dynamic_5fpayloads_5fenabled_1034',['dynamic_payloads_enabled',['../a00127.html#gadbc721157566487ec7a7e37cd3bde3af',1,'dynamic_payloads_enabled():&#160;nrf24_lib.c'],['../a00135.html#gadbc721157566487ec7a7e37cd3bde3af',1,'dynamic_payloads_enabled():&#160;nrf24_lib.c']]]
];
