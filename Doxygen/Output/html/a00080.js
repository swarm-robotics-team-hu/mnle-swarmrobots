var a00080 =
[
    [ "degree", "a00131.html#gae7e9d34ef6dcd7a77f2189dbba9814cb", null ],
    [ "Ki", "a00131.html#ga6ccd383fe0d82309eac500d9bd3b825b", null ],
    [ "Kp", "a00131.html#ga75bcee4b28157d51fa12c57a1a5d321a", null ],
    [ "rad", "a00131.html#ga499a23bc97c200aebcde99790071e1ab", null ],
    [ "InvSqrt", "a00131.html#ga55a133284c9a7f0b98e5fc2af98a67e4", null ],
    [ "SensorFusionInit", "a00131.html#ga555cab3ce2e539e1f671ec19cbc889d4", null ],
    [ "UpdateQuaternionMadgwick", "a00131.html#gae3e8294ecd518a206cbbd26d9cc88177", null ],
    [ "UpdateQuaternionMahony", "a00131.html#ga5caca731dbda8af3bfaeafaa78c4211d", null ],
    [ "beta", "a00131.html#gaa773d9a6c0ccefaa0fc9ab66fec68ec1", null ],
    [ "integralFB_x", "a00131.html#ga1c18706aba5a11cf0486b7319c0a60af", null ],
    [ "integralFB_y", "a00131.html#ga48502d589b468b958c313d80fae7a71d", null ],
    [ "integralFB_z", "a00131.html#ga3716d8e2fa1af23daf665874fcfadac9", null ]
];