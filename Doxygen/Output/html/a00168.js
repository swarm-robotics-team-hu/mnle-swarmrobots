var a00168 =
[
    [ "accel", "a00168.html#aee92f180c844b84b2415abfa3bd2ee63", null ],
    [ "accel_data", "a00168.html#a9f8cc35cd5a8e07fd90b4151c16e3f47", null ],
    [ "delta_t", "a00168.html#a080ec19b50ba0ed1202b775d72fe7d3b", null ],
    [ "delta_t_RE", "a00168.html#aeb8a88a3f1725ecd8a59f17cbf22975c", null ],
    [ "drive_control", "a00168.html#a25470980849fee5cda28f9439988dbac", null ],
    [ "error", "a00168.html#af8dd83e688373cc9bfb3b7788218f795", null ],
    [ "gyro", "a00168.html#a00a135d1e3b2dfe539e47ef5695e7790", null ],
    [ "gyro_data", "a00168.html#ae89c3c8d1da94d42a2a065ac0bfc5812", null ],
    [ "IMU_ori", "a00168.html#ac8cf12f665998de725cf8c70363803ec", null ],
    [ "imu_status", "a00168.html#a60eb4276a03ceaec07ebdfa29c510226", null ],
    [ "new_pos", "a00168.html#a8afce77e4d9aed376c121df339b2bfdc", null ],
    [ "ori", "a00168.html#a6099dbfd643bf02969a3092c5bbb465d", null ],
    [ "phi_RE", "a00168.html#afe952da07635cca851be81ef9ac289d8", null ],
    [ "pos", "a00168.html#a87be7e5fe6831cf3e0290c0aded9dc87", null ],
    [ "pos_buff", "a00168.html#a43cf3b10e40938d1dcf5a779ca71cc34", null ],
    [ "pos_IMU", "a00168.html#aa246af61f4ae336effe88645650f40bb", null ],
    [ "pos_RE", "a00168.html#ae1bdd8fdfb7cd9eb9ece2bf17655293b", null ],
    [ "q", "a00168.html#aa045980b707ab959099506f81d8d6c3f", null ],
    [ "raw_accel", "a00168.html#ac653266e59a7d1148b7bb57c1db1ead4", null ],
    [ "raw_gyro", "a00168.html#a5bb502cf7b46d824336c876b24456a91", null ],
    [ "raw_mag", "a00168.html#aa291c8d4818dd8ef6c6a09f4679dc27b", null ],
    [ "sensor_module", "a00168.html#a19970b888f97da19dff6e9cbeedf6f20", null ],
    [ "sensor_read_flag", "a00168.html#a676e09d323d92e3444d4c8662689c9c9", null ],
    [ "servoL", "a00168.html#a7d08556b86bc3d6fca2b46f4b2bb45f0", null ],
    [ "servoR", "a00168.html#add2c5216abed3997ef5e28706e0d281f", null ],
    [ "tim_new", "a00168.html#a5d3e38de8011bf8c1d33e0f3eafeb466", null ],
    [ "tim_new_RE", "a00168.html#add868495a0b1b9be085941ad5a01394f", null ],
    [ "tim_old", "a00168.html#ac13bdcc8e0a66336cbe0a3e3fa85a266", null ],
    [ "tim_old_RE", "a00168.html#a8afa29328dcbca7f6d450465a1b70f55", null ],
    [ "travel_dis", "a00168.html#a863f41976e5dcb63de3ce0a82ba8c87f", null ],
    [ "travel_ori", "a00168.html#ada28de6b94cb11e6e82e4743c6fac766", null ],
    [ "traveled_distance", "a00168.html#ada346ab61f3a5167b003a789befd8591", null ],
    [ "vel", "a00168.html#a4363f82f5999bb7e889ea916e6f21aa5", null ],
    [ "vel_IMU", "a00168.html#a2c5a374a315fbad68b3c0054cf9d331b", null ],
    [ "vel_RE", "a00168.html#a28ef0aed6386bcac2cdb7ef01f60a1a3", null ],
    [ "yaw_RE", "a00168.html#ac3c8a8aab49546c671c56f74109cccde", null ]
];