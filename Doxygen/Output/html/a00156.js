var a00156 =
[
    [ "Address", "a00156.html#aee8f7b3a6854a1ba8c9bd640b189a8b6", null ],
    [ "AmbientRate", "a00156.html#a875e87da720bdaad21e1f771b2467282", null ],
    [ "dataReady", "a00156.html#a3e59c240b01431770190782a79ace629", null ],
    [ "Distance", "a00156.html#a08e003344f1b4adc2b04f8a7c850bd16", null ],
    [ "offset", "a00156.html#aeab78ef3cb58177937cbe49292b0014a", null ],
    [ "RangeStatus", "a00156.html#a9c065ec3e38ae06ba86e28d6cde1704c", null ],
    [ "ServoAngle", "a00156.html#af2b09ce5aec8acb1cca2c8a45f5c6da7", null ],
    [ "SignalRate", "a00156.html#a03b1380ca4baf94cba843f499507e029", null ],
    [ "SpadNum", "a00156.html#a6427149ca466bd5143056633f4ff7cd6", null ],
    [ "Status", "a00156.html#a485286b47e78f30cc426d1478c67db37", null ],
    [ "stepsize", "a00156.html#a4642667756e51aa91726fbcfb07579c5", null ],
    [ "xtalk", "a00156.html#a69a76d886cc595454837cf5f61fd76ad", null ]
];