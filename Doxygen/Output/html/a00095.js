var a00095 =
[
    [ "CorrectDistance", "a00136.html#gaa969b9f9df9b3c59f3f2b9888c5d2bc4", null ],
    [ "DirCheck", "a00136.html#ga7834b67938d273d6ca790568285f6ed1", null ],
    [ "EncoderValue_dis", "a00136.html#ga07491b93de2939b3e82c78e005517e00", null ],
    [ "GetDeltaEncoderValue", "a00136.html#gadad31e1122a1ced409e82d3654f57847", null ],
    [ "KeepItStraight", "a00136.html#ga4643aa11e4eccf5ac8b10a1dcf1f363b", null ],
    [ "Odometry_RE", "a00136.html#gac3432722b65b058f863ceb6083794267", null ],
    [ "PID_init", "a00136.html#ga68a0014454759e057f960de252e23119", null ],
    [ "ReadEncoder", "a00136.html#ga20ad2efe4b0a164cf6c96807545446f9", null ],
    [ "SlipCheck", "a00136.html#ga8baa0c440fffbdea5d0b0bab5d84ad46", null ]
];