/**
  ******************************************************************************
  * @file           : interpreter.c
  * @brief          : Library for interpreting commands
  * @author			: Swarm Robotics Team
  * @date			: 08-11-2019
  ******************************************************************************
  */

/** @addtogroup HUB
  * @brief	All the software for the HUB
  * @{ 
  */

/** @addtogroup STM_Firmware
  * @brief	All the C software for the STM hardware
  * @{
  */

/** @addtogroup Interpreter
  * @brief	All code related to the interpreter
  * @{
  */

#include "interpreter.h"

void (*itrp_ptr)(uint8_t);	/* Interpreter function pointer */
uint8_t cmd_mask[] = {DRIVE, SENSOR, POSITION, MISC};
uint8_t myAckPayload[32] = "Acknowledge by HUB";
int receive_data_buf[32]; /* int32_t */

/*******************************************************************************************************//**
 * @brief Start the right function with the command
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
void ITRP_CmdNumber(uint8_t cmd)
{
	/* Store function addresses in void pointer */
	void *itrp_address[CMD_NUM_AMOUNT];
	itrp_address[0] = &ITRP_Drive;
	itrp_address[1] = &ITRP_Sensor;
	itrp_address[2] = &ITRP_Position;
	itrp_address[3] = &ITRP_Miscellaneous;

	int i;
	int cmd_nr = (cmd & 0xF0)>>4;

	/* Break if supported command num has been found */
	for(i=0; i<sizeof(cmd_mask); i++)
		if(cmd_nr == cmd_mask[i])
			break;

	itrp_ptr = itrp_address[i];	/* Point to function with found command num			    */
	itrp_ptr(cmd);				/* Execute interpret function for specified command num */

}

/*******************************************************************************************************//**
 * @brief This function is a switch case structure for the Drive commands (0x00)
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
void ITRP_Drive(uint8_t cmd)
{
	/* Switch case for Drive */
	switch(cmd)
	{
		case AHEAD:			ITRP_TransmitPacket(4, cmd, uart_arg_array[1], uart_arg_array[2], 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case BACK:			ITRP_TransmitPacket(4, cmd, uart_arg_array[1], uart_arg_array[2], 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case STANDSTILL:	ITRP_TransmitPacket(4, cmd, 0, 0, 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case LEFT_AXIS:		ITRP_TransmitPacket(4, cmd, uart_arg_array[1], uart_arg_array[2], 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case RIGHT_AXIS:	ITRP_TransmitPacket(4, cmd, uart_arg_array[1], uart_arg_array[2], 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case ANGLE_AXIS:	ITRP_TransmitPacket(4, cmd, uart_arg_array[1], 0, 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case GOTO_XY:		ITRP_TransmitPacket(4, cmd, uart_arg_array[1], uart_arg_array[2], uart_arg_array[3]);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							NRF24_StartListening();
							while(ITRP_ReceivePacket(receive_data_buf, 0x0A)==0);
							printf("%d,%d\r\n",0x0A,0);
							NRF24_StopListening();
							break;

		case LEFT_SV: 		ITRP_TransmitPacket(4, cmd, uart_arg_array[1], uart_arg_array[2], 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case RIGHT_SV: 		ITRP_TransmitPacket(4, cmd, uart_arg_array[1], uart_arg_array[2], 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		default:			break;
	}
}

/*******************************************************************************************************//**
 * @brief This function is a switch case structure for the Sensor commands (0x10)
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
void ITRP_Sensor(uint8_t cmd)
{
	/* Switch case for Sensor */
	switch(cmd)
	{
		case SET_ANGLE:		ITRP_TransmitPacket(2, cmd, uart_arg_array[1]);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case READ_SINGLE:	ITRP_TransmitPacket(2, cmd, uart_arg_array[1]);
							if(ITRP_ReceivePacket(receive_data_buf, cmd))
							printf("%d,%d,%d,%d,%d,%d,%d,%d\r\n",cmd,receive_data_buf[1],receive_data_buf[2],receive_data_buf[3],receive_data_buf[4],receive_data_buf[5],receive_data_buf[6],receive_data_buf[7]);
							break;

		case START_SENSOR:	ITRP_TransmitPacket(2, cmd, 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case STOP_SENSOR:	ITRP_TransmitPacket(2, cmd, 0);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case SENSOR_STATUS:	ITRP_TransmitPacket(2, cmd, 0);
							if(ITRP_ReceivePacket(receive_data_buf, cmd))
								printf("%d,%d,%d\r\n",cmd,receive_data_buf[1],receive_data_buf[2]);
							break;

		case READ_ALL:		ITRP_TransmitPacket(2, cmd, 0);
							int16_t sensor_data[360] = {0};
							uint8_t status;

							status = ITRP_ReceiveAllSensorData(sensor_data, 5000);
							if(!status)
								status = ITRP_RetransmitAllSensorData(sensor_data);
							if (status)
							{
								printf("%d", READ_ALL); //Cmd 0x16 from readall
								int i;
								for(i = 0; i< 360; i++)
									printf(",%d", sensor_data[i]);
								printf("\r\n");
							}
							break;

		default:			break;
	}
}

/*******************************************************************************************************//**
 * @brief This function is a switch case structure for the Position commands (0x40)
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
void ITRP_Position(uint8_t cmd)
{
	/* Switch case for Position */
	switch(cmd)
	{
		case GET_REL_POS:	ITRP_TransmitPacket(4, cmd, 0, 0, 0);
							if(ITRP_ReceivePacket(receive_data_buf, cmd))
								printf("%d,%d,%d,%d\r\n",cmd,receive_data_buf[1],receive_data_buf[2],receive_data_buf[3]);
							break;

		case SET_REL_POS:	ITRP_TransmitPacket(4, cmd, uart_arg_array[1], uart_arg_array[2], uart_arg_array[3]);
							ITRP_ReceivePacket(receive_data_buf, cmd);
							break;

		case GET_SPEED:		ITRP_TransmitPacket(4, cmd, 0, 0, 0);
							if(ITRP_ReceivePacket(receive_data_buf, cmd))
								printf("%d,%d,%d\r\n",cmd,receive_data_buf[1],receive_data_buf[2]);
							break;

		case GET_ACCEL:		ITRP_TransmitPacket(4, cmd, 0, 0, 0);
							if(ITRP_ReceivePacket(receive_data_buf, cmd))
								printf("%d,%d,%d,%d\r\n",cmd,receive_data_buf[1],receive_data_buf[2],receive_data_buf[3]);
							break;

		case GET_GYRO:		ITRP_TransmitPacket(4, cmd, 0, 0, 0);
							if(ITRP_ReceivePacket(receive_data_buf, cmd))
								printf("%d,%d,%d,%d\r\n",cmd,receive_data_buf[1],receive_data_buf[2],receive_data_buf[3]);
							break;

		case IMU_STATUS:	ITRP_TransmitPacket(4, cmd, 0, 0, 0);
							if(ITRP_ReceivePacket(receive_data_buf, cmd))
								printf("%d,%d\r\n",cmd,receive_data_buf[1]);
							break;

		default:			break;
	}
}

/*******************************************************************************************************//**
 * @brief This function is a switch case structure for the Miscellaneous commands (0x50)
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
void ITRP_Miscellaneous(uint8_t cmd)
{
	/* Switch case for Miscellaneous*/
	switch(cmd)
		{
			case GET_FAILS:		ITRP_TransmitPacket(1, cmd);
								if(ITRP_ReceivePacket(receive_data_buf, cmd))
								{
									uint8_t i;
									printf("%d",cmd);
									for(i=1;i<32;i++)
										printf(",%d", receive_data_buf[i]);
									printf("\r\n");
								}
								break;

			case BAT_PERCENT:	ITRP_TransmitPacket(1, cmd);
								if(ITRP_ReceivePacket(receive_data_buf, cmd))
									printf("%d,%d\r\n",cmd,receive_data_buf[1]);
								break;

			default:			break;
		}
}

/*******************************************************************************************************//**
 * @brief This function will receive all the 24 messages of 31 bytes with NRF from the robot
 *
 * @param sensor_data 	: Pointer to array with incoming data from NRF
 *******************************************************************************************************/
int ITRP_ReceiveAllSensorData(int16_t *sensor_data, uint16_t timeout)
{
	/* Create variables */
	int32_t counter_data = 0;
	runtime start_time = get_runtime();
	runtime get_time;
	uint8_t succesfull_transmission = 1;

	NRF24_StartListening();
	while(1)
	{
		/* Flag will be 1 when there is data available in the cmd_buf */
		if(nrf24_cmd_flag == 1)
		{
			uint8_t cmd = cmd_buf[0];
			/* Check if the cmd is one of the 24 packets with sensordata */
			if (cmd >= 0x21 && cmd <= 0x38)
			{
				NRF24_WriteAckPayload(1, myAckPayload, 32);
				/* Read data from 15 degrees */
				uint8_t i;
				for(i=0; i<15; i++)
				{
					sensor_data[i+((cmd-33)*15)] = (cmd_buf[i*2+1] << 8) | cmd_buf[i*2+2];
				}
				/* Write the incoming data to the output array */
				counter_data = counter_data | (1<<(cmd-33));
			}
			nrf24_cmd_flag = 0;
		}

		/* Check if the expected data is complete */
		if (counter_data == 0xFFFFFF)
		{
			break;
		}
		/* Timeout code after 5 seconds */
		get_time = get_runtime();
		if(start_time.total_ms + timeout < get_time.total_ms)
		{
			succesfull_transmission = 0;
			break;
		}
	}
	//HAL_Delay(2);
	NRF24_StopListening();
	return succesfull_transmission;
}

int ITRP_RetransmitAllSensorData(int16_t *sensor_data)
{
	uint8_t counter = 0;
	uint8_t status  = 0;
	while(status == 0 && counter <= 5)
	{
		ITRP_TransmitPacket(2, 0x17, 0);
		status = ITRP_ReceiveAllSensorData(sensor_data, 1000);
		counter++;
	}
	return status;
}

/*********************************************************************************************************//**
 * @brief This function will build a packet and when that is complete it will sent it with the NRF module
 *
 * @param args_num	 	: Size of arguments which need to create the right package for each functioncode
 * @param ...			: Unlimited arguments with datat that need to be transmit
 *********************************************************************************************************/
void ITRP_TransmitPacket(int args_num, ...)
{
	uint8_t cmd_ret[32] = {0};			/* Transmit buffer for commands			*/
	va_list arguments;					/* List of arguments					*/
	va_start(arguments, args_num);

	/* Build the data packet and sent it with NRF module*/
	ITRP_BuildPacket(cmd_ret, args_num, arguments);
	NRF_Transmit(cmd_ret);
	//HAL_Delay(1);
	va_end(arguments);
}

/***************************************************************************************************//**
 * @brief This function will start the wireless communication and it will read the incoming package
 *
 * @param unpacked_data 			: Pointer to array with the data that needs to be unpacked
 * @param cmd						: Function code which tells the code what to do
 *
 * @retval succesfull_transmission	: Return if all data has received correctly
 **************************************************************************************************/
int ITRP_ReceivePacket(int *unpacked_data, uint8_t cmd)
{
	uint8_t succesfull_transmission = 0;

	uint16_t counter = 0;

	/* Start with listening on the NRF modules */
	NRF24_StartListening();
	while(counter<10)
	{
		/* Check if there is data and if this data is right for getting sensordata */
		if(nrf24_cmd_flag == 1)
		{
			if(cmd_buf[0]==cmd)
			{
				NRF24_WriteAckPayload(1, myAckPayload, 32); 	/* Write Acknowledge to other device */
				ITRP_ReadPacket(unpacked_data, cmd);					/* Read the packet */
				HAL_Delay(1);
				succesfull_transmission = 1;
				nrf24_cmd_flag = 0;
				break;
			}

		}
		counter++;
		HAL_Delay(1);
	}
	//HAL_Delay(1);
	/* Stop with listening on the NRF modules */
	NRF24_StopListening();
	return succesfull_transmission;
}

/************************************************************************************************************************//**
 * @brief This function will read the package that needs to be sent over the wireless communication
 *
 * @param cmd_ret			: Pointer to array, the data will be saved in this array untill it got sent
 * @param args_num			: Size of arguments which need to create the right package for each functioncode
 * @param arguments	: Unlimited list of arguments
 ************************************************************************************************************************/
void ITRP_ReadPacket(int *unpacked_data, uint8_t cmd)
{
	uint8_t i,j;
	union int_to_bytes convert; /* Union for converting the int32_t type to bytes		*/
	uint8_t	param_size;			/* Stores paramater data size (1 byte, 2 bytes etc.)	*/
	uint8_t	cmd_type;			/* Stores CMD type	*/
	uint8_t index = 0;

	cmd_type = cmd >> 4;		/* Shift command number to get index for cmd parameter size array (most significant nibble)	*/

	/* Clear the unpacked_data */
	for(i=0; i<32; i++)
		unpacked_data[i] = 0;

	/* Read all the 32 incoming bytes */
	for(i=0; i<32; i++)
		{
			param_size = cmd_param_size_rx[cmd_type][i];		/* Get amount of bytes used by the command parameter	*/

			/* Break when all the bytes were read */
			if(param_size == 0)
				break;
			/* Add to command return buffer	*/
			for(j=0; j<4; j++)
				convert.bytes[j] = 0;	/* LSB contains first byte of the int32_t */

			/* Make bytes to ints */
			for(j=param_size; j>0; j--)
			{
				convert.bytes[j-1] = cmd_buf[index];	/* LSB contains first byte of the int32_t */
				index++;
			}
			unpacked_data[i]= convert.int_var;
		}
}

/************************************************************************************************************************//**
 * @brief This function will create the package that needs to be sent over the wireless communication
 *
 * @param cmd_ret	: Pointer to array, the data will be saved in this array untill it got sent
 * @param args_num	: Size of arguments which need to create the right package for each functioncode
 * @param arguments	: Unlimited list of arguments
 ************************************************************************************************************************/
void ITRP_BuildPacket(uint8_t *cmd_ret, int args_num, va_list arguments)
{
	union int_to_bytes convert; /* Union for converting the int32_t type to bytes		*/
	int		param_size;			/* Stores paramater data size (1 byte, 2 bytes etc.)	*/
	int		cmd_type;			/* Stores CMD type										*/


	convert.int_var = va_arg(arguments, int32_t);	/* Read command number																		*/
	cmd_ret[0] = convert.int_var;					/* Add command number as first byte of the return command									*/
	cmd_type = convert.int_var >> 4;				/* Shift command number to get index for cmd parameter size array (most significant nibble)	*/

	/* Read argument, find amount of bytes and fill the command return buffer	*/
	int i, j;
	int index = 1;	/* start at index 1, because index 0 is already filled		*/
	for (i=1;i<32;i++)
		cmd_ret[i] = 0;

	for(i=1; i<args_num; i++)
	{
		convert.int_var  = va_arg(arguments, int32_t);	/* Get argument variable and save as int32_t			*/
		param_size = cmd_param_size_tx[cmd_type][i];		/* Get amount of bytes used by the command parameter	*/

		/* Add to command return buffer	*/
		for(j=param_size; j>0; j--)
		{
			cmd_ret[index] = convert.bytes[j-1];	/* LSB contains first byte of the int32_t */
			index++;
		}
	}
	va_end(arguments);
}

/*****************************************************************************//**
 * @brief This function will split uart input to int and run the interpreter
 *****************************************************************************/
void ITRP_Run(void)
{
	if(uart_rx_flag != 0)
	{
		//printf("In interpreter\r\n");
		uint8_t i =0;

		/* Clear uart array */
		for(i=0; i<32;i++)
		{
			uart_arg_array[i]=0;
		}
		i =0;

		/* Check if there the SPLITCHAR is in the incoming data */

		char *input_string;
		char *output;
		char *saveptr;
		uint64_t robot_address;

		for(input_string = (char *)uart_rx_buf, i=0;;input_string = NULL, i++)
		{
			output = strtok_r(input_string, ",", &saveptr);
			if (output == NULL)
				break;
			if (i == 0)
			{
				robot_address = atoi(output);

				//NRF24_StopListening();					//Make sure nRF24L01 is not in RX-mode
				NRF24_OpenWritingPipe(robot_address);		//Open writing pipe on 0x
				//NRF24_StopListening();
				NRF24_OpenReadingPipe(1, robot_address+1000000000000);
				NRF24_StopListening();
				//PrintRadioSettings();
			}
			else
				uart_arg_array[i-1] = atoi(output);
		}
		uart_rx_flag =0;
		ITRP_CmdNumber(uart_arg_array[0]);
	}
}

/**
  * @}
  */
/**
  * @}
  */
/**
  * @}
  */
