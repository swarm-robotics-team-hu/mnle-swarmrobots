/**
  ******************************************************************************
  * @file           : interpreter.h
  * @brief          : Library for interpreting commands
  * @author			: Nekija Dzemaili
  * @date			: 08-11-2019
  ******************************************************************************
  */

/** @addtogroup HUB
  * @brief	All the software for the HUB
  * @{
  */

/** @addtogroup STM_Firmware
  * @brief	All the C software for the STM hardware
  * @{
  */

/** @addtogroup Interpreter
  * @brief	All code related to the interpreter
  * @{
  */


#ifndef __INTERPRETER_H
#define __INTERPRETER_H

#include "main.h"

#define CMD_NUM_AMOUNT 4		/* Amount of command types					*/
#define CMD_MASK	   0x0F		/* Mask to filter least significant nibble	*/
#define	DRIVE	0x00
#define	SENSOR	0x01
#define	POSITION	0x04
#define	MISC	0x05

/* Masks for command types */
//enum CMD_NUM_MASK
//{
//	DRIVE = 0x00,			/* Commands for movement and servos			*/
//	SENSOR,					/* Commands for distance sensors			*/
//	POSITION = 0x04,		/* Commands for relative position and IMU	*/
//	MISC					/* Miscellaneous commands					*/
//};

enum CMD_DRIVE
{
	AHEAD = 0x01,	/**< 0x01 Drive ahead							*/
	BACK,			/**< 0x02 Drive back							*/
	STANDSTILL,		/**< 0x03 Stand still							*/
	LEFT_AXIS,		/**< 0x04 Rotate left around axis				*/
	RIGHT_AXIS,		/**< 0x05 Rotate right around axis				*/
	ANGLE_AXIS,		/**< 0x06 Rotate an angle around axis			*/
	GOTO_XY,		/**< 0x07 Go to position (x,y) coordinate		*/
	LEFT_SV,		/**< 0x08 Set left servo speed and duration		*/
	RIGHT_SV		/**< 0x09 Set left servo speed and duration		*/
};

enum CMD_SENSOR
{
	SET_ANGLE = 0x11,	/**< 0x11 Set angle of sensor servo			*/
	READ_SINGLE,		/**< 0x12 Read single value from sensors	*/
	START_SENSOR,		/**< 0x13 Enable robot to use sensors		*/
	STOP_SENSOR,		/**< 0x14 Disable robot to use sensors		*/
	SENSOR_STATUS,		/**< 0x15 Retrieve status of the sensors	*/
	READ_ALL			/**< 0x16 Scan with sensor on every angle 	*/
};

enum CMD_POS
{
	GET_REL_POS = 0x41,	/**< 0x41 Get relative position of robot	*/
	SET_REL_POS,		/**< 0x42 Set new relative position		*/
	GET_SPEED,			/**< 0x43 Get current speed of the robot	*/
	GET_ACCEL,			/**< 0x44 Get accelerometer data			*/
	GET_GYRO,			/**< 0x45 Get gyroscope data				*/
	IMU_STATUS			/**< 0x46 Retrieve status of the IMU		*/
};

enum CMD_MISC
{
	GET_FAILS = 0x51,	/**< 0x51 Get fail codes					*/
	BAT_PERCENT			/**< 0x52 Get battery percentage			*/
};

union int_to_bytes
{
	int32_t int_var;	/**< Signed int							*/
	uint8_t bytes[4];	/**< Split up signed int into 4 bytes	*/
};

union bytes_to_float
{
	float float_var;	/**< Float					   		*/
	uint8_t bytes[4];	/**< Split up float into 4 bytes 	*/
};

/* Prototypes */
void ITRP_CmdNumber(uint8_t cmd);
void ITRP_Drive(uint8_t cmd);
void ITRP_Sensor(uint8_t cmd);
void ITRP_Position(uint8_t cmd);
void ITRP_Miscellaneous(uint8_t cmd);
void ITRP_Run(void);

void ITRP_TransmitPacket(int args_num, ...);
int  ITRP_ReceivePacket(int *unpacked_data, uint8_t cmd);
int ITRP_ReceiveAllSensorData(int16_t *sensor_data, uint16_t timeout);
void ITRP_BuildPacket(uint8_t *cmd_ret, int args_num, va_list arguments);
void ITRP_ReadPacket(int *unpacked_data, uint8_t cmd);
int ITRP_RetransmitAllSensorData(int16_t *sensor_data);

/**< Command data structures for every type of command for rx. Every number represents the amount of bytes per command parameter */
static const uint8_t cmd_param_size_rx[][32] =
{
		{1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/**< 0x00 Drive commands		*/
		{1,2,2,2,2,2,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/**<  0x10 Sensor commands		*/
		{1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/**< 0x20 Sensor data commands	*/
		{1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/**< 0x30 Sensor data commands	*/
		{1,4,4,4,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/**< 0x40 Position commands		*/
		{1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1}	/**< 0x50 Misc commands			*/
};

/* Command data structures for every type of command for tx. Every number represents the amount of bytes per command parameter */
static const uint8_t cmd_param_size_tx[][32] =
{
		{1,1,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/* 0x00 Drive commands			*/
		{1,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/* 0x10 Sensor commands			*/
		{1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/* 0x20 Sensor data commands	*/
		{1,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/* 0x30 Sensor data commands	*/
		{1,4,4,4,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0},	/* 0x40 Position commands		*/
		{1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0}	/* 0x50 Misc commands			*/
};
#endif

/**
  * @}
  */
/**
  * @}
  */
/**
  * @}
  */
