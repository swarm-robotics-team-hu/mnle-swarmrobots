/*
Library:					NRF24L01/NRF24L01+
Written by:				Mohamed Yaqoob (MYaqoobEmbedded YouTube Channel)
Date Written:			10/11/2018
Last modified:		-/-
Description:			This is an STM32 device driver library for the NRF24L01 Nordic Radio transceiver, using STM HAL libraries

References:				This library was written based on the Arduino NRF24 Open-Source library by J. Coliz and the NRF24 datasheet
										- https://github.com/maniacbug/RF24
										- https://www.sparkfun.com/datasheets/Components/SMD/nRF24L01Pluss_Preliminary_Product_Specification_v1_0.pdf
										
* Copyright (C) 2018 - M. Yaqoob
   This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
   of the GNU General Public Licenseversion 3 as published by the Free Software Foundation.
	
   This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
   or indirectly by this software, read more about this on the GNU General Public License.
*/

#ifndef __NRF24_LIB_H
#define __NRF24_LIB_H

//List of header files  
#include "stm32f4xx_hal.h"   //** Change this according to your STM32 series **//
#include "nrf24l01.h"
#include "spi.h"
#include "stdbool.h"

#define PAYLOAD_SIZE 32		/**< Maximum size for a message */
#define NRF_CHANNEL 120		/**< The frequency channel (2520kHz)*/


//**** TypeDefs ****//
//1. Power Amplifier function, NRF24_setPALevel() 
typedef enum { 
	RF24_PA_m18dB = 0,
	RF24_PA_m12dB,
	RF24_PA_m6dB,
	RF24_PA_0dB,
	RF24_PA_ERROR 
}rf24_pa_dbm_e ;
//2. NRF24_setDataRate() input
typedef enum { 
	RF24_1MBPS = 0,
	RF24_2MBPS,
	RF24_250KBPS
}rf24_datarate_e;
//3. NRF24_setCRCLength() input
typedef enum { 
	RF24_CRC_DISABLED = 0,
	RF24_CRC_8,
	RF24_CRC_16
}rf24_crclength_e;
//4. Pipe address registers
static const uint8_t NRF24_ADDR_REGS[7] = {
		REG_RX_ADDR_P0,
		REG_RX_ADDR_P1,
		REG_RX_ADDR_P2,
		REG_RX_ADDR_P3,
		REG_RX_ADDR_P4,
		REG_RX_ADDR_P5,
		REG_TX_ADDR
};
//5. RX_PW_Px registers addresses
static const uint8_t RF24_RX_PW_PIPE[6] = {
		REG_RX_PW_P0, 
		REG_RX_PW_P1,
		REG_RX_PW_P2,
		REG_RX_PW_P3,
		REG_RX_PW_P4,
		REG_RX_PW_P5
};
//**** Functions prototypes ****//
//Microsecond delay function
void NRF24_DelayMicroSeconds(uint32_t uSec);

//1. Chip Select function
void NRF24_Csn(int mode);
//2. Chip Enable
void NRF24_Ce(int level);
//3. Read single byte from a register
uint8_t NRF24_ReadRegister(uint8_t reg);
//4. Read multiple bytes register
void NRF24_ReadRegisterN(uint8_t reg, uint8_t *buf, uint8_t len);
//5. Write single byte register
void NRF24_WriteRegister(uint8_t reg, uint8_t value);
//6. Write multipl bytes register
void NRF24_WriteRegisterN(uint8_t reg, const uint8_t* buf, uint8_t len);
//7. Write transmit payload
void NRF24_WritePayload(const void* buf, uint8_t len);
//8. Read receive payload
void NRF24_ReadPayload(void* buf, uint8_t len);
//9. Flush Tx buffer
void NRF24_FlushTx(void);
//10. Flush Rx buffer
void NRF24_FlushRx(void);
//11. Get status register value
uint8_t NRF24_GetStatus(void);

//12. Begin function
void NRF24_Begin(GPIO_TypeDef *nrf24PORT, uint16_t nrfCSN_Pin, uint16_t nrfCE_Pin, SPI_HandleTypeDef nrfSPI);
//13. Listen on open pipes for reading (Must call NRF24_openReadingPipe() first)
void NRF24_StartListening(void);
//14. Stop listening (essential before any write operation)
void NRF24_StopListening(void);

//15. Write(Transmit data), returns true if successfully sent
bool NRF24_Write( const void* buf, uint8_t len );
//16. Check for available data to read
bool NRF24_Available(void);
//17. Read received data
bool NRF24_Read( void* buf, uint8_t len );
//18. Open Tx pipe for writing (Cannot perform this while Listenning, has to call NRF24_stopListening)
void NRF24_OpenWritingPipe(uint64_t address);
//19. Open reading pipe
void NRF24_OpenReadingPipe(uint8_t number, uint64_t address);
//20 set transmit retries (rf24_Retries_e) and delay
void NRF24_SetRetries(uint8_t delay, uint8_t count);
//21. Set RF channel frequency
void NRF24_SetChannel(uint8_t channel);
//22. Set payload size
void NRF24_SetPayloadSize(uint8_t size);
//23. Get payload size
uint8_t NRF24_GetPayloadSize(void);
//24. Get dynamic payload size, of latest packet received
uint8_t NRF24_GetDynamicPayloadSize(void);
//25. Enable payload on Ackknowledge packet
void NRF24_EnableAckPayload(void);
//26. Enable dynamic payloads
void NRF24_EnableDynamicPayloads(void);
void NRF24_DisableDynamicPayloads(void);
//27. Check if module is NRF24L01+ or normal module
bool NRF24_IsNrfPlus(void) ;
//28. Set Auto Ack for all
void NRF24_SetAutoAck(bool enable);
//29. Set Auto Ack for certain pipe
void NRF24_SetAutoAckPipe( uint8_t pipe, bool enable ) ;
//30. Set transmit power level
void NRF24_SetPaLevel( rf24_pa_dbm_e level ) ;
//31. Get transmit power level
rf24_pa_dbm_e NRF24_GetPaLevel( void ) ;
//32. Set data rate (250 Kbps, 1Mbps, 2Mbps)
bool NRF24_SetDataRate(rf24_datarate_e speed);
//33. Get data rate
rf24_datarate_e NRF24_GetDataRate( void );
//34. Set crc length (disable, 8-bits or 16-bits)
void NRF24_SetCrcLength(rf24_crclength_e length);
//35. Get CRC length
rf24_crclength_e NRF24_GetCrcLength(void);
//36. Disable CRC
void NRF24_DisableCrc( void ) ;
//37. power up
void NRF24_PowerUp(void) ;
//38. power down
void NRF24_PowerDown(void);
//39. Check if data are available and on which pipe (Use this for multiple rx pipes)
bool NRF24_AvailablePipe(uint8_t* pipe_num);
//40. Start write (for IRQ mode)
void NRF24_StartWrite( const void* buf, uint8_t len );
//41. Write acknowledge payload
void NRF24_WriteAckPayload(uint8_t pipe, const void* buf, uint8_t len);
//42. Check if an Ack payload is available
bool NRF24_IsAckPayloadAvailable(void);
//43. Check interrupt flags
void NRF24_WhatHappened(bool *tx_ok,bool *tx_fail,bool *rx_ready);
//44. Reset Status
void NRF24_ResetStatus(void);
//45. ACTIVATE cmd
void NRF24_ActivateCmd(void);
//46. Get AckPayload Size
uint8_t NRF24_GetAckPayloadSize(void);

//**********  DEBUG Functions **********//
//1. Print radio settings
void PrintRadioSettings(void);
//2. Print Status 
void PrintStatusReg(void);
//3. Print Config 
void PrintConfigReg(void);
//4. Init Variables
void NRF24_DebugUartInit(UART_HandleTypeDef nrf24Uart);
//5. FIFO Status
void PrintFIFOstatus(void);

// Our functions
void NRF_Init(uint64_t TxAddress, uint64_t RxAddress);
void NRF_Transmit(uint8_t TxData[]);
#endif
