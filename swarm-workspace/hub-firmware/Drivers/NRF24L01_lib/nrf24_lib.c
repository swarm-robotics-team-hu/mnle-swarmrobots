/**
  ******************************************************************************
  * @file           : nrf24_lib.c
  * @brief          : Library for interpreting commands
  * Description		: This is an STM32 device driver library for the NRF24L01 Nordic Radio transceiver, using STM HAL libraries
  * References		: This library was written based on the Arduino NRF24 Open-Source library by J. Coliz and the NRF24 datasheet
  * 						- https://github.com/maniacbug/RF24
  * 						- https://www.sparkfun.com/datasheets/Components/SMD/nRF24L01Pluss_Preliminary_Product_Specification_v1_0.pdf
  * @author			: Swarm Robotics Team based on Mohamed Yaqoob (MYaqoobEmbedded YouTube Channel)
  * @date			: 08-11-2019
  ******************************************************************************
  */

/** @addtogroup HUB
  * @brief	All the software for the HUB
  * @{
  */

/** @addtogroup STM_Firmware
  * @brief	All the C software for the STM hardware
  * @{
  */

/** @addtogroup Wireless_communication
  * @brief	All code related to the interpreter
  * @{
  */

//List of header files
#include "nrf24_lib.h"
#include "stm32f4xx_it.h"
#include "main.h"


//*** Variables declaration ***//
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))
#define _BOOL(x) (((x)>0) ? 1:0)

//*** Library variables ***//
static uint64_t pipe0_reading_address;
static bool ack_payload_available; 		/**< Whether there is an ack payload waiting */
static uint8_t ack_payload_length; 		/**< Dynamic size of pending ack payload. */
static uint8_t payload_size; 			/**< Fixed size of payloads */
static bool dynamic_payloads_enabled; 	/**< Whether dynamic payloads are enabled. */
static bool p_variant; 					/* False for RF24L01 and true for RF24L01P */
static bool wide_band; 					/* 2Mbs data rate in use? */

//*** NRF24L01 pins and handles ***//
//CE and CSN pins
static GPIO_TypeDef	   *nrf24_PORT;
static uint16_t			nrf24_CSN_PIN;
static uint16_t			nrf24_CE_PIN;
//SPI handle
static SPI_HandleTypeDef nrf24_hspi;
//Debugging UART handle
static UART_HandleTypeDef nrf24_huart;

//**** Functions prototypes ****//
/*******************************************************************************************************//**
 * @brief This function makes a delay in microseconds
 *
 * @param u_sec	:	The amout in microseconds for the delay
 *******************************************************************************************************/
void NRF24_DelayMicroSeconds(uint32_t u_sec)
{
	uint32_t tick_us = htim2.Instance->CNT + u_sec;
	if(tick_us > 1000000)
		tick_us = tick_us - 1000000;

	while(htim2.Instance->CNT != tick_us);
}

/*******************************************************************************************************//**
 * @brief This function is for enabling the chip select
 *
 * @param state	:	The state for the chip select pin (1 or 0)
 *******************************************************************************************************/
void NRF24_Csn(int state)
{
	if(state) HAL_GPIO_WritePin(nrf24_PORT, nrf24_CSN_PIN, GPIO_PIN_SET);
	else HAL_GPIO_WritePin(nrf24_PORT, nrf24_CSN_PIN, GPIO_PIN_RESET);
}

/*******************************************************************************************************//**
 * @brief This function is for setting the chip enable pin
 *
 * @param state	:	The state for the chip enable pin (1 or 0)
 *******************************************************************************************************/
void NRF24_Ce(int state)
{
	if(state) HAL_GPIO_WritePin(nrf24_PORT, nrf24_CE_PIN, GPIO_PIN_SET);
	else HAL_GPIO_WritePin(nrf24_PORT, nrf24_CE_PIN, GPIO_PIN_RESET);
}

/*******************************************************************************************************//**
 * @brief This function is for reading a single byte from a register
 *
 * @param reg	:	The selected register
 *******************************************************************************************************/
uint8_t NRF24_ReadRegister(uint8_t reg)
{
	uint8_t spiBuf[3];
	uint8_t retData;
	//Put CSN low
	NRF24_Csn(0);
	//Transmit register address
	spiBuf[0] = reg&0x1F;
	HAL_SPI_Transmit(&nrf24_hspi, spiBuf, 1, 100);
	//Receive data
	HAL_SPI_Receive(&nrf24_hspi, &spiBuf[1], 1, 100);
	retData = spiBuf[1];
	//Bring CSN high
	NRF24_Csn(1);
	return retData;
}

/*******************************************************************************************************//**
 * @brief This function is for reading a multiple bytes from a register
 *
 * @param reg	:	The selected register
 * @param buf	:	The buffer for the mutiple bytes
 * @param len	:	Length of buf
 *******************************************************************************************************/
void NRF24_ReadRegisterN(uint8_t reg, uint8_t *buf, uint8_t len)
{
	uint8_t spiBuf[3];
	//Put CSN low
	NRF24_Csn(0);
	//Transmit register address
	spiBuf[0] = reg&0x1F;
	//spiStatus = NRF24_SPI_Write(spiBuf, 1);
	HAL_SPI_Transmit(&nrf24_hspi, spiBuf, 1, 100);
	//Receive data
	HAL_SPI_Receive(&nrf24_hspi, buf, len, 100);
	//Bring CSN high
	NRF24_Csn(1);
}

/*******************************************************************************************************//**
 * @brief This function is for writing a single byte to a register
 *
 * @param reg	:	The selected register
 * @param value	:	The value for the register
 *******************************************************************************************************/
void NRF24_WriteRegister(uint8_t reg, uint8_t value)
{
	uint8_t spiBuf[3];
	//Put CSN low
	NRF24_Csn(0);
	//Transmit register address and data
	spiBuf[0] = reg|0x20;
	spiBuf[1] = value;
	HAL_SPI_Transmit(&nrf24_hspi, spiBuf, 2, 100);
	//Bring CSN high
	NRF24_Csn(1);
}

/*******************************************************************************************************//**
 * @brief This function is for writing multiple bytes to a register
 *
 * @param reg	:	The selected register
 * @param buf	:	The values for the register
 * @param len	: 	Length from buf
 *******************************************************************************************************/
void NRF24_WriteRegisterN(uint8_t reg, const uint8_t* buf, uint8_t len)
{
	uint8_t spiBuf[3];
	//Put CSN low
	NRF24_Csn(0);
	//Transmit register address and data
	spiBuf[0] = reg|0x20;
	HAL_SPI_Transmit(&nrf24_hspi, spiBuf, 1, 100);
	HAL_SPI_Transmit(&nrf24_hspi, (uint8_t*)buf, len, 100);
	//Bring CSN high
	NRF24_Csn(1);
}

/*******************************************************************************************************//**
 * @brief This function is for writing the payload
 *
 * @param buf	:	The buffer with the tx payload data
 * @param len	:	Length of buf
 *******************************************************************************************************/
void NRF24_WritePayload(const void* buf, uint8_t len)
{
	uint8_t wrPayloadCmd;
	//Bring CSN low
	NRF24_Csn(0);
	//Send Write Tx payload command followed by pbuf data
	wrPayloadCmd = CMD_W_TX_PAYLOAD;
	HAL_SPI_Transmit(&nrf24_hspi, &wrPayloadCmd, 1, 100);
	HAL_SPI_Transmit(&nrf24_hspi, (uint8_t *)buf, len, 100);
	//Bring CSN high
	NRF24_Csn(1);
}

/*******************************************************************************************************//**
 * @brief This function is for reading the payload
 *
 * @param buf	:	The buffer with the tx payload data
 * @param len	:	Length of buf
 *******************************************************************************************************/
void NRF24_ReadPayload(void* buf, uint8_t len)
{
	uint8_t cmdRxBuf;
	//Get data length using payload size
	uint8_t data_len = MIN(len, NRF24_GetPayloadSize());
	//Read data from Rx payload buffer
	NRF24_Csn(0);
	cmdRxBuf = CMD_R_RX_PAYLOAD;
	HAL_SPI_Transmit(&nrf24_hspi, &cmdRxBuf, 1, 100);
	HAL_SPI_Receive(&nrf24_hspi, buf, data_len, 100);
	NRF24_Csn(1);
}

/*******************************************************************************************************//**
 * @brief This function is flushing the tx
 *******************************************************************************************************/
void NRF24_FlushTx(void)
{
	NRF24_WriteRegister(CMD_FLUSH_TX, 0xFF);
}

/*******************************************************************************************************//**
 * @brief This function is flushing the rx
 *******************************************************************************************************/
void NRF24_FlushRx(void)
{
	NRF24_WriteRegister(CMD_FLUSH_RX, 0xFF);
}
/*******************************************************************************************************//**
 * @brief This function is for getting the status from the NRF24L01
 *
 * @retval statReg	:	The values from the register (status)
 *******************************************************************************************************/
uint8_t NRF24_GetStatus(void)
{
	uint8_t statReg;
	statReg = NRF24_ReadRegister(REG_STATUS);
	return statReg;
}

/*******************************************************************************************************//**
 * @brief This function will start the SPI connection witht the NRF24L01 and will setup all the pins
 *
 * @param nrf24PORT			:	The port is the CSN and CE pin
 * @param nrfCSN_Pin		:	The pin where CSN pin is connected
 * @param nrfCE_Pin			:	The pin where CE pin is connected
 * @param SPI_HandleTypeDef	:	Handler for SPI commands
 *******************************************************************************************************/
void NRF24_Begin(GPIO_TypeDef *nrf24PORT, uint16_t nrfCSN_Pin, uint16_t nrfCE_Pin, SPI_HandleTypeDef nrfSPI)
{
	//Copy SPI handle variable
	memcpy(&nrf24_hspi, &nrfSPI, sizeof(nrfSPI));
	//Copy Pins and Port variables
	nrf24_PORT = nrf24PORT;
	nrf24_CSN_PIN = nrfCSN_Pin;
	nrf24_CE_PIN = nrfCE_Pin;
	
	//Put pins to idle state
	NRF24_Csn(1);
	NRF24_Ce(0);
	//5 ms initial delay
	HAL_Delay(5);
	
	//**** Soft Reset Registers default values ****//
	NRF24_WriteRegister(0x00, 0x08);
	NRF24_WriteRegister(0x01, 0x3f);
	NRF24_WriteRegister(0x02, 0x03);
	NRF24_WriteRegister(0x03, 0x03);
	NRF24_WriteRegister(0x04, 0x03);
	NRF24_WriteRegister(0x05, 0x02);
	NRF24_WriteRegister(0x06, 0x0f);
	NRF24_WriteRegister(0x07, 0x0e);
	NRF24_WriteRegister(0x08, 0x00);
	NRF24_WriteRegister(0x09, 0x00);
	uint8_t pipeAddrVar[6];
	pipeAddrVar[4]=0xE7; pipeAddrVar[3]=0xE7; pipeAddrVar[2]=0xE7; pipeAddrVar[1]=0xE7; pipeAddrVar[0]=0xE7; 
	NRF24_WriteRegisterN(0x0A, pipeAddrVar, 5);
	pipeAddrVar[4]=0xC2; pipeAddrVar[3]=0xC2; pipeAddrVar[2]=0xC2; pipeAddrVar[1]=0xC2; pipeAddrVar[0]=0xC2; 
	NRF24_WriteRegisterN(0x0B, pipeAddrVar, 5);
	NRF24_WriteRegister(0x0C, 0xC3);
	NRF24_WriteRegister(0x0D, 0xC4);
	NRF24_WriteRegister(0x0E, 0xC5);
	NRF24_WriteRegister(0x0F, 0xC6);
	pipeAddrVar[4]=0xE7; pipeAddrVar[3]=0xE7; pipeAddrVar[2]=0xE7; pipeAddrVar[1]=0xE7; pipeAddrVar[0]=0xE7; 
	NRF24_WriteRegisterN(0x10, pipeAddrVar, 5);
	NRF24_WriteRegister(0x11, 0);
	NRF24_WriteRegister(0x12, 0);
	NRF24_WriteRegister(0x13, 0);
	NRF24_WriteRegister(0x14, 0);
	NRF24_WriteRegister(0x15, 0);
	NRF24_WriteRegister(0x16, 0);
	
	NRF24_ActivateCmd();
	NRF24_WriteRegister(0x1c, 0);
	NRF24_WriteRegister(0x1d, 0);
	PrintRadioSettings();
	//Initialise retries 15 and delay 1250 usec
	NRF24_SetRetries(15, 15);
	//Initialise PA level to max (0dB)
	NRF24_SetPaLevel(RF24_PA_0dB);
	//Initialise data rate to 1Mbps
	NRF24_SetDataRate(RF24_2MBPS);
	//Initalise CRC length to 16-bit (2 bytes)
	NRF24_SetCrcLength(RF24_CRC_16);
	//Disable dynamic payload
	NRF24_DisableDynamicPayloads();
	//Set payload size
	NRF24_SetPayloadSize(32);
	
	//Reset status register
	NRF24_ResetStatus();
	//Initialise channel to 76
	NRF24_SetChannel(76);
	//Flush buffers
	NRF24_FlushTx();
	NRF24_FlushRx();
	
	NRF24_PowerDown();
	
}

/*******************************************************************************************************//**
 * @brief This function listens on open pipes for reading (Must call NRF24_OpenReadingPipe() first)
 *******************************************************************************************************/
void NRF24_StartListening(void)
{
	//Power up and set to RX mode
	NRF24_WriteRegister(REG_CONFIG, NRF24_ReadRegister(REG_CONFIG) | (1UL<<1) |(1UL <<0));
	//Restore pipe 0 address if exists
	if(pipe0_reading_address)
		NRF24_WriteRegisterN(REG_RX_ADDR_P0, (uint8_t *)(&pipe0_reading_address), 5);
	
	//Flush buffers
	NRF24_FlushTx();
	NRF24_FlushRx();
	//Set CE HIGH to start listenning
	NRF24_Ce(1);
	//Wait for 130 uSec for the radio to come on
	NRF24_DelayMicroSeconds(150);
	NRF24_WriteRegister(REG_CONFIG, 0x2F);	// Set IRQ on in Config register for receiving
}

/*******************************************************************************************************//**
 * @brief This function stop listening (essential before any write operation)
 *******************************************************************************************************/
void NRF24_StopListening(void)
{
	NRF24_Ce(0);
	NRF24_FlushTx();
	NRF24_FlushRx();
	NRF24_WriteRegister(REG_CONFIG, 0x7E);	// Set IRQ off in Config register for transmitting
}

/*******************************************************************************************************//**
 * @brief This function writes(Transmit data), returns true if successfully sent
 *
 * @param buf			:	The buffer with the Transmit data
 * @param len			:	Length of buf
 *
 * @retval ret_status	:	The status from the tx register
 *******************************************************************************************************/
bool NRF24_Write( const void* buf, uint8_t len )
{
	bool ret_status;
	//Start writing
	NRF24_ResetStatus();
	NRF24_StartWrite(buf,len);
	//Data monitor
  uint8_t observe_tx;
  uint8_t status;
  uint32_t sent_at = HAL_GetTick();
	const uint32_t timeout = 10; //ms to wait for timeout
	do
  {
    NRF24_ReadRegisterN(REG_OBSERVE_TX,&observe_tx,1);
		//Get status register
		status = NRF24_GetStatus();
  }
  while( ! ( status & ( _BV(BIT_TX_DS) | _BV(BIT_MAX_RT) ) ) && ( HAL_GetTick() - sent_at < timeout ) );
	
	//PrintConfigReg();
	//PrintStatusReg();
	
	bool tx_ok, tx_fail;
  NRF24_WhatHappened(&tx_ok,&tx_fail, &ack_payload_available);
	ret_status = tx_ok;
	if ( ack_payload_available )
  {
    ack_payload_length = NRF24_GetDynamicPayloadSize();
	}
	
	//Power down
	NRF24_Available();
	NRF24_FlushTx();
	return ret_status;
}

/*******************************************************************************************************//**
 * @brief This function checks for available data to read
 *******************************************************************************************************/
bool NRF24_Available(void)
{
	return NRF24_AvailablePipe(NULL);
}

/*******************************************************************************************************//**
 * @brief This function reads the incoming data
 *
 * @param buf			:	The buffer with the read data
 * @param len			:	Length of buf
 *
 * @retval rx_status	:	The status from the rx register
 *******************************************************************************************************/
bool NRF24_Read( void* buf, uint8_t len )
{
	NRF24_ReadPayload( buf, len );
	uint8_t rx_status = NRF24_ReadRegister(REG_FIFO_STATUS) & _BV(BIT_RX_EMPTY);
	NRF24_FlushRx();
	NRF24_GetDynamicPayloadSize();
	return rx_status;
}

/*************************************************************************************************************************//**
 * @brief This function opens Tx pipe for writing (Cannot perform this while Listenning, has to call NRF24_StopListening)
 *
 * @param address	:	The addres where you want to write
 *************************************************************************************************************************/
void NRF24_OpenWritingPipe(uint64_t address)
{
	NRF24_WriteRegisterN(REG_RX_ADDR_P0, (uint8_t *)(&address), 5);
	NRF24_WriteRegisterN(REG_TX_ADDR, (uint8_t *)(&address), 5);
	const uint8_t max_payload_size = 32;
	NRF24_WriteRegister(REG_RX_PW_P0,MIN(payload_size,max_payload_size));
}

/*******************************************************//**
 * @brief This function opens a reading pipe
 *
 * @param number	:	On which pipe you want to read
 * @param address	:	The address where you want to read
 ******************************************************/
void NRF24_OpenReadingPipe(uint8_t number, uint64_t address)
{
	if (number == 0)
    pipe0_reading_address = address;
	
	if(number <= 6)
	{
		if(number < 2)
		{
			//Address width is 5 bytes
			NRF24_WriteRegisterN(NRF24_ADDR_REGS[number], (uint8_t *)(&address), 5);
		}
		else
		{
			NRF24_WriteRegisterN(NRF24_ADDR_REGS[number], (uint8_t *)(&address), 1);
		}
		//Write payload size
		NRF24_WriteRegister(RF24_RX_PW_PIPE[number],payload_size);
		//Enable pipe
		NRF24_WriteRegister(REG_EN_RXADDR, NRF24_ReadRegister(REG_EN_RXADDR) | _BV(number));
	}
	
}

/*******************************************************************************************************//**
 * @brief This function sets transmit retries (rf24_Retries_e) and delay
 *
 * @param delay			:	How big is the delay after each retransmit
 * @param count			:	How many retransmits
 *******************************************************************************************************/
void NRF24_SetRetries(uint8_t delay, uint8_t count)
{
	NRF24_WriteRegister(REG_SETUP_RETR,(delay&0xf)<<BIT_ARD | (count&0xf)<<BIT_ARC);
}

/*******************************************************************************************************//**
 * @brief This function sets RF channel frequency
 *
 * @param channel	:	The frequency channel
 *******************************************************************************************************/
void NRF24_SetChannel(uint8_t channel)
{
	const uint8_t max_channel = 127;
	NRF24_WriteRegister(REG_RF_CH,MIN(channel,max_channel));
}

/*******************************************************************************************************//**
 * @brief This function sets the payload size
 *
 * @param size	: Payload size
 *******************************************************************************************************/
void NRF24_SetPayloadSize(uint8_t size)
{
	const uint8_t max_payload_size = 32;
	payload_size = MIN(size,max_payload_size);
}

/*******************************************************************************************************//**
 * @brief This function gets payload size
 *
 * @retval payload_size	: The payload size
 *******************************************************************************************************/
uint8_t NRF24_GetPayloadSize(void)
{
	return payload_size;
}

/*******************************************************************************************************//**
 * @brief This function gets dynamic payload size, of latest packet received
 *
 * @retval : The dynamic payload size
 *******************************************************************************************************/
uint8_t NRF24_GetDynamicPayloadSize(void)
{
	return NRF24_ReadRegister(CMD_R_RX_PL_WID);
}

/*******************************************************************************************************//**
 * @brief This function enables the payload on Ackknowledge packet
 *******************************************************************************************************/
void NRF24_EnableAckPayload(void)
{
	//Need to enable dynamic payload and Ack payload together
	 NRF24_WriteRegister(REG_FEATURE,NRF24_ReadRegister(REG_FEATURE) | _BV(BIT_EN_ACK_PAY) | _BV(BIT_EN_DPL) );
	if(!NRF24_ReadRegister(REG_FEATURE))
	{
		NRF24_ActivateCmd();
		NRF24_WriteRegister(REG_FEATURE,NRF24_ReadRegister(REG_FEATURE) | _BV(BIT_EN_ACK_PAY) | _BV(BIT_EN_DPL) );
	}
	// Enable dynamic payload on pipes 0 & 1
	NRF24_WriteRegister(REG_DYNPD,NRF24_ReadRegister(REG_DYNPD) | _BV(BIT_DPL_P1) | _BV(BIT_DPL_P0));
}

/*******************************************************************************************************//**
 * @brief This function enables the dynamic payload
 *******************************************************************************************************/
void NRF24_EnableDynamicPayloads(void)
{
	//Enable dynamic payload through FEATURE register
	NRF24_WriteRegister(REG_FEATURE,NRF24_ReadRegister(REG_FEATURE) |  _BV(BIT_EN_DPL) );
	if(!NRF24_ReadRegister(REG_FEATURE))
	{
		NRF24_ActivateCmd();
		NRF24_WriteRegister(REG_FEATURE,NRF24_ReadRegister(REG_FEATURE) |  _BV(BIT_EN_DPL) );
	}
	//Enable Dynamic payload on all pipes
	NRF24_WriteRegister(REG_DYNPD,NRF24_ReadRegister(REG_DYNPD) | _BV(BIT_DPL_P5) | _BV(BIT_DPL_P4) | _BV(BIT_DPL_P3) | _BV(BIT_DPL_P2) | _BV(BIT_DPL_P1) | _BV(BIT_DPL_P0));
  dynamic_payloads_enabled = true;
	
}

/*******************************************************************************************************//**
 * @brief This function disables the dynamic payload
 *******************************************************************************************************/
void NRF24_DisableDynamicPayloads(void)
{
	NRF24_WriteRegister(REG_FEATURE,NRF24_ReadRegister(REG_FEATURE) &  ~(_BV(BIT_EN_DPL)) );
	//Disable for all pipes 
	NRF24_WriteRegister(REG_DYNPD,0);
	dynamic_payloads_enabled = false;
}

/*******************************************************************************************************//**
 * @brief This function checks if module is NRF24L01+ or normal moduledisables the dynamic payload
 *
 * @retval	p_variant	:	Give back the version of the NRF24L01 or NRF24LO1+
 *******************************************************************************************************/
bool NRF24_IsNrfPlus(void)
{
	return p_variant;
}

/*******************************************************************************************************//**
 * @brief This function sets Auto Ack for all
 *
 * @param enable	: Value for setting or resetting the pin
 *******************************************************************************************************/
void NRF24_SetAutoAck(bool enable)
{
	if ( enable )
    NRF24_WriteRegister(REG_EN_AA, 0x3F);
  else
    NRF24_WriteRegister(REG_EN_AA, 0x00);
}

/*******************************************************************************************************//**
 * @brief This function sets Auto Ack for certain pipe
 *
 * @param	pipe	:	Select the pipe
 * @param 	enable 	:	Value for setting or resetting the pin
 *******************************************************************************************************/
void NRF24_SetAutoAckPipe( uint8_t pipe, bool enable )
{
	if ( pipe <= 6 )
  {
    uint8_t en_aa = NRF24_ReadRegister( REG_EN_AA ) ;
    if( enable )
    {
      en_aa |= _BV(pipe) ;
    }
    else
    {
      en_aa &= ~_BV(pipe) ;
    }
    NRF24_WriteRegister( REG_EN_AA, en_aa ) ;
  }
}

/*******************************************************************************************************//**
 * @brief This function sets transmit power level
 *
 * @param	level	:	The level of transmitting power
 *******************************************************************************************************/
void NRF24_SetPaLevel( rf24_pa_dbm_e level )
{
	uint8_t setup = NRF24_ReadRegister(REG_RF_SETUP) ;
  setup &= ~(_BV(RF_PWR_LOW) | _BV(RF_PWR_HIGH)) ;

  // switch uses RAM (evil!)
  if ( level == RF24_PA_0dB)
  {
    setup |= (_BV(RF_PWR_LOW) | _BV(RF_PWR_HIGH)) ;
  }
  else if ( level == RF24_PA_m6dB )
  {
    setup |= _BV(RF_PWR_HIGH) ;
  }
  else if ( level == RF24_PA_m12dB )
  {
    setup |= _BV(RF_PWR_LOW);
  }
  else if ( level == RF24_PA_m18dB )
  {
    // nothing
  }
  else if ( level == RF24_PA_ERROR )
  {
    // On error, go to maximum PA
    setup |= (_BV(RF_PWR_LOW) | _BV(RF_PWR_HIGH)) ;
  }

  NRF24_WriteRegister( REG_RF_SETUP, setup ) ;
}

/*******************************************************************************************************//**
 * @brief This function gets transmit power level
 *
 * @retval	result	:	The level of transmitting power
 *******************************************************************************************************/
rf24_pa_dbm_e NRF24_GetPaLevel( void )
{
	rf24_pa_dbm_e result = RF24_PA_ERROR ;
  uint8_t power = NRF24_ReadRegister(REG_RF_SETUP) & (_BV(RF_PWR_LOW) | _BV(RF_PWR_HIGH));

  // switch uses RAM (evil!)
  if ( power == (_BV(RF_PWR_LOW) | _BV(RF_PWR_HIGH)) )
  {
    result = RF24_PA_0dB ;
  }
  else if ( power == _BV(RF_PWR_HIGH) )
  {
    result = RF24_PA_m6dB ;
  }
  else if ( power == _BV(RF_PWR_LOW) )
  {
    result = RF24_PA_m12dB ;
  }
  else
  {
    result = RF24_PA_m18dB ;
  }

  return result ;
}

/*******************************************************************************************************//**
 * @brief This function sets data rate (250 Kbps, 1Mbps, 2Mbps)
 *
 * @param	speed	:	Speed of the data rate
 *
 * @retval	result	:	Returns true when it worked and false when there was an error
 *******************************************************************************************************/
bool NRF24_SetDataRate(rf24_datarate_e speed)
{
  bool result = false;
  uint8_t setup = NRF24_ReadRegister(REG_RF_SETUP) ;

  // HIGH and LOW '00' is 1Mbs - our default
  wide_band = false ;
  setup &= ~(_BV(RF_DR_LOW) | _BV(RF_DR_HIGH)) ;
  if( speed == RF24_250KBPS )
  {
    // Must set the RF_DR_LOW to 1; RF_DR_HIGH (used to be RF_DR) is already 0
    // Making it '10'.
    wide_band = false ;
    setup |= _BV( RF_DR_LOW ) ;
  }
  else
  {
    // Set 2Mbs, RF_DR (RF_DR_HIGH) is set 1
    // Making it '01'
    if ( speed == RF24_2MBPS )
    {
      wide_band = true ;
      setup |= _BV(RF_DR_HIGH);
    }
    else
    {
      // 1Mbs
      wide_band = false ;
    }
  }
  NRF24_WriteRegister(REG_RF_SETUP,setup);

  // Verify our result
  if ( NRF24_ReadRegister(REG_RF_SETUP) == setup )
  {
    result = true;
  }
  else
  {
    wide_band = false;
  }

  return result;
}

/*******************************************************************************************************//**
 * @brief This function gets data rate (250 Kbps, 1Mbps, 2Mbps)
 *
 * @retval	result	:	Speed of the data rate
 *******************************************************************************************************/
rf24_datarate_e NRF24_getDataRate( void )
{
	rf24_datarate_e result ;
  uint8_t dr = NRF24_ReadRegister(REG_RF_SETUP) & (_BV(RF_DR_LOW) | _BV(RF_DR_HIGH));
  
  // switch uses RAM (evil!)
  // Order matters in our case below
  if ( dr == _BV(RF_DR_LOW) )
  {
    // '10' = 250KBPS
    result = RF24_250KBPS ;
  }
  else if ( dr == _BV(RF_DR_HIGH) )
  {
    // '01' = 2MBPS
    result = RF24_2MBPS ;
  }
  else
  {
    // '00' = 1MBPS
    result = RF24_1MBPS ;
  }
  return result ;
}

/*******************************************************************************************************//**
 * @brief This function sets crc length (disable, 8-bits or 16-bits)
 *
 * @param	length	:	The crc length
 *******************************************************************************************************/
void NRF24_SetCrcLength(rf24_crclength_e length)
{
	uint8_t config = NRF24_ReadRegister(REG_CONFIG) & ~( _BV(BIT_CRCO) | _BV(BIT_EN_CRC)) ;
  
  // switch uses RAM
  if ( length == RF24_CRC_DISABLED )
  {
    // Do nothing, we turned it off above. 
  }
  else if ( length == RF24_CRC_8 )
  {
    config |= _BV(BIT_EN_CRC);
  }
  else
  {
    config |= _BV(BIT_EN_CRC);
    config |= _BV( BIT_CRCO );
  }
  NRF24_WriteRegister( REG_CONFIG, config );
}

/*******************************************************************************************************//**
 * @brief This function gets crc length (disable, 8-bits or 16-bits)
 *
 * @param	result	:	The crc length
 *******************************************************************************************************/
rf24_crclength_e NRF24_GetCrcLength(void)
{
	rf24_crclength_e result = RF24_CRC_DISABLED;
  uint8_t config = NRF24_ReadRegister(REG_CONFIG) & ( _BV(BIT_CRCO) | _BV(BIT_EN_CRC)) ;

  if ( config & _BV(BIT_EN_CRC ) )
  {
    if ( config & _BV(BIT_CRCO) )
      result = RF24_CRC_16;
    else
      result = RF24_CRC_8;
  }

  return result;
}

/*******************************************************************************************************//**
 * @brief This function disables crc
 *******************************************************************************************************/
void NRF24_DisableCrc( void )
{
	uint8_t disable = NRF24_ReadRegister(REG_CONFIG) & ~_BV(BIT_EN_CRC) ;
  NRF24_WriteRegister( REG_CONFIG, disable ) ;
}

/*******************************************************************************************************//**
 * @brief This function sets power up
 *******************************************************************************************************/
void NRF24_PowerUp(void)
{
	NRF24_WriteRegister(REG_CONFIG,NRF24_ReadRegister(REG_CONFIG) | _BV(BIT_PWR_UP));
}

/*******************************************************************************************************//**
 * @brief This function sets power down
 *******************************************************************************************************/
void NRF24_PowerDown(void)
{
	NRF24_WriteRegister(REG_CONFIG,NRF24_ReadRegister(REG_CONFIG) & ~_BV(BIT_PWR_UP));
}

/*******************************************************************************************************//**
 * @brief This function checks if data are available and on which pipe (Use this for multiple rx pipes)
 *
 * @param pipe_num	:	The Pipe you want to check
 *
 * @retval result	:	Returns the result
 *******************************************************************************************************/
bool NRF24_AvailablePipe(uint8_t* pipe_num)
{
	uint8_t status = NRF24_GetStatus();

  bool result = ( status & _BV(BIT_RX_DR) );

  if (result)
  {
    // If the caller wants the pipe number, include that
    if ( pipe_num )
      *pipe_num = ( status >> BIT_RX_P_NO ) & 0x7;

    // Clear the status bit
    NRF24_WriteRegister(REG_STATUS,_BV(BIT_RX_DR) );

    // Handle ack payload receipt
    if ( status & _BV(BIT_TX_DS) )
    {
      NRF24_WriteRegister(REG_STATUS,_BV(BIT_TX_DS));
    }
  }
  return result;
}

/*******************************************************************************************************//**
 * @brief This function starts write (for IRQ mode)
 *
 * @param buf	:	The buffer for the payload
 * @param len	:	Length of buf
 *******************************************************************************************************/
void NRF24_StartWrite( const void* buf, uint8_t len )
{
	// Transmitter power-up
  NRF24_WriteRegister(REG_CONFIG, ( NRF24_ReadRegister(REG_CONFIG) | _BV(BIT_PWR_UP) ) & ~_BV(BIT_PRIM_RX) );
  NRF24_DelayMicroSeconds(150);

  // Send the payload
  NRF24_WritePayload( buf, len );

  // Enable Tx for 15usec
  NRF24_Ce(1);
  HAL_Delay(2);
  NRF24_Ce(0);
}

/*******************************************************************************************************//**
 * @brief This function writes acknowledge payload
 *
 * @param pipe	:	The selected pipe
 * @param buf	:	The buffer
 * @param len	:	Length of the buffer
 *******************************************************************************************************/
void NRF24_WriteAckPayload(uint8_t pipe, const void* buf, uint8_t len)
{
	const uint8_t* current = (uint8_t *)buf;
	const uint8_t max_payload_size = 32;
  uint8_t data_len = MIN(len,max_payload_size);
	
  NRF24_Csn(0);
	NRF24_WriteRegisterN(CMD_W_ACK_PAYLOAD | ( pipe & 0x7 ) , current, data_len);
  NRF24_Csn(1);
}

/*******************************************************************************************************//**
 * @brief This function checks if an Ack payload is available
 *
 * @retval result	:	Return result
 *******************************************************************************************************/
bool NRF24_IsAckPayloadAvailable(void)
{
	bool result = ack_payload_available;
  ack_payload_available = false;
  return result;
}

/*******************************************************************************************************//**
 * @brief This function checks interrupt flags
 *
 * @param tx_ok		: 	Set Tx_ok bit
 * @param tx_fail	:	Set Tx_fail bit
 * @param tx_ready	:	Set Tx_ready bit
 *******************************************************************************************************/
void NRF24_WhatHappened(bool *tx_ok, bool *tx_fail, bool *rx_ready)
{
	uint8_t status = NRF24_GetStatus();
	*tx_ok = 0;
	NRF24_WriteRegister(REG_STATUS,_BV(BIT_RX_DR) | _BV(BIT_TX_DS) | _BV(BIT_MAX_RT) );
  // Report to the user what happened
  *tx_ok = status & _BV(BIT_TX_DS);
  *tx_fail = status & _BV(BIT_MAX_RT);
  *rx_ready = status & _BV(BIT_RX_DR);
}

/****************************************//**
 * @brief This function reset the status
 ****************************************/
void NRF24_ResetStatus(void)
{
	NRF24_WriteRegister(REG_STATUS,_BV(BIT_RX_DR) | _BV(BIT_TX_DS) | _BV(BIT_MAX_RT) );
}

/****************************************//**
 * @brief This function activate cmd
 ****************************************/
void NRF24_ActivateCmd(void)
{
	uint8_t cmdRxBuf[2];
	//Read data from Rx payload buffer
	NRF24_Csn(0);
	cmdRxBuf[0] = CMD_ACTIVATE;
	cmdRxBuf[1] = 0x73;
	HAL_SPI_Transmit(&nrf24_hspi, cmdRxBuf, 2, 100);
	NRF24_Csn(1);
}

/****************************************//**
 * @brief This function gets AckPayload Size
 *
 * @retval ack_payload_length	:	The length of the ack payload
 ****************************************/
uint8_t NRF24_GetAckPayloadSize(void)
{
	return ack_payload_length;
}
/*****************************************************//**
 * @brief This function prints all the radio settings
 *****************************************************/
void PrintRadioSettings(void)
{
	uint8_t reg8Val;
	char uartTxBuf[100];
	sprintf(uartTxBuf, "\r\n**********************************************\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	//a) Get CRC settings - Config Register
	reg8Val = NRF24_ReadRegister(0x00);
	if(reg8Val & (1 << 3))
	{
		if(reg8Val & (1 << 2)) sprintf(uartTxBuf, "CRC:		Enabled, 2 Bytes \r\n");
		else sprintf(uartTxBuf, "CRC:		Enabled, 1 Byte \r\n");
	}
	else
	{
		sprintf(uartTxBuf, "CRC:		Disabled \r\n");
	}
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	//b) AutoAck on pipes
	reg8Val = NRF24_ReadRegister(0x01);
	sprintf(uartTxBuf, "ENAA:		P0:	%d\r\n		P1:	%d\r\n		P2:	%d\r\n		P3:	%d\r\n		P4:	%d\r\n		P5:	%d\r\n\r\n",
	_BOOL(reg8Val&(1<<0)), _BOOL(reg8Val&(1<<1)), _BOOL(reg8Val&(1<<2)), _BOOL(reg8Val&(1<<3)), _BOOL(reg8Val&(1<<4)), _BOOL(reg8Val&(1<<5)));
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	//c) Enabled Rx addresses
	reg8Val = NRF24_ReadRegister(0x02);
	sprintf(uartTxBuf, "EN_RXADDR:	P0:	%d\r\n		P1:	%d\r\n		P2:	%d\r\n		P3:	%d\r\n		P4:	%d\r\n		P5:	%d\r\n\r\n",
	_BOOL(reg8Val&(1<<0)), _BOOL(reg8Val&(1<<1)), _BOOL(reg8Val&(1<<2)), _BOOL(reg8Val&(1<<3)), _BOOL(reg8Val&(1<<4)), _BOOL(reg8Val&(1<<5)));
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	//d) Address width
	reg8Val = NRF24_ReadRegister(0x03)&0x03;
	reg8Val +=2;
	sprintf(uartTxBuf, "SETUP_AW:	%d bytes \r\n", reg8Val);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	//e) RF channel
	reg8Val = NRF24_ReadRegister(0x05);
	sprintf(uartTxBuf, "RF_CH:		%d CH \r\n", reg8Val&0x7F);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	//f) Data rate & RF_PWR
	reg8Val = NRF24_ReadRegister(0x06);
	if(reg8Val & (1 << 3)) sprintf(uartTxBuf, "Data Rate:	2Mbps \r\n");
	else sprintf(uartTxBuf, "Data Rate:	1Mbps \r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	reg8Val &= (3 << 1);
	reg8Val = (reg8Val>>1);
	if(reg8Val == 0) sprintf(uartTxBuf, "RF_PWR:		-18dB \r\n\r\n");
	else if(reg8Val == 1) sprintf(uartTxBuf, "RF_PWR:		-12dB \r\n\r\n");
	else if(reg8Val == 2) sprintf(uartTxBuf, "RF_PWR:		-6dB \r\n\r\n");
	else if(reg8Val == 3) sprintf(uartTxBuf, "RF_PWR:		0dB \r\n\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);

	uint8_t pipeAddrs[6];
	NRF24_ReadRegisterN(0x0A+6, pipeAddrs, 5);
	sprintf(uartTxBuf, "TX Addr:	%02X,%02X,%02X,%02X,%02X  \r\n\r\n", pipeAddrs[4], pipeAddrs[3], pipeAddrs[2],pipeAddrs[1],pipeAddrs[0]);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);

	//g) RX pipes addresses
	NRF24_ReadRegisterN(0x0A, pipeAddrs, 5);
	sprintf(uartTxBuf, "RX_Pipe0 Addr:	%02X,%02X,%02X,%02X,%02X  \r\n", pipeAddrs[4], pipeAddrs[3], pipeAddrs[2],pipeAddrs[1],pipeAddrs[0]);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	NRF24_ReadRegisterN(0x0A+1, pipeAddrs, 5);
	sprintf(uartTxBuf, "RX_Pipe1 Addr:	%02X,%02X,%02X,%02X,%02X  \r\n", pipeAddrs[4], pipeAddrs[3], pipeAddrs[2],pipeAddrs[1],pipeAddrs[0]);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	NRF24_ReadRegisterN(0x0A+2, pipeAddrs, 1);
	sprintf(uartTxBuf, "RX_Pipe2 Addr:	xx,xx,xx,xx,%02X  \r\n", pipeAddrs[0]);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	NRF24_ReadRegisterN(0x0A+3, pipeAddrs, 1);
	sprintf(uartTxBuf, "RX_Pipe3 Addr:	xx,xx,xx,xx,%02X  \r\n", pipeAddrs[0]);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	NRF24_ReadRegisterN(0x0A+4, pipeAddrs, 1);
	sprintf(uartTxBuf, "RX_Pipe4 Addr:	xx,xx,xx,xx,%02X  \r\n", pipeAddrs[0]);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	NRF24_ReadRegisterN(0x0A+5, pipeAddrs, 1);
	sprintf(uartTxBuf, "RX_Pipe5 Addr:	xx,xx,xx,xx,%02X  \r\n\r\n", pipeAddrs[0]);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	
	//h) RX PW (Payload Width 0 - 32)
	reg8Val = NRF24_ReadRegister(0x11);
	sprintf(uartTxBuf, "RX_PW_P0:	%d bytes \r\n", reg8Val&0x3F);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	reg8Val = NRF24_ReadRegister(0x11+1);
	sprintf(uartTxBuf, "RX_PW_P1:	%d bytes \r\n", reg8Val&0x3F);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	reg8Val = NRF24_ReadRegister(0x11+2);
	sprintf(uartTxBuf, "RX_PW_P2:	%d bytes \r\n", reg8Val&0x3F);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	reg8Val = NRF24_ReadRegister(0x11+3);
	sprintf(uartTxBuf, "RX_PW_P3:	%d bytes \r\n", reg8Val&0x3F);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	reg8Val = NRF24_ReadRegister(0x11+4);
	sprintf(uartTxBuf, "RX_PW_P4:	%d bytes \r\n", reg8Val&0x3F);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	reg8Val = NRF24_ReadRegister(0x11+5);
	sprintf(uartTxBuf, "RX_PW_P5:	%d bytes \r\n\r\n", reg8Val&0x3F);
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	//i) Dynamic payload enable for each pipe
	reg8Val = NRF24_ReadRegister(0x1c);
	sprintf(uartTxBuf, "DYNPD_pipe:	P0:	%d\r\n		P1:	%d\r\n		P2:	%d\r\n		P3:	%d\r\n		P4:	%d\r\n		P5:	%d\r\n\r\n",
	_BOOL(reg8Val&(1<<0)), _BOOL(reg8Val&(1<<1)), _BOOL(reg8Val&(1<<2)), _BOOL(reg8Val&(1<<3)), _BOOL(reg8Val&(1<<4)), _BOOL(reg8Val&(1<<5)));
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	//j) EN_DPL (is Dynamic payload feature enabled ?)
	reg8Val = NRF24_ReadRegister(0x1d);
	if(reg8Val&(1<<2)) sprintf(uartTxBuf, "EN_DPL:		Enabled \r\n");
	else sprintf(uartTxBuf, "EN_DPL:		Disabled \r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	//k) EN_ACK_PAY
	if(reg8Val&(1<<1)) sprintf(uartTxBuf, "EN_ACK_PAY:		Enabled \r\n");
	else sprintf(uartTxBuf, "EN_ACK_PAY:	Disabled \r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	
	sprintf(uartTxBuf, "\r\n**********************************************\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
}

/**************************************************//**
 * @brief This function prints the status register
 *************************************************/
void PrintStatusReg(void)
{
	uint8_t reg8Val;
	char uartTxBuf[100];
	sprintf(uartTxBuf, "\r\n-------------------------\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	reg8Val = NRF24_ReadRegister(0x07);
	sprintf(uartTxBuf, "STATUS reg:\r\n		RX_DR:		%d\r\n		TX_DS:		%d\r\n		MAX_RT:		%d\r\n		RX_P_NO:	%d\r\n		TX_FULL:	%d\r\n",
	_BOOL(reg8Val&(1<<6)), _BOOL(reg8Val&(1<<5)), _BOOL(reg8Val&(1<<4)), _BOOL(reg8Val&(3<<1)), _BOOL(reg8Val&(1<<0)));
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	sprintf(uartTxBuf, "\r\n-------------------------\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
}

/***************************************//**
 * @brief This function prints the config register
 ***************************************/
void PrintConfigReg(void)
{
	uint8_t reg8Val;
	char uartTxBuf[100];
	
	sprintf(uartTxBuf, "\r\n-------------------------\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	reg8Val = NRF24_ReadRegister(0x00);
	sprintf(uartTxBuf, "CONFIG reg:\r\n		PWR_UP:		%d\r\n		PRIM_RX:	%d\r\n",
	_BOOL(reg8Val&(1<<1)), _BOOL(reg8Val&(1<<0)));
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	sprintf(uartTxBuf, "\r\n-------------------------\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
}

/********************************************************//**
 * @brief This function sets the init variables for uart
 *
 * @param nrf23_uart	:
 *******************************************************/
void NRF24_DebugUartInit(UART_HandleTypeDef nrf24_uart)
{
	memcpy(&nrf24_huart, &nrf24_uart, sizeof(nrf24_uart));
}
/*********************************************//**
 * @brief This function prints the FIFO status
 *
 * @param nrf23_uart	:
 *********************************************/
void PrintFIFOstatus(void)
{
	uint8_t reg8Val;
	char uartTxBuf[100];
	sprintf(uartTxBuf, "\r\n-------------------------\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	reg8Val = NRF24_ReadRegister(0x17);
	sprintf(uartTxBuf, "FIFO Status reg:\r\n		TX_FULL:		%d\r\n		TX_EMPTY:		%d\r\n		RX_FULL:		%d\r\n		RX_EMPTY:		%d\r\n",
	_BOOL(reg8Val&(1<<5)), _BOOL(reg8Val&(1<<4)), _BOOL(reg8Val&(1<<1)), _BOOL(reg8Val&(1<<0)));
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
	sprintf(uartTxBuf, "\r\n-------------------------\r\n");
	HAL_UART_Transmit(&nrf24_huart, (uint8_t *)uartTxBuf, strlen(uartTxBuf), 10);
	
}

/**************************************************************//**
 * @brief This function sent wireless data and flushes rx after
 *
 * @param TxData	:	Pointer to Transmit data array
 **************************************************************/
void NRF_Transmit(uint8_t *TxData)
{
	HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
	uint8_t i = 0;
	bool tx_status = 0;
	for(; i < 10; i++)
	{
		tx_status = NRF24_Write(TxData, 32);
		if(tx_status){
			NRF24_FlushRx();
			//printf("Transmit succesfull \r\n");
			//HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
			return;
		}

		//HAL_Delay(50);
	}
	//printf("255\r\n"); 	// Sent to HUB that the data failed
}

/**************************************************************//**
 * @brief This function is the init for our code
 *
 * @param TxAddress	:	Transmit Address
 * @param RxAddress	:	Receive Address
 **************************************************************/
void NRF_Init(uint64_t TxAddress, uint64_t RxAddress)
{
	NRF24_Begin(NRF_CONTROL_GPIO_PORT, NRF_CS_Pin, NRF_CE_Pin, hspi1);	//Set nRF24L01 with default register values
	NRF24_DebugUartInit(huart2);		//Set UART peripheral for debugging
	NRF24_StopListening();					//Make sure nRF24L01 is not in RX-mode
    NRF24_OpenWritingPipe(TxAddress);		//Open writing pipe on 0x
    NRF24_StopListening();
    NRF24_SetAutoAck(true);					//Acknowledgement off
    NRF24_SetChannel(NRF_CHANNEL);			//Channel 120
	NRF24_SetPayloadSize(PAYLOAD_SIZE);		//32 byte payload
	NRF24_SetDataRate(RF24_2MBPS);
	NRF24_OpenReadingPipe(1, RxAddress);
	NRF24_SetRetries(0x00, 0x0A);			//10 Retries with a delay of 1000us between retransmits
	NRF24_StopListening();
	//NRF24_EnableDynamicPayloads();
	//NRF24_EnableAckPayload();
	nrf24_cmd_flag = 0;						//Set RX message flag to false
	//PrintRadioSettings();
}
