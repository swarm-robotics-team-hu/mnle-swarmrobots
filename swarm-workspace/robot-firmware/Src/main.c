/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  *
  * Peripheral		Function									Details
  * -------------------------------------------------------------------------------------------------------------------------------
  * TIM2			Universal timer for small tasks 			(1 us interval)
  * USART2			UART for debugging							(115200 Baud, 8-bit word, 1 stop bit, parity none, no flow control)
  * I2C1			I2C communication for VL53L1X & ICM20948	(400 kHz fast mode)
  * SPI1			SPI communication for ICM20948				(SPI mode 0, CPOL = 0, CPHA = 1, MSB first)
  *
  *
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "VL53L1X.h"
#include "rotary_encoder_lib.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
extern UART_HandleTypeDef huart2;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */
  

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_TIM2_Init();
  MX_TIM3_Init();
  MX_TIM4_Init();
  MX_TIM5_Init();
  MX_TIM9_Init();
  MX_TIM1_Init();
  /* USER CODE BEGIN 2 */
  robot_init();
  HAL_Delay(4000);
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

//  robot.new_pos[X] = 0;
//  robot.new_pos[Y] = 0.5;
//  printf("X: %f \t Y: %f \r\n", robot.new_pos[X], robot.new_pos[Y]);
//  CalculateTravel(robot.new_pos);
//
//  RobotTravel(robot.travel_dis, 50);
//  SensorModuleSweep(MIN_ANGLE, MAX_ANGLE, 1);
//
//  robot.new_pos[X] = 0.5;
//  robot.new_pos[Y] = 0.5;
//  printf("X: %f \t Y: %f \r\n", robot.new_pos[X], robot.new_pos[Y]);
//  CalculateTravel(robot.new_pos);
//
//  RobotTravel(robot.travel_dis, 50);
//  SensorModuleSweep(MIN_ANGLE, MAX_ANGLE, 1);
//
//  robot.new_pos[X] = 0.5;
//  robot.new_pos[Y] = 0;
//  printf("X: %f \t Y: %f \r\n", robot.new_pos[X], robot.new_pos[Y]);
//  CalculateTravel(robot.new_pos);
//
//  RobotTravel(robot.travel_dis, 50);
//  SensorModuleSweep(MIN_ANGLE, MAX_ANGLE, 1);
//
//  robot.new_pos[X] = 1;
//  robot.new_pos[Y] = 0;
//  printf("X: %f \t Y: %f \r\n", robot.new_pos[X], robot.new_pos[Y]);
//  CalculateTravel(robot.new_pos);
//
//  RobotTravel(robot.travel_dis, 50);
//  SensorModuleSweep(MIN_ANGLE, MAX_ANGLE, 1);
//
//  robot.new_pos[X] = 1;
//  robot.new_pos[Y] = 0.5;
//  printf("X: %f \t Y: %f \r\n", robot.new_pos[X], robot.new_pos[Y]);
//  CalculateTravel(robot.new_pos);
//
//  RobotTravel(robot.travel_dis, 50);
//  SensorModuleSweep(MIN_ANGLE, MAX_ANGLE, 1);
//
//  robot.new_pos[X] = 0;
//  robot.new_pos[Y] = 0;
//  printf("X: %f \t Y: %f \r\n", robot.new_pos[X], robot.new_pos[Y]);
//  CalculateTravel(robot.new_pos);
//
//  RobotTravel(robot.travel_dis, 50);
//  SensorModuleSweep(MIN_ANGLE, MAX_ANGLE, 1);

  while (1)
  {
	  //IMU_EstimateOri();

	  ITRP_CmdNumber();
	  SERVO_TASK();
	  /* USER CODE END WHILE */
    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage 
  */
    __HAL_RCC_PWR_CLK_ENABLE();
    __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE2);
  /** Initializes the CPU, AHB and APB busses clocks 
  */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLM = 25;
    RCC_OscInitStruct.PLL.PLLN = 168;
    RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
    RCC_OscInitStruct.PLL.PLLQ = 4;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
      Error_Handler();
    }
  /** Initializes the CPU, AHB and APB busses clocks 
  */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
    {
      Error_Handler();
    }
}

/* USER CODE BEGIN 4 */

void robot_init()
{
	printf("================================= \r\n");
	printf("Robot is started \r\n");
	printf("================================= \r\n\r\n");

	/* counter */
	int i,j;

	/* init timers */

    HAL_TIM_Base_Start_IT(&htim5);					/*	System runtime timer (every second)					*/

    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);		/*	Left servo											*/
    HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);		/*	Right servo											*/
    HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);		/*	Sensor servo										*/

    HAL_UART_MspInit(&huart2);						/*	Initializes UART peripheral and corresponding GPIO	*/
    HAL_I2C_MspInit(&hi2c1);						/*	Initializes I2C peripheral and corresponding GPIO	*/
    HAL_SPI_MspInit(&hspi1);						/*	ICM20948											*/

    HAL_TIM_Encoder_Stop(&htim1, TIM_CHANNEL_1);	/*	Encoder timer: servo left							*/
    HAL_TIM_Encoder_Stop(&htim2, TIM_CHANNEL_1);	/*	Encoder timer: servo right							*/

    uint64_t robot_address = 2;

    NRF_Init(robot_address + 1000000000000, robot_address); //Tx, Rx
    NRF24_StartListening();
    PrintRadioSettings();

	/* init functions */
	ServoInit();							/*	Initializes the servo's and encoders			*/
	PID_init();
	VL53L1X_Init(2);						/* 	Initializes VL53L1X devices and set settings 	*/
	ICM_Initialize(ACC_G8, GYR_DPS250);		/*	Initializes ICM IMU	en set settings				*/

	SensorFusionInit();						/* 	Initializes sensor fusion						*/
	//HAL_TIM_Base_Start_IT(&htim9);			/*	Quaternion update (every microsecond)			*/

	/* single variable init */
	robot.sensor_read_flag = 1;
	robot.delta_t 	= 0;

	/* double variable init */
	for(i=0; i<2; i++)
	{
		robot.pos[i]		= 0;
		robot.pos_RE[i] 	= 0;
		robot.pos_IMU[i] 	= 0;

		robot.vel[i]   		= 0;
		robot.vel_RE[i]   	= 0;
		robot.vel_IMU[i]   	= 0;
	}

	/* tripple variable init */
	for(i=0; i<3; i++)
	{
		robot.raw_accel[i] 	= 0;
		robot.accel_data[i] = 0;
		robot.accel[i] 		= 0;
		robot.raw_gyro[i]  	= 0;
		robot.gyro_data[i] 	= 0;
		robot.gyro[i]  		= 0;

		robot.ori[i] 		= 0;
		robot.IMU_ori[i]	= 0;
		robot.yaw_RE[i]		= 0;
	}

	/* 2x3 array init */
	for(i=0; i<2; i++)
	{
		for(j=0;j<3;j++)
		{
			robot.pos_buff[i][j] = 0;
		}
	}
	ServoSetAngle(SENSOR_SERVO, 50);
	HAL_Delay(500);
	ServoSetAngle(SENSOR_SERVO, 0);

    printf("================================= \r\n");
    printf("Robot is initialized \r\n");
    printf("================================= \r\n\r\n");

}

void SimulateTravel(float x, float y)
{
	robot.new_pos[X] = x;
	robot.new_pos[Y] = y;
	CalculateTravel(robot.new_pos);	/* Calculate travel distance and orientation */
	robot.pos[X] = x;
	robot.pos[Y] = y;
	robot.ori[Z] = robot.travel_ori;
	printf("Travel ori: %.2f \t Travel dis: %.2f \t", robot.travel_ori, robot.travel_dis);
	printf("PosX: %.2f \t PosY: %.2f \t Ori: %.2f \r\n\r\n", robot.pos[X], robot.pos[Y], robot.ori[Z]);
}

void RobotTest(void)
{
	int i;

	/* Test driving servos */
	for(i=-100; i<100; i++)
	{
	  ServoSetSpeed(LEFT_SERVO, i);
	  ServoSetSpeed(RIGHT_SERVO, i);
	  HAL_Delay(50);
	}

	ServoSetSpeed(LEFT_SERVO, 0);
	ServoSetSpeed(RIGHT_SERVO, 0);

	/* Test Sensor servo */
	for(i=0; i<180; i++)
	{
		ServoSetAngle(SENSOR_SERVO, i);
		HAL_Delay(50);
	}

	for(i=179; i>-1; i--)
	{
		ServoSetAngle(SENSOR_SERVO, i);
		HAL_Delay(50);
	}

	robot.sensor_read_flag = 1;
	SensorModuleSweep(MIN_ANGLE, MAX_ANGLE, 1);

}

void IMU_ManualCalibrate()
{
	int i,j = 0;
	const int mean_count = 50;
	int16_t accel_data_total[3] = {0};
	int16_t gyro_data_total[3]  = {0};

	for(i=0; i<mean_count; i++)
	{
		ICM_ReadAccelGyro();
		robot.accel_data[Z] -= 4096;
		//printf("ax: %i \t ay: %i \t az: %i \r\n", robot.accel_data[X], robot.accel_data[Y], robot.accel_data[Z]);
		//printf("gx: %i \t gy: %i \t gz: %i \r\n", robot.gyro_data[X], robot.gyro_data[Y], robot.gyro_data[Z]);

		for(j=0; j<3; j++)
		{
			accel_data_total[j] += robot.accel_data[j];
			gyro_data_total[j]  += robot.gyro_data[j];
		}

		//HAL_Delay(100);
	}

	for(j=0; j<3; j++)
	{
		accel_data_total[j] /= mean_count;
		gyro_data_total[j]  /= mean_count;
	}

	//printf("\r\n MEAN ax: %i \t ay: %i \t az: %i \r\n\r\n", accel_data_total[X], accel_data_total[Y], accel_data_total[Z]);
	printf("MEAN gx: %i \t gy: %i \t gz: %i\r\n", gyro_data_total[X], gyro_data_total[Y], gyro_data_total[Z]);
}

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */

  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
