/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup main
  * @brief	All code related to the main
  * @{
  */

/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_hal.h"
#include "stm32f4xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stm32f4xx_hal_i2c.h>
#include "stm32f4xx_it.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <math.h>
#include <stdbool.h>

#include "servo.h"
#include "VL53L1X.h"

#include "i2c.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"
#include <stm32f4xx_hal_i2c.h>
#include <stm32f4xx_hal_spi.h>

#include "ICM20948.h"
#include "sensor_fusion.h"
#include "pos_tracking.h"
#include "robot_motion.h"
#include "nrf24l01.h"
#include "nrf24_lib.h"
#include "interpreter.h"



/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* Prototypes ----------------------------------------------------------------*/
void SimulateTravel(float x, float y);
void robot_init();
void RobotTest(void);
void IMU_ManualCalibrate();

/* Buffers -------------------------------------------------------------------*/
uint8_t cmd_buf[32];		/**< Receive buffer for commands	*/
uint8_t uart_rx_buf[32];
uint32_t uart_arg_array[32];

/* Task flags ----------------------------------------------------------------*/
uint8_t servo_flag;
uint8_t nrf24_cmd_flag;
uint8_t slip_flag;

/* Struct's ------------------------------------------------------------------*/
typedef struct
{
	uint8_t 	Address;			/**< I2C address							*/

	uint16_t 	SignalRate;			/**< Signal rate							*/
	uint16_t 	AmbientRate;		/**< Ambient rate							*/
	uint16_t 	SpadNum;			/**< SpadNum								*/
	uint8_t 	dataReady;			/**< flag to enable/disable sensor reading	*/

	uint16_t 	offset;				/**< Offset to the values					*/
	uint16_t 	xtalk;				/**< Offset for Xtalk to the values			*/

	uint8_t		stepsize;			/**< The stepsize of the sensor servo		*/

	uint8_t 	RangeStatus[180];	/**< Array for Range status [angle]			*/
	uint16_t	Distance[180];		/**< Array for distance		[angle]			*/
	uint8_t		ServoAngle[180];	/**< Array for servo angle	[angle]			*/
	uint16_t    Status;				/**< Status of the VL53L1X					*/

}DualSensorModuleTypeDef;

typedef struct
{
	uint8_t servo_ID;			/**< Servo ID									*/

	int REvalue[3];				/**< Buffer of the rotary encoder values		*/
	int16_t RE_dir_buff[2];		/**< Buffer of direction values					*/
	int16_t RE_dval;			/**< Differential value of the rotary encoder	*/

	runtime REtime_new[2];		/**< New time stamp								*/
	uint64_t REtime_old[2];		/**< Old time stamp								*/
	uint64_t RE_dt;				/**< Differential value of the time	(us)		*/

	uint8_t dir_bit[2];			/**< Used direction value						*/

	/* needed ? */
	double RE_rp_us;			/**< Rotations per microsecond					*/
	float RE_rp_s;				/**< Rotations per second						*/
	float RE_rp_m;				/**< Rotations per minute						*/
	float RE_mp_s;				/**< Meters per seconds							*/

	float distance;				/**< traveled distance							*/
	float distance_old;			/**< last teveled distance						*/

	/* flags */
	uint8_t dir_change;			/**< Flag to state the direction has changed	*/
	uint8_t status;				/**< Flag to state in which state the direction is going	*/

	uint16_t pulse_width_us;	/**< The pulse width for PWM signal	(us)		*/

} wheelservo;

typedef struct
{
	float error;				/**< Error of the reference value and measured	*/
	float error_old;			/**< Old error									*/
	float error_DoT;			/**< Difference of error over time				*/
	float E;					/**< Som of all errors							*/
	float KP;					/**< Proportial gain							*/
	float KI;					/**< Integration gain							*/
	float KD;					/**< Differential gain							*/

	float OUT;					/**< Output PID									*/
} PID;

typedef struct
{
	DualSensorModuleTypeDef sensor_module[2];	/**< Sensor modules struct 		*/

	wheelservo servoR;							/**< servo structs right servo	*/
	wheelservo servoL;							/**< servo structs left servo 	*/

	PID drive_control;							/**< PID struct					*/

	/* Used for calculating delta t */
	runtime tim_new;							/**< New runtime 				*/
	runtime tim_old;							/**< Old runtime 				*/
	float   delta_t;							/**< delta time	 				*/

	/* Used for calculating delta t RE */
	runtime tim_new_RE;							/**< New runtime rotary encoder	*/
	runtime tim_old_RE;							/**< Old runtime rotary encoder	*/
	float   delta_t_RE;							/**< delta time rotary encoder	*/

	/* Desired position and orientation */
	float new_pos[2];							/**< Desired position to travel to	(m) [x][y]				*/
	float travel_dis;							/**< Desired distance to travel		(m)						*/
	float travel_ori;							/**< Desired orientation to face when traveling (degrees)	*/

	/* Raw IMU data in g and dps */
	int16_t accel_data[3];						/**< Raw Acceleration data		 (bytes) [x][y][z]	*/
	int16_t gyro_data[3];						/**< Raw Angular momentum data	 (bytes) [x][y][z]	*/
	float   raw_accel[3];						/**< Raw Acceleration values	 (g)	 [x][y][z]	*/
	float   raw_gyro[3];						/**< Raw Angular momentum values (dps)	 [x][y][z]	*/
	float   raw_mag[3];							/**< Raw Magnetic field values   (mG)	 [x][y][z]	*/

	/* Robot sensor values */
	float accel[3];								/**< Acceleration of the IMU	 (m/s2) [x][y][z]	*/
	float gyro[3];								/**< Angular momentum of the IMU (dps)	[x][y][z]	*/

	/* Robot orientation and position data */
	float pos[2];								/**< Robot position				(m)		[x][y]		*/
	float pos_IMU[2];							/**< IMU position				(m)		[x][y]		*/
	float pos_RE[2];							/**< Rotary Encoder position	(m)		[x][y]		*/
	float pos_buff[2][3];						/**< Buffer position			(m)		[new][old][oldest]	*/
	float traveled_distance[3];					/**< Buffer distance			(m)		[new][old][oldest]	*/
	/* velocity */
	float vel[2];								/**< Robot velocity				(m/s) 	[x][y]	*/
	float vel_IMU[2];							/**< IMU velocity				(m/s) 	[x][y]	*/
	float vel_RE[2];							/**< Rotary encoder velocity	(m/s) 	[x][y]	*/

	/* orientation quaternion */
	float q[4];									/**< Quaternion orientation				[Re][i][j][k]	*/

	/* Orientation variables */
	float ori[3];								/**< Current orientation			(degrees) [Roll][Pitch][Yaw]	*/
	float IMU_ori[3];
	float yaw_RE[3];							/**< Rotary Encoder yaw orientation	(degrees) [new][old][oldest]	*/
	float phi_RE;								/**< Rotary Encoder phi orientation (rad)		*/

	uint8_t error;								/**< Last set error								*/
	uint8_t imu_status;							/**< IMU status									*/
	uint8_t sensor_read_flag;					/**< Sensor read flag to enable/disable distance sensor	*/



} robot_data;
robot_data robot;

/* Enum's --------------------------------------------------------------------*/
enum POS
{
	X = 0,	/* X axis	*/
	Y,		/* Y axis	*/
	Z		/* Z axis	*/
};

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */
/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */

/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define LED_Pin GPIO_PIN_13
#define LED_GPIO_Port GPIOC
#define ICM_CS_Pin GPIO_PIN_4
#define ICM_CS_GPIO_Port GPIOA
#define NRF_IRQ_Pin GPIO_PIN_0
#define NRF_IRQ_GPIO_Port GPIOB
#define NRF_IRQ_EXTI_IRQn EXTI0_IRQn
#define NRF_CS_Pin GPIO_PIN_1
#define NRF_CS_GPIO_Port GPIOB
#define NRF_CE_Pin GPIO_PIN_2
#define NRF_CE_GPIO_Port GPIOB
#define XSHUTDOWN_2_Pin GPIO_PIN_13
#define XSHUTDOWN_2_GPIO_Port GPIOB
#define XSHUTDOWN_1_Pin GPIO_PIN_14
#define XSHUTDOWN_1_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* standard defines */
#define	HIGH			1
#define	LOW				0
#define TRUE			1
#define FALSE			0
#define PI				(3.14159265359f)
/* Position */
#define NEW				0
#define OLD				1
#define OLDEST			2
#define DISTANCE		2
/* I2C */
#define	I2C_1			0
#define I2C_2			1
/* NRF */
#define NRF_CONTROL_GPIO_PORT GPIOB
/* IMU - ICM */
#define ICM_CS_Pin GPIO_PIN_4
#define ICM_CS_GPIO_Port GPIOA
#define ICM_SCK_Pin GPIO_PIN_5
#define ICM_SCK_GPIO_Port GPIOA
#define ICM_MISO_Pin GPIO_PIN_6
#define ICM_MISO_GPIO_Port GPIOA
#define ICM_MOSI_Pin GPIO_PIN_7
#define ICM_MOSI_GPIO_Port GPIOA
/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
/**
  * @}
  */
/**
  * @}
  */
