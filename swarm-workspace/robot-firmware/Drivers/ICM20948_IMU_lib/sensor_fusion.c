/**
  ******************************************************************************
  * @file           : sensor_fusion.c
  * @brief          : Library for fusion of IMU accelerometer and gyroscope data to produce orientation estimation
  * Description		: This is a universal c library that implements the Madgwick and Mahony algorithms for IMU sensor fusion.
  * References		: This library was written by Sebastian Madgwick and modified by the Swarm Robotics Team:
  * 					- https://www.x-io.co.uk/res/doc/madgwick_internal_report.pdf
  * 					- https://x-io.co.uk/open-source-imu-and-ahrs-algorithms/
  * 					- http://en.wikipedia.org/wiki/Fast_inverse_square_root
  * @author			: Swarm Robotics Team
  * @author			: Sebastian Madgwick
  * @date			: 02-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup sensor_fusion
  * @brief	All code related to the sensor fusion
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "sensor_fusion.h"

/*******************************************************************************************************//**
 * @brief This function initializes the quaternions to create a stable base quaternion
 *******************************************************************************************************/
void SensorFusionInit(void)
{
	int i;

	robot.q[0] = 1.0f;
	robot.q[1] = 0.0f;
	robot.q[2] = 0.0f;
	robot.q[3] = 0.0f;

	integralFB_x = 0.0f;
	integralFB_y = 0.0f;
	integralFB_z = 0.0f;
	beta        = 0.1f;

	//Stabilize quaternion
	for(i=0; i<200; i++)
		IMU_UpdateQuaternion();
}

/*******************************************************************************************************//**
 * @brief This function updates a quaternion using the Madgwick algorithm
 *
 * A 6-DoF IMU is required to make use of this function. Raw accelerometer & gyroscope values are fused
 * to estimate a quaternion representing the absolute orientation of the robot. Yaw, pitch and roll can be found
 * by using Euler angle calculations. The performance of the orientation filter is at least as good
 * as conventional Kalman-based filtering algorithms, but is much less computationally intensive.
 * More information about the algorithm can be found here: https://www.x-io.co.uk/res/doc/madgwick_internal_report.pdf
 *
 * @param ax	:	Acceleration on x-axis (in g's)
 * @param ay	:	Acceleration on y-axis (in g's)
 * @param az	:	Acceleration on z-axis (in g's)
 * @param gx	:	Angular momentum on x-axis (in rad/s)
 * @param gy	:	Angular momentum on y-axis (in rad/s)
 * @param gz	:	Angular momentum on z-axis (in rad/s)
 *******************************************************************************************************/
void UpdateQuaternionMadgwick(float ax, float ay, float az, float gx, float gy, float gz)
{
	float q0 = robot.q[0], q1 = robot.q[1], q2 = robot.q[2], q3 = robot.q[3];   /* short name local variable for readability */
	float recip_norm;
	float s0, s1, s2, s3;
	float qdot1, qdot2, qdot3, qdot4;
	float _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2 ,_8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

	/* Rate of change of quaternion from gyroscope */
	qdot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
	qdot2 = 0.5f * ( q0 * gx + q2 * gz - q3 * gy);
	qdot3 = 0.5f * ( q0 * gy - q1 * gz + q3 * gx);
	qdot4 = 0.5f * ( q0 * gz + q1 * gy - q2 * gx);

	/* Normalise accelerometer measurement */
	recip_norm = InvSqrt(ax * ax + ay * ay + az * az);
	ax *= recip_norm;
	ay *= recip_norm;
	az *= recip_norm;

	/* Auxiliary variables to avoid repeated arithmetic */
	_2q0 = 2.0f * q0;
	_2q1 = 2.0f * q1;
	_2q2 = 2.0f * q2;
	_2q3 = 2.0f * q3;

	_4q0 = 4.0f * q0;
	_4q1 = 4.0f * q1;
	_4q2 = 4.0f * q2;

	_8q1 = 8.0f * q1;
	_8q2 = 8.0f * q2;

	q0q0 = q0 * q0;
	q1q1 = q1 * q1;
	q2q2 = q2 * q2;
	q3q3 = q3 * q3;

	/* Gradient decent algorithm corrective step */
	s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
	s1 = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
	s2 = 4.0f * q0q0 * q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
	s3 = 4.0f * q1q1 * q3 - _2q1 * ax + 4.0f * q2q2 * q3 - _2q2 * ay;
	recip_norm = InvSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
	s0 *= recip_norm;
	s1 *= recip_norm;
	s2 *= recip_norm;
	s3 *= recip_norm;

	/* Apply feedback step */
	qdot1 -= beta * s0;
	qdot2 -= beta * s1;
	qdot3 -= beta * s2;
	qdot4 -= beta * s3;


	/* Integrate rate of change of quaternion to yield quaternion */
	q0 += qdot1 * robot.delta_t;
	q1 += qdot2 * robot.delta_t;
	q2 += qdot3 * robot.delta_t;
	q3 += qdot4 * robot.delta_t;

	/* Normalise quaternion */
	recip_norm = InvSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 *= recip_norm;
	q1 *= recip_norm;
	q2 *= recip_norm;
	q3 *= recip_norm;

	robot.q[0] = q0;
	robot.q[1] = q1;
	robot.q[2] = q2;
	robot.q[3] = q3;
}

/*******************************************************************************************************//**
 * @brief This function updates a quaternion using the Mahony algorithm
 *
 * A 6-DoF IMU is required to make use of this function. Raw accelerometer & gyroscope values are fused
 * to estimate a quaternion representing the absolute orientation of the robot.
 * Similar to Madgwick algorithm but uses proportional and integral filtering on the error
 * between estimated reference vectors and measured ones.
 *
 * @param ax	:	Acceleration on x-axis (in g's)
 * @param ay	:	Acceleration on y-axis (in g's)
 * @param az	:	Acceleration on z-axis (in g's)
 * @param gx	:	Angular momentum on x-axis (in rad/s)
 * @param gy	:	Angular momentum on y-axis (in rad/s)
 * @param gz	:	Angular momentum on z-axis (in rad/s)
 *******************************************************************************************************/
void UpdateQuaternionMahony(float ax, float ay, float az, float gx, float gy, float gz)
{
	float q0 = robot.q[0], q1 = robot.q[1], q2 = robot.q[2], q3 = robot.q[3];   /* short name local variable for readability */
	float recip_norm;
	float half_vx, half_vy, half_vz;
	float half_ex, half_ey, half_ez;
	float qa, qb, qc;

	/* Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation) */
	if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f)))
	{
		/* Normalise accelerometer measurement */
		recip_norm = InvSqrt(ax * ax + ay * ay + az * az);
		ax *= recip_norm;
		ay *= recip_norm;
		az *= recip_norm;

		/* Estimated direction of gravity and vector perpendicular to magnetic flux */
		half_vx = q1 * q3 - q0 * q2;
		half_vy = q0 * q1 + q2 * q3;
		half_vz = q0 * q0 - 0.5f + q3 * q3;

		/* Error is sum of cross product between estimated and measured direction of gravity */
		half_ex = (ay * half_vz - az * half_vy);
		half_ey = (az * half_vx - ax * half_vz);
		half_ez = (ax * half_vy - ay * half_vx);

		/* Compute and apply integral feedback if enabled */
		if(Ki > 0.0f)
		{
			integralFB_x += Ki * half_ex * robot.delta_t;	/* integral error scaled by Ki */
			integralFB_y += Ki * half_ey * robot.delta_t;
			integralFB_z += Ki * half_ez * robot.delta_t;
			gx += integralFB_x;								/* apply integral feedback */
			gy += integralFB_y;
			gz += integralFB_z;
		}
		else
		{
			integralFB_x = 0.0f;	/* prevent integral windup */
			integralFB_y = 0.0f;
			integralFB_z = 0.0f;
		}

		/* Apply proportional feedback */
		gx += Kp * half_ex;
		gy += Kp * half_ey;
		gz += Kp * half_ez;
	}

	/* Integrate rate of change of quaternion */
	gx *= (0.5f * robot.delta_t);		/* pre-multiply common factors */
	gy *= (0.5f * robot.delta_t);
	gz *= (0.5f * robot.delta_t);
	qa = q0;
	qb = q1;
	qc = q2;
	q0 += (-qb * gx - qc * gy - q3 * gz);
	q1 += (qa * gx + qc * gz - q3 * gy);
	q2 += (qa * gy - qb * gz + q3 * gx);
	q3 += (qa * gz + qb * gy - qc * gx);

	/* Normalise quaternion */
	recip_norm = InvSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 *= recip_norm;
	q1 *= recip_norm;
	q2 *= recip_norm;
	q3 *= recip_norm;

	robot.q[0] = q0;
	robot.q[1] = q1;
	robot.q[2] = q2;
	robot.q[3] = q3;
}

/*******************************************************************************************************//**
 * @brief This function calculates the fast inverse square-root
 *
 * More information on the functions workings can be found at: http://en.wikipedia.org/wiki/Fast_inverse_square_root
 *
 * @param x	:	Value from which the inverse square-root is calculated
 * @retval		Inverse square-root of x
 *******************************************************************************************************/
float InvSqrt(float x)
{
	float halfx = 0.5f * x;
	float y = x;
	long i = *(long*)&y;

	i = 0x5f3759df - (i>>1);
	y = *(float*)&i;
	y = y * (1.5f - (halfx * y * y));

	return y;
}

/**
  * @}
  */
/**
  * @}
  */
