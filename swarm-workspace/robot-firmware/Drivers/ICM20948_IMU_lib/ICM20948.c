/**
  ******************************************************************************
  * @file           : ICM20948.c
  * @brief          : ICM20948 drivers
  * References		: This library was written by CoryCline and modified by the Swarm Robotics Team:
  * 					- https://github.com/CoryCline/ICM20948
  * @author			: Swarm Robotics Team
  * @author			: CoryCline
  * @date			: 02-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */


/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup ICM20948
  * @brief	All code related to the sensor fusion
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "ICM20948.h"


/***************************************************************************************************//**
 * @brief Function for reading multiple registers with spi the to be read registers must be consecutive
 * by one byte
 *
 * @param reg 						: The starting register to be read
 * @param pdata						: Pointer to where received SPI data is stored
 * @param Size						: The amount of registers to be read
 **************************************************************************************************/
void ICM_ReadBytes(uint8_t reg, uint8_t *pData, uint16_t Size)
{
	reg = reg | 0x80;
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(SPI_BUS, &reg, 1, 0xFFFF);
	HAL_SPI_Receive(SPI_BUS, pData, Size, 0xFFFF);
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_SET);
}


/***************************************************************************************************//**
 * @brief Used to write multiple registers consecutively with spi
 *
 * @param reg 						: The starting register to be written to
 * @param pdata						: Pointer to which data is being written
 * @param Size						: The amount of registers to be written to
 **************************************************************************************************/
void ICM_WriteBytes(uint8_t reg, uint8_t *pData, uint16_t Size)
{
	reg = reg & 0x7F;
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(SPI_BUS, &reg, 1, 0xFFFF);
	HAL_SPI_Transmit(SPI_BUS, pData, Size, 0xFFFF);
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_SET);
}


/***************************************************************************************************//**
 * @brief Used to read one register with spi
 *
 * @param reg 						: The register to be read
 * @param pdata						: Pointer to where received SPI data is stored
 **************************************************************************************************/
void ICM_ReadOneByte(uint8_t reg, uint8_t* pData) // ***
{
	reg = reg | 0x80;
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(SPI_BUS, &reg, 1, 0xFFFF);
	while (HAL_SPI_GetState(SPI_BUS) != HAL_SPI_STATE_READY);
	HAL_SPI_Receive(SPI_BUS, pData, 1, 0xFFFF);
	while (HAL_SPI_GetState(SPI_BUS) != HAL_SPI_STATE_READY);
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_SET);
}


/***************************************************************************************************//**
 * @brief Used to write one register with spi
 *
 * @param reg 						: The register to be written to
 * @param pdata						: Pointer to which data is being written
 **************************************************************************************************/
void ICM_WriteOneByte(uint8_t reg, uint8_t Data) // ***
{
	reg = reg & 0x7F;
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_RESET);
	HAL_SPI_Transmit(SPI_BUS, &reg, 1, 0xFFFF);
	HAL_SPI_Transmit(SPI_BUS, &Data, 1, 0xFFFF);
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, GPIO_PIN_SET);
}


/***************************************************************************************************//**
 * @brief Used to write one register of the magnetometer with spi through i2c
 *
 * @param reg 						: The register to be written to
 * @param value						: The Byte being written to the register
 **************************************************************************************************/
void ICM_MagWriteI2C(uint8_t reg,uint8_t value)
{
	ICM_WriteOneByte(0x7F, 0x30);
	DelayMicroSeconds(MAG_DELAY_US);
	ICM_WriteOneByte(0x03, 0x0C);	//mode: write
	DelayMicroSeconds(MAG_DELAY_US);
	ICM_WriteOneByte(0x04, reg);	//set reg addr
	DelayMicroSeconds(MAG_DELAY_US);
	ICM_WriteOneByte(0x06, value);	//send value
	DelayMicroSeconds(MAG_DELAY_US);
}


/***************************************************************************************************//**
 * @brief Used to read one register of the magnetometer with spi through i2c
 *
 * @param reg 						: The register to be read
 * @retval Data						: The data of the register
 **************************************************************************************************/
uint8_t ICM_MagReadReg(uint8_t reg)
{
	uint8_t  Data;
	ICM_WriteOneByte(0x7F, 0x30);		// register bank 3
	DelayMicroSeconds(MAG_DELAY_US);
	ICM_WriteOneByte(0x03, 0x0C|0x80); 	//address van magnetometer I2C 0x80 is read
	DelayMicroSeconds(MAG_DELAY_US);
	ICM_WriteOneByte(0x04, reg);		// set reg addr
	DelayMicroSeconds(MAG_DELAY_US);
	ICM_WriteOneByte(0x06, 0xff);		//read
	DelayMicroSeconds(MAG_DELAY_US);
	ICM_WriteOneByte(0x7F, 0x00);
	ICM_ReadOneByte(0x3B, &Data);
	DelayMicroSeconds(MAG_DELAY_US);
	return Data;
}


/***************************************************************************************************//**
 * @brief Function to read the 3 axis of the magnetometer
 *
 * @param magn 						: Pointer to array to store magnetometer data
 **************************************************************************************************/
void ICM_ReadMag(float *magn)
{
	uint8_t mag_buffer[10];

	SPI_peripheral_switch(ICM20948);

	mag_buffer[0] = ICM_MagReadReg(0x01);	//device ID
	mag_buffer[1] = ICM_MagReadReg(0x11);
	mag_buffer[2] = ICM_MagReadReg(0x12);
	magn[0] = (float)((mag_buffer[1] | mag_buffer[2] << 8) * MAG_CONVERT_VAL);

	mag_buffer[3] = ICM_MagReadReg(0x13);
	mag_buffer[4] = ICM_MagReadReg(0x14);
	magn[1] = (float)((mag_buffer[3] | mag_buffer[4] << 8) * MAG_CONVERT_VAL);

	mag_buffer[5] = ICM_MagReadReg(0x15);
	mag_buffer[6] = ICM_MagReadReg(0x16);
	magn[2] = (float)((mag_buffer[5] | mag_buffer[6] << 8) * MAG_CONVERT_VAL);

	ICM_MagWriteI2C(0x31,0x01); // Single measurement mode

	SPI_peripheral_switch(NRF24);
}


/***************************************************************************************************//**
 * @brief Function to reset the ICM20948 after powering the icm20948
 **************************************************************************************************/
void ICM_PowerOn(void)
{
	ICM_CSHigh();
	HAL_Delay(10);
	ICM_SelectBank(USER_BANK_0);
	HAL_Delay(10);
	ICM_DisableI2C();
	HAL_Delay(10);
	ICM_SetClock((uint8_t)CLK_BEST_AVAIL);
	HAL_Delay(10);
	ICM_AccelGyroOff();
	HAL_Delay(20);
	ICM_AccelGyroOn();
	HAL_Delay(10);
}


/***************************************************************************************************//**
 * @brief Function to initialize various settings such as:
 * - accelerometer, gyro, magnetometer sensitivities
 * - sample rates
 * - internal filters
 * - continuous measurements
 * - the offset
 *
 *
 * @param acc_sens 						: Value determines which accelerometer sensitivity is selected
 * @param gyr_sens						: Value determines which gyroscope sensitivity is selected
 *
 **************************************************************************************************/
uint16_t ICM_Initialize(ACC_SENS acc_sens, GYR_SENS gyr_sens)
{
		SPI_peripheral_switch(ICM20948);
	    HAL_Delay(10);

		ICM_Sensitivity(acc_sens, gyr_sens);
		ICM_PowerOn();
	    ICM_SelectBank(USER_BANK_0);
		HAL_Delay(10);

		ICM_PowerOn();
		HAL_Delay(20);

		ICM_SelectBank(USER_BANK_2);
		HAL_Delay(20);

		ICM_WriteOneByte(GYRO_CONFIG_1, ((0x06 << 3) | (gyr_sens << 1) | 0x01));
		HAL_Delay(10);

		// Set gyroscope sample rate to 100hz (0x0A) in GYRO_SMPLRT_DIV register (0x00)
		ICM_WriteOneByte(0x00, 0x00);
		HAL_Delay(10);

		// Set accelerometer low pass filter to 136hz (0x11) and the rate to 8G (0x04) in register ACCEL_CONFIG (0x14)
		ICM_WriteOneByte(0x14, ((0x04 << 3) | (acc_sens << 1) | 0x01));

		// Set accelerometer sample rate to 225hz (0x00) in ACCEL_SMPLRT_DIV_1 register (0x10)
		ICM_WriteOneByte(0x10, 0x00);
		HAL_Delay(10);

		// Set accelerometer sample rate to 100 hz (0x0A) in ACCEL_SMPLRT_DIV_2 register (0x11)
		ICM_WriteOneByte(0x11, 0x00); //1.125khz
		HAL_Delay(10);

		ICM_SelectBank(USER_BANK_2);
		HAL_Delay(20);

		// Configure AUX_I2C Magnetometer (onboard ICM-20948)
		ICM_WriteOneByte(0x7F, 0x00); // Select user bank 0
		ICM_WriteOneByte(0x0F, 0x30); // INT Pin / Bypass Enable Configuration
		ICM_WriteOneByte(0x03, 0x20); // I2C_MST_EN //0x20
		ICM_WriteOneByte(0x7F, 0x30); // Select user bank 3
		ICM_WriteOneByte(0x01, 0x4D); // I2C Master mode and Speed 400 kHz
		ICM_WriteOneByte(0x02, 0x01); // I2C_SLV0 _DLY_ enable
		ICM_WriteOneByte(0x05, 0x81); // enable IIC	and EXT_SENS_DATA==1 Byte

		// Initialize magnetometer
		ICM_MagWriteI2C(0x32, 0x01); // Reset AK8963
		HAL_Delay(1000);
		ICM_MagWriteI2C(0x31, 0x02); // use i2c to set AK8963 working on Continuous measurement mode1 & 16-bit output

		//Robot 1 values
//		int32_t acc_mean[3], gyr_mean[3];
//		acc_mean[0] = 5747;
//		acc_mean[1] = 1031;
//		acc_mean[2] = -979;
//
//		gyr_mean[0] = 38;
//		gyr_mean[1] = -26;
//		gyr_mean[2] = -73;
//		ICM_SetOffset(&acc_mean[0], &gyr_mean[0]);//set new offset

		ICM_ConfigureOffset(acc_sens, gyr_sens);
		SPI_peripheral_switch(NRF24);
		return 1;
}


/***************************************************************************************************//**
 * @brief Function for reading the 3 axis of the accelerometer and gyroscope
 **************************************************************************************************/
void ICM_ReadAccelGyro(void)
{
	uint8_t raw_data[12];
	uint8_t i;

	SPI_peripheral_switch(ICM20948);

	/* Read data from IMU */
	ICM_SelectBank(USER_BANK_0);
	ICM_ReadBytes(0x2D, &raw_data[0], 12);

	SPI_peripheral_switch(NRF24);

	/* Store accelerometer and gyroscope data (in float values) */
	for(i=0; i<3; i++)
	{
		robot.accel_data[i] = ((raw_data[i*2]   << 8) | (raw_data[i*2+1] & 0xFF));
		robot.gyro_data[i]  = ((raw_data[i*2+6] << 8) | (raw_data[i*2+7] & 0xFF));

		robot.raw_accel[i] = (float)robot.accel_data[i] / acc_sens_val;
		robot.raw_gyro[i]  = (float)robot.gyro_data[i]  / gyr_sens_val;
	}
}


/***************************************************************************************************//**
 * @brief Selects the register bank
 * 	the ICM20948 has 4 register banks
 * @param bank 						:The selected bank (0, 1, 2 or 3)
 **************************************************************************************************/
void ICM_SelectBank(uint8_t bank)
{
	ICM_WriteOneByte(USER_BANK_SEL, bank);
}


/***************************************************************************************************//**
 * @brief Disables I2C
 **************************************************************************************************/
void ICM_DisableI2C(void)
{
	ICM_WriteOneByte(0x03, 0x78);
}


/***************************************************************************************************//**
 * @brief Sets the Chip Select pin high for spi
 **************************************************************************************************/
void ICM_CSHigh(void)
{
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, SET);
}


/***************************************************************************************************//**
 * @brief Sets the Chip Select pin low for spi
 **************************************************************************************************/
void ICM_CSLow(void)
{
	HAL_GPIO_WritePin(ICM_CS_GPIO_Port, ICM_CS_Pin, RESET);
}


/***************************************************************************************************//**
 * @brief Sets the clock of the ICM20948
 *
 * @param clk 				: determines the clock source 3 bits [2:0]
 **************************************************************************************************/
void ICM_SetClock(uint8_t clk)
{
	ICM_WriteOneByte(PWR_MGMT_1, clk);
}


/***************************************************************************************************//**
 * @brief Turns off the accelerometer and gyroscope of the ICM20948
 **************************************************************************************************/
void ICM_AccelGyroOff(void)
{
	ICM_WriteOneByte(PWR_MGMT_2, (0x38 | 0x07));
}


/***************************************************************************************************//**
 * @brief Turns on the accelerometer and gyroscope of the ICM20948
 **************************************************************************************************/
void ICM_AccelGyroOn(void)
{
	ICM_WriteOneByte(0x07, (0x00 | 0x00));
}


/***************************************************************************************************//**
 * @brief Returns the whoami register of the ICM20948
 * @retval					: returns the value in the WhoAmI register
 **************************************************************************************************/
uint8_t ICM_WhoAmI(void)
{
	uint8_t spi_data = 0x01;
	ICM_ReadOneByte(0x00, &spi_data);
	return spi_data;
}


/***************************************************************************************************//**
 * @brief Determines the offset of the accelerometer and gyroscope
 * and sets the offset values in the corresponding registers
 * @
 **************************************************************************************************/
void ICM_ConfigureOffset(ACC_SENS acc_sens, GYR_SENS gyr_sens)
{
	int i,j,k;
	int32_t acc_mean[3] 	= {0};
	int32_t gyr_mean[3] 	= {0};
	int32_t acc_offset[3] = {0};
	int32_t gyr_offset[3] = {0};

	// dividers for different sensitivities
	int8_t  acc_div = 0;
	int8_t  gyr_div = 0;
	int16_t offset[3]; // x,y,zs
	uint8_t raw_data[6];

	acc_div = ICM_SensDiv(acc_sens);
	gyr_div = ICM_SensDiv(gyr_sens);

	ICM_ResetOffset();
	HAL_Delay(1000);

	// add values to get sum
	// divide to get mean
	// divide for sens
	// set new offset
	// repeat

	// loop to set offset
	// averaging once doesn't work because axis values can clip at certain sensitivities
	for(i=0;i<ITERATIONS;i++)
	{
		for(j=0;j<3;j++) //clear the mean
		{
			acc_mean[j] = 0;
			gyr_mean[j] = 0;
		}
		for(j=0;j<MEAN_COUNT;j++) // get sum of MEAN_COUNT amount of data
		{
			ICM_ReadAccelGyro();

			for(k=0;k<3;k++) // add the newly retrieved data to the sum
			{
				acc_mean[k] += robot.accel_data[k];
				gyr_mean[k] += robot.gyro_data[k];
			}

			printf("Raw = (Ax: %d | Ay: %d | Az: %d | Gx: %d | Gy: %d | Gz: %d)\r\n",
					robot.accel_data[0], robot.accel_data[1], robot.accel_data[2],
					robot.gyro_data[0],  robot.gyro_data[1],  robot.gyro_data[2]);
		}


		printf("total = (Ax: %li | Ay: %li | Az: %li | Gx: %li | Gy: %li | Gz: %li)\r\n",
				acc_mean[0], acc_mean[1], acc_mean[2], gyr_mean[0], gyr_mean[1], gyr_mean[2]);

		printf("sensitivity: %u , %u\r\n", acc_sens, gyr_sens);


		for(j=0;j<3;j++) //this function doesn't work properly
		{
			// sum dived by number of summed numbers
			acc_mean[j] = (acc_mean[j] / MEAN_COUNT);
			gyr_mean[j] = (gyr_mean[j] / MEAN_COUNT);
			printf("(A: %li | G: %li \r\n", acc_mean[j], gyr_mean[j]);

			// determines if the mean should be added or
			// subtracted from the offset
			if(acc_mean[j] >= 0 && acc_offset[j] < 0)		acc_mean[j] = -acc_mean[j] / acc_div;
			else if(acc_mean[j] < 0 && acc_offset[j] < 0) 	acc_mean[j] = -acc_mean[j] / acc_div;
			else if(acc_mean[j] < 0 && acc_offset[j] >= 0) 	acc_mean[j] = -acc_mean[j] / acc_div;
			else if(acc_mean[j] >= 0 && acc_offset[j] >= 0) acc_mean[j] = -acc_mean[j] / acc_div;

			if(gyr_mean[j] >= 0 && gyr_offset[j] < 0)		gyr_mean[j] = -gyr_mean[j] / gyr_div;
			else if(gyr_mean[j] < 0 && gyr_offset[j] < 0) 	gyr_mean[j] = -gyr_mean[j] / gyr_div;
			else if(gyr_mean[j] < 0 && gyr_offset[j] >= 0) 	gyr_mean[j] = -gyr_mean[j] / gyr_div;
			else if(gyr_mean[j] >= 0 && gyr_offset[j] >= 0) 	gyr_mean[j] = -gyr_mean[j] / gyr_div;

			if(i > 0)	//after the first iteration
			{
				acc_mean[j] = acc_offset[j] + acc_mean[j];
				gyr_mean[j] = gyr_offset[j] + gyr_mean[j];
			}

			acc_offset[j] = acc_mean[j];
			gyr_offset[j] = gyr_mean[j];
		}

		printf("offset = (Ax: %li | Ay: %li | Az: %li | Gx: %li | Gy: %li | Gz: %li)\r\n",
				acc_mean[0], acc_mean[1], acc_mean[2], gyr_mean[0], gyr_mean[1], gyr_mean[2]);

		ICM_SetOffset(&acc_mean[0], &gyr_mean[0]);//set new offset

		HAL_Delay(200);
	}

	acc_mean[2] = acc_mean[2] + 2048; // add gravity offset to the z axis
	ICM_SetOffset(&acc_mean[0], &gyr_mean[0]);// set final offset

	ICM_SelectBank(USER_BANK_1);
	for(i=0;i<3;i++) //read offset acc
	{
		ICM_ReadBytes((0x14 + i*3), &raw_data[i*2], 2);
		offset[i] = ((raw_data[i*2] << 8) | (raw_data[i*2+1] & 0xFF));
	}

	printf("x_offs: %d | y_offs: %d | z_offs: %d\r\n", offset[0], offset[1], offset[2]);
	printf("gyrx_offs: %li | gyry_offs: %li | gyrz_offs: %li\r\n", gyr_mean[0], gyr_mean[1], gyr_mean[2]);
}


/***************************************************************************************************//**
 * @brief Determines the divider to get the 'real' value
 *  from the int16_t accelerometer and gyroscope readings
 *
 *
 * @param acc 						: Value determines which accelerometer sensitivity is selected
 * @param gyr						: Value determines which gyroscope sensitivity is selected
 **************************************************************************************************/
void ICM_Sensitivity(ACC_SENS acc, GYR_SENS gyr)
{
	switch(acc)
	{
		case 0:  acc_sens_val = 16384.0f; break;
		case 1:  acc_sens_val = 8192.0f;  break;
		case 2:  acc_sens_val = 4096.0f;  break;
		case 3:  acc_sens_val = 2048.0f;  break;
		default: break;
	}

	switch(gyr)
	{
		case 0:  gyr_sens_val = 131.0f; break;
		case 1:  gyr_sens_val = 65.5f;  break;
		case 2:  gyr_sens_val = 32.8f;  break;
		case 3:  gyr_sens_val = 16.4f;  break;
		default: break;
	}
}


/***************************************************************************************************//**
 * @brief Used to determine a divider needed for the offset values
 *
 *
 * @param sens 							: Sensitivity value to determine divider
 * @retval								: Divider value based on sensitivity
 **************************************************************************************************/
int8_t ICM_SensDiv(uint8_t sens)
{
	int8_t sens_div = 0;
	switch(sens)
	{
		case 0:  sens_div = 8; break;
		case 1:  sens_div = 4; break;
		case 2:  sens_div = 2; break;
		case 3:  sens_div = 1; break;
		default: break;
	}

	return sens_div;
}


/***************************************************************************************************//**
 * @brief Sets the offset register of the accelerometer and gyroscope to zero
 **************************************************************************************************/
void ICM_ResetOffset(void)
{
	uint8_t i;

	ICM_SelectBank(USER_BANK_1); //Reset offset Accel
	for(i=0;i<3;i++)
	{

		ICM_WriteOneByte(0x14 + 3 * i, 0x00);	//X acc high byte
		HAL_Delay(1);
		ICM_WriteOneByte(0x15 + 3 * i, 0x00);	//X acc low byte
		HAL_Delay(1);
	}

	ICM_SelectBank(USER_BANK_2); //Reset offset Gyro
	for(i=0;i<3;i++)
	{
		ICM_WriteOneByte(0x03 + 2 * i, 0x00);	//X gyro high byte
		HAL_Delay(1);
		ICM_WriteOneByte(0x04 + 2 * i, 0x00);	//X gyro low byte
		HAL_Delay(1);
	}
}


/***************************************************************************************************//**
 * @brief Sets the offset register of the accelerometer and gyroscope
 *
 * @param pacc_mean 				:pointer to array where accelerometer offset values are stored
 * @param pgyr_mean					:pointer to array where gyroscope offset values are stored
 **************************************************************************************************/
void ICM_SetOffset(int32_t* pacc_mean,int32_t* pgyr_mean)
{
	uint8_t i;

	ICM_SelectBank(USER_BANK_1);//write offset to icm for acc
	for(i=0;i<3;i++)
	{
		ICM_WriteOneByte(0x14 + 3 * i, *(pacc_mean + i) >> 8);		//X acc high byte
		HAL_Delay(1);
		ICM_WriteOneByte(0x15 + 3 * i, *(pacc_mean + i) & 0x00FE);	//X acc low byte
		HAL_Delay(1);
	}

	ICM_SelectBank(USER_BANK_2);//write offset to icm for gyr
	for(i=0;i<3;i++)
	{
		ICM_WriteOneByte(0x03 + 2 * i, *(pgyr_mean + i) >> 8);		//X gyr high byte
		HAL_Delay(1);
		ICM_WriteOneByte(0x04 + 2 * i, *(pgyr_mean + i) & 0x00FF);	//X gyr low byte
		HAL_Delay(1);
	}
}
/**
  * @}
  */
/**
  * @}
  */
