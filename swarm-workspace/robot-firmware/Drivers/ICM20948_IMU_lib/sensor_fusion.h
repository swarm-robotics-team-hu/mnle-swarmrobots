/**
  ******************************************************************************
  * @file           : sensor_fusion.h
  * @brief          : Library for fusion of IMU accelerometer and gyroscope data to produce orientation estimation
  * Description		: This is a universal c library that implements the Madgwick and Mahony algorithms for IMU sensor fusion.
  * References		: This library was written by Sebastian Madgwick and modified by the Swarm Robotics Team:
  * 					- https://www.x-io.co.uk/res/doc/madgwick_internal_report.pdf
  * 					- http://en.wikipedia.org/wiki/Fast_inverse_square_root
  * @author			: Swarm Robotics Team
  * @author			: Sebastian Madgwick
  * @date			: 02-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup sensor_fusion
  * @brief	All code related to the sensor fusion
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SENSOR_FUSION_H
#define __SENSOR_FUSION_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "math.h"
#include "ICM20948.h"
#include "pos_tracking.h"

/* Defines -------------------------------------------------------------------*/
#define Kp (2.0f * 0.5f)						/**< Proportional gain used in Mahony filter	*/
#define Ki (0.0f)								/**< Integral gain used in Mahony filter		*/
#define rad(degree) (degree * (M_PI/180.0f))	/**< Marco for converting degrees to radians	*/
#define degree(rad)	(rad	* (180.0f/M_PI))	/**< Macro for converting radians to degrees	*/

/* Global variables ----------------------------------------------------------*/
/* parameters for 6 DoF sensor fusion calculations */
float beta;										/**< 2 * proportional gain (Kp)			*/
float integralFB_x, integralFB_y, integralFB_z;	/**< integral error terms scaled by Ki	*/

/* Prototypes ----------------------------------------------------------------*/
void  SensorFusionInit(void);
void  UpdateQuaternionMadgwick(float ax, float ay, float az, float gx, float gy, float gz);
void  UpdateQuaternionMahony(float ax, float ay, float az, float gx, float gy, float gz);
float InvSqrt(float x);

#endif

/**
  * @}
  */
/**
  * @}
  */
