/**
  ******************************************************************************
  * @file           : ICM20948.h
  * @brief          : ICM20948 drivers
  * References		: This library was written by CoryCline and modified by the Swarm Robotics Team:
  * 					- https://github.com/CoryCline/ICM20948
  * @author			: Swarm Robotics Team
  * @author			: CoryCline
  * @date			: 02-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup ICM20948
  * @brief	All code related to the sensor fusion
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ICM20948_H_
#define __ICM20948_H_

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Defines -------------------------------------------------------------------*/
#define SPI_BUS			(&hspi1) 		/**< The SPI Bus used by the IMU	*/
#define UART_BUS		(&huart3)		/**< The UART Bus used by the IMU	*/

#define USER_BANK_SEL	(0x7F)			/**< Register to select user bank	*/
#define USER_BANK_0		(0x00)			/**< User bank 0	*/
#define USER_BANK_1		(0x10)			/**< User bank 1	*/
#define USER_BANK_2		(0x20)			/**< User bank 2	*/
#define USER_BANK_3		(0x30)			/**< User bank 3	*/

#define PWR_MGMT_1 		(0x06)			/**< Power management register 1	*/
#define PWR_MGMT_2		(0x07)			/**< Power management register 2	*/
#define GYRO_CONFIG_1	(0x01)			/**< Gyro config register	*/

#define CLK_BEST_AVAIL	(0x01)			/**< Clock source select value 	*/
#define GYRO_RATE_250	(0x00)			/**< Gyro sampling rate value	*/
#define GYRO_LPF_17HZ 	(0x29)			/**< Gyro LPF value	*/

#define MAG_CONVERT_VAL	(0.15f)		/**< 0.15 uT per decimal 1 value read from register */

#define MEAN_COUNT 		50	/**< Number of total measurements before taking the mean */
#define ITERATIONS 		15	/**< Number of cycles of taking measurments and adjusting the offset	*/
#define MAG_DELAY_US	125	/**< Number of delays needed for magnetometer	(ms)	*/
/* Enums -------------------------------------------------------------------*/
typedef enum
{
	GYR_DPS250 = 0,
	GYR_DPS500,
	GYR_DPS1000,
	GYR_DPS2000
} GYR_SENS;

typedef enum
{
	ACC_G2 = 0,
	ACC_G4,
	ACC_G8,
	ACC_G16
} ACC_SENS;

/* Global variables -------------------------------------------------------------------*/
int16_t accel_data[3];	/**< Accelerations x, y, z	*/
int16_t gyro_data[3];	/**< Degrees per second x, y, z	*/
int16_t mag_data[3];	/**< Magnetism x, y, z	*/
float 	acc_sens_val;	/**< Divider value based on accel sens	*/
float 	gyr_sens_val;	/**< Divider value based on gyro  sens	*/

/* Prototypes ----------------------------------------------------------------*/
void 	 ICM_PowerOn(void);
uint8_t  ICM_WhoAmI(void);
void 	 ICM_SelectBank(uint8_t bank);
void 	 ICM_ReadAccelGyro(void);
void 	 ICM_MagWriteI2C(uint8_t reg,uint8_t value);
uint8_t	 ICM_MagReadReg(uint8_t reg);
void 	 ICM_ReadMag(float *magn);
void 	 ICM_ReadBytes(uint8_t reg, uint8_t *pData, uint16_t Size);
uint16_t ICM_Initialize(ACC_SENS acc_sens, GYR_SENS gyr_sens);
void 	 ICM_DisableI2C(void);
void 	 ICM_CSHigh(void);
void 	 ICM_CSLow(void);
void 	 ICM_SetClock(uint8_t clk);
void 	 ICM_AccelGyroOff(void);
void 	 ICM_AccelGyroOn(void);
void 	 ICM_SetGyroRateLPF(uint8_t rate, uint8_t lpf);
void 	 ICM_SetGyroLPF(uint8_t lpf);
void 	 ICM_Sensitivity(ACC_SENS acc, GYR_SENS gyr);
void 	 ICM_ConfigureOffset(ACC_SENS acc_sens, GYR_SENS gyr_sens);
int8_t 	 ICM_SensDiv(uint8_t sens);
void 	 ICM_ResetOffset();
void 	 ICM_SetOffset(int32_t* pacc_mean,int32_t* pgyr_mean);

#endif /* ICM20948_H_ */

/**
  * @}
  */
/**
  * @}
  */
