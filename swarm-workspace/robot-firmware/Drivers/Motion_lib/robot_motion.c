/**
  ******************************************************************************
  * @file           : robot_motion.c
  * @brief          : This library lets the robot rotate and drive a certain distance
  *
  * @author			: Swarm Robotics Team
  * @date			: 02-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup Position_Tracking
  * @brief	All code related to motion
  * @{
  */


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "robot_motion.h"
#include "rotary_encoder_lib.h"


/***************************************************************************************************//**
 * @brief This function controls the servo's and constantly check how far the robot has travelled and
 * how far it still has to go
 *
 * @param travel_length 						:The length the robot needs to travel
 **************************************************************************************************/
int RobotTravel(float travel_length, uint8_t vel)
{
	int i;
	float dx,dy = 0;

	if(travel_length < 0.01)
		return 1;

	RobotRotate(robot.travel_ori, 50);

	HAL_TIM_Encoder_Start(&htim1, TIM_CHANNEL_1);
	HAL_TIM_Encoder_Start(&htim2, TIM_CHANNEL_1);

	robot.ori[Z] = robot.IMU_ori[Z];

	robot.pos_buff[X][NEW] = robot.pos[X];
	robot.pos_buff[Y][NEW] = robot.pos[Y];
	robot.traveled_distance[NEW] = 0;

	ServoSetSpeed(LEFT_SERVO, 50);
	ServoSetSpeed(RIGHT_SERVO, 65);

	/*  && robot.pos[0] == robot.new_pos[0] && robot.pos[1] == robot.new_pos[1] */
	while(robot.traveled_distance[NEW] <= robot.travel_dis)
	{
		/* Clear position variables of RE and IMU */
		for(i=0; i<2; i++)
		{
			robot.pos_RE[i] = 0;
			robot.pos_IMU[i] = 0;
		}

		for(i=0; i<2; i++)
		{
			robot.pos_buff[i][OLDEST] = robot.pos_buff[i][OLD];
			robot.pos_buff[i][OLD] = robot.pos_buff[i][NEW];
		}

		robot.yaw_RE[OLDEST] = robot.yaw_RE[OLD];
		robot.yaw_RE[OLD] = robot.yaw_RE[NEW];

		robot.traveled_distance[OLDEST] = robot.traveled_distance[OLD];
		robot.traveled_distance[OLD] = robot.traveled_distance[NEW];

		/* Estimate IMU and RE traveled distance */
		IMU_EstimatePose();
		Odometry_RE();

		/* PID correction */
//			if(robot.servoR.status != 1 || robot.servoL.status != 1)
//				KeepItStraight(&robot.drive_control);

		/* Moments of acceleration and de-acceleration are handled by the IMU */
		if((robot.accel[X] > 500) || (robot.accel[X] < -500))
		{
			printf("accelerate ");
			robot.pos_buff[X][NEW] = robot.pos_buff[X][OLD] + robot.pos_IMU[X];
			robot.pos_buff[Y][NEW] = robot.pos_buff[Y][OLD] + robot.pos_IMU[Y];
		}
		else	/* During constant speed */
		{

			//printf("constant   ");
			robot.accel[X] = 0;
			robot.pos_buff[X][NEW] = robot.pos_buff[X][OLD] + robot.pos_RE[X];
			robot.pos_buff[Y][NEW] = robot.pos_buff[Y][OLD] + robot.pos_RE[Y];
		}

		/* seet the new orientation/position values	*/
		robot.pos[X] = robot.pos_buff[X][NEW];
		robot.pos[Y] = robot.pos_buff[Y][NEW];
		robot.ori[Z] = robot.IMU_ori[Z];

		dx = robot.pos_buff[X][NEW] - robot.pos_buff[X][OLD];
		dy = robot.pos_buff[Y][NEW] - robot.pos_buff[Y][OLD];


		if(robot.servoR.status == 2 || robot.servoL.status == 2)
		{
			robot.traveled_distance[OLD] = robot.traveled_distance[OLDEST];
			robot.traveled_distance[NEW] = robot.traveled_distance[OLD];
			//printf("back to old distance \n\r");
		}
		else
			robot.traveled_distance[NEW] += sqrt((dx * dx) + (dy * dy));			/* Traveled distance on x-axis of the robot */

//			printf("x-acc: %f \t IMU yaw: %.2f \t RE yaw: %.2f \t yaw: %.2f \t pos x: %.2f \t pos y: %.2f \t dis %.2f \n\r",
//					robot.accel[X]
//					, robot.IMU_ori[Z]
//					, robot.yaw_RE[NEW]
//					, robot.ori[Z]
//					, robot.pos[X]
//					, robot.pos[Y]
//					, robot.traveled_distance[NEW]);
		printf("yaw: %.2f \t pos x: %.2f \t pos y: %.2f \t dis %.2f \n\r"
				, robot.ori[Z]
				, robot.pos[X]
				, robot.pos[Y]
				, robot.traveled_distance[NEW]);

	}
	ServoSetSpeed(LEFT_SERVO, 0);
	ServoSetSpeed(RIGHT_SERVO, 0);
	HAL_TIM_Encoder_Stop(&htim1, TIM_CHANNEL_1);
	HAL_TIM_Encoder_Stop(&htim2, TIM_CHANNEL_1);

	return 1;
}

/***************************************************************************************************//**
 * @brief Rotate the robot to the desired degrees (enter between 0:360)
 *
 * @param degrees 						:The desired angle the robot needs to rotate to
 * @retval error						:Returns the error
 **************************************************************************************************/
uint8_t RobotRotate(float degrees, uint8_t vel)
{
	vel = 45;
	int diff = 0;

	HAL_TIM_Base_Stop_IT(&htim9);
	HAL_TIM_Base_Start_IT(&htim9);

	/* Prevent arguments that are to high or to low		*/
	if(degrees < 0)			{degrees = 0;}
	else if(degrees > 360)	{degrees = 360;}
	/* Prevent percentages that are to high or to low	*/
	if(vel < MIN_PCT)		{vel = MIN_PCT;}
	else if(vel > MAX_PCT)	{vel = MAX_PCT;}

	/* Calculate difference between 2 angles */
	if(robot.IMU_ori[Z] > degrees)
		diff = 360 + (degrees - robot.IMU_ori[Z]);
	else if(robot.IMU_ori[Z] < degrees)
		diff = degrees - robot.IMU_ori[Z];

	/* Check which rotation direction is shorter (left or right) */
	if((diff >= -ROTP) && (diff <= ROTP))	/* diff is less than ROTP away from zero */
		return 0;
	else if(diff <= 180)	/* Turn left */
	{
		ServoSetSpeed(LEFT_SERVO,   vel);
		ServoSetSpeed(RIGHT_SERVO, -vel);
	}
	else	/* Turn right */
	{
		ServoSetSpeed(LEFT_SERVO, -vel);
		ServoSetSpeed(RIGHT_SERVO, vel);
	}

	/* Constantly check how close you are */
	do
	{
		IMU_EstimateOri();

		printf("yaw: %.2f \t degrees: %.2f \r\n", robot.IMU_ori[Z], degrees);
	}
	while(!((robot.IMU_ori[Z] > (degrees - ROTP)) && (robot.IMU_ori[Z] < (degrees + ROTP))));

	ServoSetSpeed(LEFT_SERVO,  0);
	ServoSetSpeed(RIGHT_SERVO, 0);

	printf("Reached desired orientation");
	printf("IMU yaw: %f \r\n", robot.IMU_ori[Z]);
	printf("RE yaw:  %f \r\n", robot.yaw_RE[NEW]);

	return 1;
}

/**
  * @}
  */
/**
  * @}
  */
