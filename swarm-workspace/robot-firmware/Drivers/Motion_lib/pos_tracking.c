/**
  ******************************************************************************
  * @file           : pos_tracking.c
  * @brief          : Library for tracking the position of the robot
  * References		: This library uses magdwick filter and is further complemented by the Swarm Robotics Team:
  * 					- https://x-io.co.uk/open-source-ahrs-with-x-imu/
  * @author			: Swarm Robotics Team
  * @date			: 02-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with public for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */



/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup Position_Tracking
  * @brief	All code related to motion
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "pos_tracking.h"

/***************************************************************************************************//**
 * @brief This function calculates the distance the robot has to travel between the current and new position
 * and it calculates which orientation it needs to drive there
 *
 * @param new_pos 						:The new position of the robot
 **************************************************************************************************/
void CalculateTravel(float *new_pos)
{
	/* Calculate delta x and delta y */
	float dx, dy;
	dx = new_pos[Y] - robot.pos[Y];
	dy = new_pos[X] - robot.pos[X];

	/* Calculate travel distance and orientation for going to desired position */
	robot.travel_dis = sqrt((dx * dx) + (dy * dy));
	robot.travel_ori = atan2(dx, dy) * (180.0f / M_PI);

	if(robot.travel_ori < 0)
		robot.travel_ori = 360 + robot.travel_ori;

	printf("dis = %f ori = %f \n\r",robot.travel_dis, robot.travel_ori );
}

/***************************************************************************************************//**
 * @brief This function updates the robot's position on the grid
 *
 * @param dx 						:The difference in x
 * @param dy 						:The difference in y
 * @param yaw 						:The orientation of the robot
 **************************************************************************************************/
void UpdateGridmapXY(float dx, float dy, float yaw)
{
	robot.pos[X] = robot.pos[X] + (cos(yaw) * dy) + (sin(yaw) * dx);
	robot.pos[Y] = robot.pos[Y] + (sin(yaw) * dy) + (cos(yaw) * dx);
}

void IMU_EstimatePose(void)
{
	IMU_EstimatePos();
	IMU_EstimateOri();
}


/***************************************************************************************************//**
 * @brief Estimates the robot's position based on IMU data and previous speed and position
 *
 **************************************************************************************************/
void IMU_EstimatePos(void)
{
	int i;
	float g[3] = {0};	/* Gravity vector in g					*/
	//float q[4];
	float *q = IMU_UpdateQuaternion();

	/* QUATERNIONS ARE UPDATED IN INTERRUPT
	   Store quaternion values in q			*/
//	for(i=0; i<4; i++)
//		q[i] = robot.q[i];

	/*Calculate expected direction of gravity */
	g[0] = 2 * ((q[1] * q[3]) - (q[0] * q[2]));
	g[1] = 2 * ((q[0] * q[1]) - (q[2] * q[3]));
	g[2] = (q[0] * q[0]) - (q[1] * q[1]) - (q[2] * q[2]) + (q[3] * q[3]);

	/* Estimate acceleration, velocity and position for x and y */
	for(i=0; i<2; i++)
	{
		robot.accel[i]    = (robot.raw_accel[i] - g[i]) * 9.81;		/* Calculate robot acceleration with gravity compensation	*/
		robot.vel_IMU[i] += (robot.accel[i]   * robot.delta_t);		/* Calculate robot velocity									*/
		robot.pos_IMU[i] += (robot.vel_IMU[i] * robot.delta_t);		/* Calculate robot position 								*/
	}

	/* DEBUG: Uncomment to test */
	//printf("pos x: %.2f  pos y: %.2f  pos z: %.2f ",   robot.pos[0],   robot.pos[1],   robot.pos[2]);
	//printf("vel x: %.2f  vel y: %.2f  vel z: %.2f ",   robot.vel[0],   robot.vel[1],   robot.vel[2]);
	//printf("acc x: %.2f  acc y: %.2f  acc z: %.2f ", robot.accel[0], robot.accel[1], robot.accel[2]);
	//printf("r_acc x: %f c_acc y: %f c_acc z: %f \r\n", robot.accel[0], robot.accel[1], robot.accel[2]);
}


/***************************************************************************************************//**
 * @brief Returns orientations between 0:360 degrees
 *
 **************************************************************************************************/
void IMU_EstimateOri(void)
{
	int i;
//	float q[4];
//
//	//HAL_TIM_Base_Stop_IT(&htim9);
//
//	/* QUATERNIONS ARE UPDATED IN INTERRUPT
//	   Store quaternion values in q			*/
//	for(i=0; i<4; i++)
//		q[i] = robot.q[i];

	float *q = IMU_UpdateQuaternion();

	/* Calculate roll, pitch and yaw using Euler angles	*/
	robot.IMU_ori[0] = atan2(2.0f * ((q[0] * q[1]) + (q[2] * q[3]))   , (q[0] * q[0]) - (q[1] * q[1]) - (q[2] * q[2]) + (q[3] * q[3]));
	robot.IMU_ori[1] = -asin(2.0f * ((q[1] * q[3]) - (q[0] * q[2])));
	robot.IMU_ori[2] = atan2(2.0f * ((q[1] * q[2]) + (q[0] * q[3]))   , (q[0] * q[0]) + (q[1] * q[1]) - (q[2] * q[2]) - (q[3] * q[3]));

	/* Calculate degrees */
	for(i=0; i<3; i++)
	{
		robot.IMU_ori[i] *= -180.0f / M_PI;

		/* Convert -180:180 to 0:360 */
		if(robot.IMU_ori[i] < 0)
			robot.IMU_ori[i] = 360 + robot.IMU_ori[i];
		else if(robot.IMU_ori[i] >= 360.0)
			robot.IMU_ori[i] -= 360.0;
	}

	//HAL_TIM_Base_Start_IT(&htim9);

	/* DEBUG: Uncomment to test */
//	printf("Roll: %.2f \t Pitch: %.2f \t Yaw: %.2f \t dt: %f \r\n", robot.IMU_ori[0], robot.IMU_ori[1], robot.IMU_ori[2], robot.delta_t);
//	printf("%.2f, %.2f, %.2f\r\n", robot.ori[0], robot.ori[1], robot.ori[2]);	/* Only for python IMU visualizer */
//	HAL_Delay(10);															/* Only for python IMU visualizer */

}



/***************************************************************************************************//**
 * @brief Update quaternion with raw IMU data
 * @note Takes 61 us on average
 *
 * @retval robot.q				:The updated quaternion values
 **************************************************************************************************/
float *IMU_UpdateQuaternion(void)
{
	ICM_ReadAccelGyro();		/* Accelerometer data in G's, Gyroscope data in degrees/s (dps) */
	robot.delta_t = IMU_Get_dt();	/* Calculate integration time (delta t)							*/

	/* Calculate orientation quaternion with raw IMU data (gyro data in rad/s) */
	UpdateQuaternionMadgwick (robot.raw_accel[0],      robot.raw_accel[1],      robot.raw_accel[2],
						   rad(robot.raw_gyro[0]),  rad(robot.raw_gyro[1]),  rad(robot.raw_gyro[2]));

	/* DEBUG: Uncomment to test */
	//printf("q0: %f \t q1: %f \t q2: %f \t q3: %f \r\n", robot.q[0], robot.q[1], robot.q[2], robot.q[3]);

	return robot.q;
}



/***************************************************************************************************//**
 * @brief Calculate integration time (delta t)
 *
 * @retval temp_dt				:The calculated integration time
 **************************************************************************************************/
float IMU_Get_dt(void)
{
	float temp_dt;

	robot.tim_new = get_runtime();
	temp_dt = (float)(robot.tim_new.total_us - robot.tim_old.total_us) / 1000000.0f;
	robot.tim_old = robot.tim_new;

	if(temp_dt > 1)
		temp_dt = 0.002001;

	return temp_dt;
	//return 0.0101535;
}

/**
  * @}
  */
/**
  * @}
  */
