/**
  ******************************************************************************
  * @file          	: robot_motion.h
  * @brief        	: Library for robot control
  * Description		: This file provides code for the control of the robot, like moving and all
  * 				  the other commands the robot needs to execute
  * @author			: Swarm Robotics Team
  * @date			: 17-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup robot_motion
  * @brief	All code related to rotary encoders
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MOTION_ROBOT_H
#define __MOTION_ROBOT_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Defines -------------------------------------------------------------------*/
#define ROTP (0.5f)	/**< Rotation precision */

/* Prototypes ----------------------------------------------------------------*/
int RobotTravel(float travel_length, uint8_t vel);
uint8_t RobotRotate(float degrees, uint8_t vel);

#endif
/**
  * @}
  */
/**
  * @}
  */
