/**
  ******************************************************************************
  * @file          	: pos_tracking.h
  * @brief        	: Library for position tracking with rotary IMU
  * Description		: This file provides code for the position tracking with the value of the IMU instances.
  * @author			: Swarm Robotics Team
  * @date			: 17-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup pos_tracking
  * @brief	All code related to rotary encoders
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __POS_TRACKING_H
#define __POS_TRACKING_H


/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "math.h"
#include "ICM20948.h"
#include "sensor_fusion.h"

/* Defines -------------------------------------------------------------------*/
#define ACCEL_SENS (0.2f)		/**< Acceleration sensitivity	*/
#define VELOC_SENS (0.2f)		/**< Velocity sensitivity		*/

/* Prototypes ----------------------------------------------------------------*/
void   CalculateTravel(float *new_pos);
void   UpdateGridmapXY(float dx, float dy, float yaw);
void   IMU_EstimatePose(void);
void   IMU_EstimatePos(void);
void   IMU_EstimateOri(void);
float *IMU_UpdateQuaternion(void);
float  IMU_Get_dt(void);

#endif
/**
  * @}
  */
/**
  * @}
  */

