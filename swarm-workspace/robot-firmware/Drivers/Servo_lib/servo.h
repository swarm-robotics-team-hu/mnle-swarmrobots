/**
  ******************************************************************************
  * @file           : servo.h
  * @brief          : Library for servo motor control
  * Description		: This file provides code for the configuration of the servo instances.
  * @author			: Swarm Robotics Team
  * @date			: 10-10-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup servo
  * @brief	All code related to servo control
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __SERVO_H
#define __SERVO_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Defines -------------------------------------------------------------------*/
/* sensor servo	*/
/* ROBOT #1 */
#define SENSOR_US_STEP	(9.4444f)		/**< Step size of the sensor servo	(us)							*/
#define SENSOR_MIN_US   	580				/**< Minimal range of the pulse width of the sensor servo	(us)	*/
#define SENSOR_MAX_US		2280			/**< Maximal range of the pulse width of the sensor servo	(us)	*/

/* ROBOT #2 */
//#define SENSOR_US_STEP	  	(10.0f)			/**< Step size of the sensor servo	(us)							*/
//#define SENSOR_MIN_US   	750				/**< Minimal range of the pulse width of the sensor servo	(us)	*/
//#define SENSOR_MAX_US		2550			/**< Maximal range of the pulse width of the sensor servo	(us)	*/

#define MIN_ANGLE 			0				/**< Minimal range of the angel of the sensor servo		(degrees)	*/
#define MAX_ANGLE 			179				/**< Maximal range of the angel of the sensor servo		(degrees)	*/
/* wheel servo */
#define MIN_US				1250			/**< Minimal range of the pulse width of the wheel servo	(us)	*/
#define MAX_US				1750			/**< Maximal range of the pulse width of the wheel servo	(us)	*/
#define MIDDLE_US			1500			/**< Middle point of the wheel servo where the speed is 0	(us)	*/
#define MIN_PCT				-100			/**< Minimal percentage for servo set to speed	*/
#define MAX_PCT				100				/**< Maximal percentage for servo set to speed	*/
/* velocity */
#define STD_SPEED			100				/**< Standard speed				*/
#define STD_ROT_SPEED		30				/**< Standard rotation speed	*/

/* Enum ---------------------------------------------------------------------*/
/* Servo enumeration */
enum SERVOS
{
	RIGHT_SERVO = 0,		/**< Right servo number		*/
	LEFT_SERVO,		/**< Left servo number		*/
	SENSOR_SERVO		/**< Sensor servo number	*/
};

/* Structs ------------------------------------------------------------------*/
/* Duration struct for servos */
typedef struct
{
	bool enable_dur;	/**< Enables or disables duration functionality		*/
	int  end_time;		/**< Ending timestamp								*/
	int  run_dur;		/**< Duration for servo commands					*/
} duration;
duration servo_duration;

/* Prototypes ---------------------------------------------------------------*/
void	 SERVO_TASK(void);
void	 ServoInit(void);
void	 ServoSetSpeed(uint8_t servo, int rot_speed_pct);
void	 ServoSetAngle(uint8_t servo, int angle);
void 	 ServoConfigurePWM(uint8_t servo, int PWM_pulse_us);
uint16_t MapVal(float x, float in_min, float in_max, float out_min, float out_max);


#endif

/**
  * @}
  */
/**
  * @}
  */
