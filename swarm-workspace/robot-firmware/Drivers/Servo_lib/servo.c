/**
  ******************************************************************************
  * @file           : servo.c
  * @brief          : Library for servo motor control
  * Description		: This file provides code for the configuration of the servo instances.
  * @author			: Swarm Robotics Team
  * @date			: 10-10-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup servo
  * @brief	All code related to servo control
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "servo.h"

/*******************************************************************************************************//**
 * @brief		This function lets the robot drive for a specific amount of time.
 *
 *******************************************************************************************************/
void SERVO_TASK(void)
{
	/* If a command regarding the servo's has been received */
	if(servo_flag == 1)
	{
		/* Only if duration functionality is enabled (0 ms = infinite) */
		if((servo_duration.enable_dur == true) && (servo_duration.run_dur != 0))
		{
			runtime current_time = get_runtime();

			/* Check if rotation duration has past */
			if(current_time.total_ms > servo_duration.end_time)
			{
				ServoSetSpeed(LEFT_SERVO,  0);
				ServoSetSpeed(RIGHT_SERVO, 0);
				servo_flag = 0;
			}
		}
		else
		{
			servo_flag = 0;
		}
	}
}

/*******************************************************************************************************//**
 * @brief 	Init function to initialize the servo's and encoders.
 *
 *******************************************************************************************************/
void ServoInit(void)
{
	robot.servoL.servo_ID = LEFT_SERVO;
	robot.servoR.servo_ID = RIGHT_SERVO;

	robot.servoR.pulse_width_us = MIDDLE_US;
	robot.servoR.REvalue[NEW]		= 0;
	robot.servoR.RE_dval 			= 0;
	robot.servoR.REtime_old[NEW] 	= 0;
	robot.servoR.RE_dt 				= 0;

	robot.servoL.pulse_width_us = MIDDLE_US;
	robot.servoL.REvalue[NEW] 		= 0;
	robot.servoL.RE_dval 			= 0;
	robot.servoL.REtime_old[NEW] 	= 0;
	robot.servoL.RE_dt 				= 0;

	/* set servo's to speed = 0 */
	ServoSetSpeed(LEFT_SERVO, 0);
	ServoSetSpeed(RIGHT_SERVO, 0);

	HAL_TIM_Encoder_Stop(&htim1, TIM_CHANNEL_1);
	HAL_TIM_Encoder_Stop(&htim2, TIM_CHANNEL_1);

	slip_flag = FALSE;
}

/*******************************************************************************************************//**
 * @brief	Function for adjusting PWM pulse values to change servo rotation speed
 *
 * @param 	servo 			:	Servo number that will be controlled
 * @param	rot_speed_pct	:	Speed in percentage (between -100 and 100 for clockwise and counter clockwise)
 *
 *******************************************************************************************************/
void ServoSetSpeed(uint8_t servo, int rot_speed_pct)
{
	/* Prevent percentages that are to high or to low */
	if(rot_speed_pct < MIN_PCT)			{rot_speed_pct = MIN_PCT;}
	else if(rot_speed_pct > MAX_PCT)	{rot_speed_pct = MAX_PCT;}

	if(servo == LEFT_SERVO)
	{
		robot.servoL.pulse_width_us = MapVal(rot_speed_pct, MIN_PCT, MAX_PCT, MIN_US, MAX_US);	//Map percentages to microseconds
		ServoConfigurePWM(servo, robot.servoL.pulse_width_us);
		//printf("Servo L PWM = %d \n\r", robot.servoL.pulse_width_us);									//Configure PWM settings for specified servo

	}

	if(servo == RIGHT_SERVO)
	{
		robot.servoR.pulse_width_us = MapVal(rot_speed_pct, MIN_PCT, MAX_PCT, MAX_US, MIN_US);	//Map percentages to microseconds
		ServoConfigurePWM(servo, robot.servoR.pulse_width_us);
		//printf("Servo R PWM = %d \n\r", robot.servoR.pulse_width_us);

	}
}

/*******************************************************************************************************//**
 * @brief	Function for setting timer values to change servo angle to desired angle
 * @note	Only works for sensor servo
 *
 * @param	servo	:	Servo number that will be controlled
 * @param	angle	:	Angle between 0 and 180 degrees
 *******************************************************************************************************/
void ServoSetAngle(uint8_t servo, int angle)
{
	/* Prevent angles that are to high or to low */
	if(angle < MIN_ANGLE)		{angle = MIN_ANGLE;}
	else if(angle > MAX_ANGLE)	{angle = MAX_ANGLE;}

	int pulse_width_us = angle * SENSOR_US_STEP + SENSOR_MIN_US;	/* Map percentages to microseconds */
	ServoConfigurePWM(servo, pulse_width_us);						/* Configure PWM settings for specified servo */
}

/*******************************************************************************************************//**
 * @brief	Function for setting timer values to change servo speed
 *
 * @param	servo			:	Servo number that will be controlled
 * @param	PWM_pulse_us	:	Angle between 0 and 180 degrees
 *******************************************************************************************************/
void ServoConfigurePWM(uint8_t servo, int PWM_pulse_us)
{
	/* PWM configuration */
	TIM_OC_InitTypeDef s_config_OC = {0};
	s_config_OC.OCMode     = TIM_OCMODE_PWM1;
	s_config_OC.Pulse      = PWM_pulse_us;			/* Value between 1000 and 2000 microseconds	*/
	s_config_OC.OCPolarity = TIM_OCPOLARITY_HIGH;
	s_config_OC.OCFastMode = TIM_OCFAST_ENABLE;

	/* Left servo uses TIM3. Right servo uses TIM4, Sensor servo uses TIM5 */
	if(servo == LEFT_SERVO)
	{
		HAL_TIM_PWM_ConfigChannel(&htim3, &s_config_OC, TIM_CHANNEL_1);
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_1);
	}
	else if(servo == RIGHT_SERVO)
	{
		HAL_TIM_PWM_ConfigChannel(&htim3, &s_config_OC, TIM_CHANNEL_2);
		HAL_TIM_PWM_Start(&htim3, TIM_CHANNEL_2);
	}
	else if(servo == SENSOR_SERVO)
	{
		HAL_TIM_PWM_ConfigChannel(&htim4, &s_config_OC, TIM_CHANNEL_1);
		HAL_TIM_PWM_Start(&htim4, TIM_CHANNEL_1);
	}
}

/*******************************************************************************************************//**
 * @brief	Function for mapping a number from one numeric range to another
 *
 * @param	x		:	Input value
 * @param 	in_min	:	Input numeric range minimum
 * @param 	in_max	:	Input numeric range maximum
 * @param 	out_min	:	Output numeric range minimum
 * @param 	out_max	:	Output numeric range maximum
 *
 * @retval	mapped value to pulse width	(us)
 *******************************************************************************************************/

uint16_t MapVal(float x, float in_min, float in_max, float out_min, float out_max)
{
	return (uint16_t) (out_min + ((x - in_min) * ((out_max - out_min) / (in_max - in_min))));
}



