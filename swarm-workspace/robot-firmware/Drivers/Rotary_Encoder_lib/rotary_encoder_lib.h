/**
  ******************************************************************************
  * @file          	: rotary_encoder_lib.h
  * @brief        	: Library for position tracking with rotary encoders.
  * Description		: This file provides code for the configuration of the rotary encoders instances.
  * @author			: Swarm Robotics Team
  * @date			: 17-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup rotary_encoder
  * @brief	All code related to rotary encoders
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __ROTARY_ENCODER_LIB_H
#define __ROTARY_ENCODER_LIB_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Defines -------------------------------------------------------------------*/
#define	ROTATION		(48.0f)			/**< The resolution of ticks of one rotation			*/
#define	SECONDS			1000000			/**< Amount of us in a second for conversion			*/
#define	MINUTES			60				/**< Amount of seconds in a minute for conversion		*/
#define	CIRCUMFERENCE	(0.225566f)		/**< The circumference value							*/
#define	AXIS_DISTANCE	(0.133f)		/**< The distance between the wheels					*/
#define MAX_COUNTER_VAL	96				/**< Max range of the encoder timer before it overflows	*/

/* Prototypes ----------------------------------------------------------------*/

void EncoderValue_read(wheelservo *servo);
void EncoderValue_dis(wheelservo *servo);
void GetDeltaEncoderValue(wheelservo *servo, uint8_t set);
void ReadEncoder(wheelservo *servo);
void Odometry_RE(void);
void SlipCheck(void);
void CorrectDistance(void);
void KeepItStraight(PID* PIDctrl);
void PID_init(void);
uint8_t DirCheck(uint8_t dir_new, wheelservo *servo);

#endif

/**
  * @}
  */
/**
  * @}
  */
