/**
  ******************************************************************************
  * @file          	: rotary_encoder_lib.c
  * @brief        	: Library for position tracking with rotary encoders.
  * Description		: This file provides code for the configuration of the rotary encoders instances.
  * @author			: Swarm Robotics Team
  * @date			: 17-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup rotary_encoder
  * @brief	All code related to rotary encoders
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "rotary_encoder_lib.h"

/*******************************************************************************************************//**
 * @brief	This function determines the DT of the values and time to determine the rpm
 *
 * @param 	servo	:	Pointer of servo struct
 *
 *******************************************************************************************************/
void EncoderValue_read(wheelservo *servo)
{
	/* read the encoder value and direction */
	servo->REvalue[OLDEST] = servo->REvalue[OLD];
	servo->REvalue[OLD] = servo->REvalue[NEW];

	servo->dir_bit[OLD] = servo->dir_bit[NEW];

	ReadEncoder(servo);
}
/*******************************************************************************************************//**
 * @brief	This function determines the DT of the values and time to determine the rpm
 *
 * @param 	servo	:	Pointer of servo struct
 *
 *******************************************************************************************************/
void EncoderValue_dis(wheelservo *servo)
{
	/* read the encoder value and direction */
	servo->REvalue[OLDEST] = servo->REvalue[OLD];
	servo->REvalue[OLD] = servo->REvalue[NEW];

	servo->dir_bit[OLD] = servo->dir_bit[NEW];

	ReadEncoder(servo);

	/* determine the delta encoder value */
	GetDeltaEncoderValue(servo, NEW);

	servo->distance_old = servo->distance;

	/* get traveled distance of the servo's */
	servo->distance = (((float)servo->RE_dval) / ROTATION) * CIRCUMFERENCE;
}

/*******************************************************************************************************//**
 * @brief	This function determines the Delta encoder value
 *
 * @param	servo pointer of servo struct
 *
 *******************************************************************************************************/
void GetDeltaEncoderValue(wheelservo *servo, uint8_t set)
{
    /* DIR Configuration
    LOW     ------> forwards
    HIGH    ------> backwards
    */

	/* Determine the delta encoder value */
	if((servo->REvalue[set] >= servo->REvalue[set+1]) && servo->dir_bit[set] == LOW)
	{
		/* normal forward */
//		printf("normal forward,");
		servo->RE_dval 	= servo->REvalue[set] - servo->REvalue[set+1];
	}
	else if((servo->REvalue[set] < servo->REvalue[set+1]) && servo->dir_bit[set] == LOW)
	{
		/* extreme forward */
//		printf("extreme forward,");

		servo->RE_dval 	= (MAX_COUNTER_VAL - servo->REvalue[set+1]) + servo->REvalue[set];
	}
	else if((servo->REvalue[set] > servo->REvalue[set+1]) && servo->dir_bit[set] == HIGH)
	{
		/* extreme backward */
//		printf("extreme backward,");

		servo->RE_dval 	= ((MAX_COUNTER_VAL - servo->REvalue[set]) + servo->REvalue[set+1]) * -1;
	}
	else if((servo->REvalue[set] <= servo->REvalue[set+1]) && servo->dir_bit[set] == HIGH)
	{
		/* normal backward */
//		printf("normal backward,");

		servo->RE_dval 	= servo->REvalue[set] - servo->REvalue[set+1];
	}

	if(servo->status == 3)
	{
		if(servo->RE_dval > 10)
		{
			if(servo->dir_bit[set] == HIGH)
				servo->RE_dval = -1;
			else if(servo->dir_bit[set] == LOW)
				servo->RE_dval = 1;
		}
	}

	/* print delta value */
	printf("RE dval = %d ", servo->RE_dval);
}



/*******************************************************************************************************//**
 * @brief	This function determines the Delta encoder value
 *
 * @param	servo	:	Pointer of servo struct
 *
 *******************************************************************************************************/
void ReadEncoder(wheelservo *servo)
{
	/* get the encoder value depending of the servo and direction */
	if(servo->servo_ID == LEFT_SERVO)
	{
		/* encoder value left */
		servo->REvalue[NEW]	= (int)TIM1->CNT;

		/* direction value left */
		servo->dir_bit[NEW] = DirCheck((uint8_t)(TIM1->CR1 & 0x10) >> 4,servo);

	}
	else if(servo->servo_ID == RIGHT_SERVO)
	{
		/* encoder value right */
		servo->REvalue[NEW]	= (int)TIM2->CNT;

		/* direction value right */
		servo->dir_bit[NEW] = DirCheck((uint8_t)(TIM2->CR1 & 0x10) >> 4, servo);

	}

	/* print encoder value */
//	printf("REvalue = %d ", servo->REvalue[NEW]);

}

/*******************************************************************************************************//**
 *	@brief 	This function keeps track of the position by look at the Rotary encoders
 *	@note	Code inspired from https://www.youtube.com/watch?v=XbXhA4k7Ur8
 *
 *******************************************************************************************************/
void Odometry_RE(void)
{
	/* local variables */
	double direction_c 	= 0;

	double distance_x 	= 0;
	double distance_y 	= 0;
	double vel_x 		= 0;
	double vel_y 		= 0;
	double phi			= 0;

	/* get delta time of the pos rotary encoders */
	robot.tim_new_RE 	= get_runtime();
	robot.delta_t_RE	= robot.tim_new_RE.total_us - robot.tim_old_RE.total_us;

	/* get RE values and distance */
	EncoderValue_dis(&robot.servoR);
	EncoderValue_dis(&robot.servoL);

	/* slip check */
	//SlipCheck();

	/* slip check -> action */
	if(slip_flag != FALSE)
	{
		/* IMU says there is no movement so
		 * the there wont be any traveled distance
		 */

		distance_x = 0;
		distance_y = 0;
	}
	else
	{
		/* The change of direction was correct, so re-use the measured distance */
		if(robot.servoR.status == 2 || robot.servoL.status == 2)
			CorrectDistance();

		/* determine the actual distance */
		direction_c = (robot.servoL.distance + robot.servoR.distance) / 2;
//		direction_c = (robot.servoR.distance + robot.servoR.distance) / 2;
		phi = (robot.servoL.distance - robot.servoR.distance) / AXIS_DISTANCE;

		/* determine the new phi orientation */
		robot.yaw_RE[NEW] = robot.yaw_RE[OLD] + degree(phi);

		if(robot.yaw_RE[NEW] >= 360.0)
			robot.yaw_RE[NEW] = -360.0;
		else if(robot.yaw_RE[NEW] <= -360.0)
			robot.yaw_RE[NEW] = 360.0;

		/* set yaw back to phi for further implementations */
		robot.phi_RE = rad(robot.yaw_RE[NEW]);
		//printf("yaw_RE = %.2f \t", robot.yaw_RE[NEW]);

		/* test */
		robot.phi_RE = 0;

		/* distance x-axis */
		distance_x = direction_c * cos(rad(robot.ori[Z]));	/* x' 	= x + l * cos(phi)	*/
		distance_y = direction_c * sin(rad(robot.ori[Z]));	/* y' 	= y + l * sin(phi)	*/

		vel_x = (distance_x / robot.delta_t_RE) * SECONDS;	/* v 	= s / t				*/
		vel_y = (distance_y / robot.delta_t_RE) * SECONDS;	/* v 	= s / t				*/
	}

	robot.pos_RE[X] += distance_x;
	robot.pos_RE[Y] += distance_y;

	robot.vel_RE[X] = vel_x;
	robot.vel_RE[Y] = vel_y;

	/* set the old time stamp */
	robot.tim_old_RE.total_us = robot.tim_new_RE.total_us;

}


/*******************************************************************************************************//**
 * @brief 	This function checks the right direction
 *
 * @param	dir		: 	Direction of rotary encoder
 * @param	servo 	:	Pointer of servo struct
 *
 *******************************************************************************************************/
uint8_t DirCheck(uint8_t dir_new, wheelservo *servo)
{

	uint8_t dir = 0;
	servo->RE_dir_buff[1] = servo->RE_dir_buff[0];
	servo->RE_dir_buff[0] = dir_new;

	/* the new value differs from old value
	 * in the last check the two values did not differ
	 * example |0 0| -> |0 1|
	 *
	 * this means the encoder might have changed direction but its not certain
	 * therefore, the last direction will be retained for another iteration
	 */
	if(servo->RE_dir_buff[1] != dir_new && (servo->dir_change != 1))
	{
		dir = servo->RE_dir_buff[1];
		servo->dir_change = 1;
		servo->status = 1;
	}
	/* the new value is the same as the old value
	 * in the last check the values did differ
	 * example |0 1| -> |1 1|
	 *
	 * this means the encoder definitely changed direction
	 * but because the last iteration was retained, means the calculated
	 * traveled length of the direct per servo was in the wrong direction
	 * therefore, the old values that have been calculated will be recalculated
	 * with the correct direction and must be replaced with the old values
	 */
	else if(servo->RE_dir_buff[1] == dir_new && (servo->dir_change == 1))
	{
		servo->dir_bit[OLD] = dir_new;
		servo->dir_change = 0;
		dir = dir_new;
		servo->status = 2;
	}
	/* the new value differs from the old value
	 * in the last check the values did differ
	 * example |0 1| -> |1 0|
	 *
	 * this means the encoder definitely did not change direction
	 * therefore, reset the direction to is old direction
	 */
	else if(servo->RE_dir_buff[1] != dir_new && (servo->dir_change == 1))
	{
		dir = dir_new;
		servo->dir_change = 0;
		servo->status = 3;
	}
	/* the new value is the same as the old value
	 * in the last check the values did not differ
	 * example |0 0| -> |0 0|
	 *
	 * this means all is good
	 * don't worry 'bout it
	 */
	else
	{
		dir = dir_new;
		servo->dir_change = 0;
		servo->status = 4;
	}

	printf("dir_new = %d || dir = %d || status = %d ",dir_new, dir, servo->status);

	return dir;
}

/*******************************************************************************************************//**
 * @brief 	This function corrects the old position and orientation if the direction
 * 			was correct after all
 *
 *******************************************************************************************************/
void CorrectDistance(void)
{
	printf("\n\r correction \n\r");
	/* local variables */
	double direction_c 	= 0;

	double distance_x 	= 0;
	double distance_y 	= 0;
	double vel_x 	= 0;
	double vel_y 	= 0;
	double phi		= 0;

	/* reset everything before the wrong correction was made */
	robot.pos_buff[X][OLD] = robot.pos_buff[X][OLDEST];
	robot.pos_buff[Y][OLD] = robot.pos_buff[Y][OLDEST];
	robot.yaw_RE[OLD] = robot.yaw_RE[OLDEST];

	printf("back to old coordinates \n\r");
	printf("x = %f y = %f yaw = %f \n\r", robot.pos_buff[X][OLD], robot.pos_buff[Y][OLD], robot.yaw_RE[OLD]);

	/* recalculate the x y and phi with the correct correction	*/

	/* determine the old delta encoder value but now with the current direction */
	printf("REnew %d REold %d REOLDEST %d",robot.servoR.REvalue[NEW],robot.servoR.REvalue[OLD],robot.servoR.REvalue[OLDEST]);
	GetDeltaEncoderValue(&robot.servoR, OLD);

	//printf("REnew %d REold %d REOLDEST %d",robot.servoL.REvalue[NEW],robot.servoL.REvalue[OLD],robot.servoL.REvalue[OLDEST]);
	GetDeltaEncoderValue(&robot.servoL, OLD);

	robot.servoR.distance = (((float)robot.servoR.RE_dval) / ROTATION) * CIRCUMFERENCE;
	robot.servoL.distance = (((float)robot.servoL.RE_dval) / ROTATION) * CIRCUMFERENCE;

	/* determine the actual distance */
	direction_c = (robot.servoL.distance + robot.servoR.distance) / 2;

	phi = (robot.servoL.distance - robot.servoR.distance) / AXIS_DISTANCE;

	/* determine the new phi orientation */
	robot.yaw_RE[OLD] = robot.yaw_RE[OLDEST] + degree(phi);

	if(robot.yaw_RE[OLD] >= 360.0)
		robot.yaw_RE[OLD] = -360.0;
	else if(robot.yaw_RE[OLD] <= -360.0)
		robot.yaw_RE[OLD] = 360.0;

	/* distance x-axis */
	distance_x = direction_c * cos(0);					/* x' 	= x + l * cos(phi)	*/
	distance_y = direction_c * sin(0);					/* y' 	= y + l * sin(phi)	*/

	vel_x = (distance_x / robot.delta_t_RE) / SECONDS;	/* v 	= s / t				*/
	vel_y = (distance_y / robot.delta_t_RE) / SECONDS;	/* v 	= s / t				*/

	robot.pos_RE[X] += distance_x;
	robot.pos_RE[Y] += distance_y;

	robot.vel_RE[X] = vel_x;
	robot.vel_RE[Y] = vel_y;

	printf("\n\rnew coordinates \n\r");
	printf("x = %f y = %f Phi = %f \n\r", robot.pos_RE[X], robot.pos_RE[Y], robot.yaw_RE[OLD]);

	/* move on with the current values	*/

	GetDeltaEncoderValue(&robot.servoR, NEW);
	GetDeltaEncoderValue(&robot.servoL, NEW);

	robot.servoR.distance = (((float)robot.servoR.RE_dval) / ROTATION) * CIRCUMFERENCE;
	robot.servoL.distance = (((float)robot.servoL.RE_dval) / ROTATION) * CIRCUMFERENCE;
}

/*******************************************************************************************************//**
 * @brief 	This function keeps the robot drive in a straight line
 *
 * @param	PIDctrl	:	pointer to PID struct
 *
 *******************************************************************************************************/
void KeepItStraight(PID* PIDctrl)
{
	float temp_us = 1500.0;

	/* read error */
	PIDctrl->error = robot.travel_ori - robot.ori[Z];
	//printf("e = %0.2f ",PIDctrl->error);
	/* [ -pi, pi ] */
	//PIDctrl->error = atan2(sin(error),cos(error));
	//printf("error = %0.2f ",PIDctrl->error);

	/* determine error over time */
	PIDctrl->error_DoT = PIDctrl->error - PIDctrl->error_old;
	//printf("dot = %0.2f ",PIDctrl->error_DoT);

	/* som of all errors */
	PIDctrl->E += PIDctrl->error;
	//printf("E = %0.2f ", PIDctrl->E);

	/* PID */
	PIDctrl->OUT = 	(PIDctrl->KP * PIDctrl->error) +
//					(PIDctrl->KI * PIDctrl->E) +
					(PIDctrl->KD * PIDctrl->error_DoT);
	//printf("U = %0.2f ",PIDctrl->OUT);

	/* give the correcotion value with correct round off*/
	temp_us = robot.servoR.pulse_width_us;
	temp_us -= PIDctrl->OUT;
	robot.servoR.pulse_width_us = (uint16_t)temp_us + 0.5;

	/* set limits */
	if(robot.servoR.pulse_width_us > MAX_US)
		robot.servoR.pulse_width_us = MAX_US;
	else if(robot.servoR.pulse_width_us < MIN_US)
		robot.servoR.pulse_width_us = MIN_US;

	/* set pwm */
	ServoConfigurePWM(RIGHT_SERVO, robot.servoR.pulse_width_us);
	//printf("pwm = %d ",robot.servoR.pulse_width_us);

	/* save old error */
	PIDctrl->error_old = PIDctrl->error;
}

/*******************************************************************************************************//**
 * @brief 	This function initializes PID values
 *
 *******************************************************************************************************/
void PID_init(void)
{
	robot.drive_control.error		= 0;
	robot.drive_control.error_old	= 0;
	robot.drive_control.error_DoT	= 0;
	robot.drive_control.E			= 0;

	robot.drive_control.KP			= 0.1;
	robot.drive_control.KI			= 0.005;
	robot.drive_control.KD			= 2;

	robot.drive_control.OUT			= 0;
}

/*******************************************************************************************************//**
 * @brief 	This function determines the Delta encoder value
 *
 *******************************************************************************************************/
void SlipCheck(void)
{
	/* get acc value */
//	IMU_EstimatePos();

	/* check if the IMU thinks the robot stands still */
	if(robot.accel[X] < 3 || robot.accel[X] > -3)
	{
		/* check if the robot slips */
		if(robot.servoR.RE_dval > robot.accel[X] || robot.servoR.RE_dval < robot.accel[X])
		{
			slip_flag = TRUE;	/* slip	*/
			printf("SLIP Right! \r\n");
		}
		else if(robot.servoL.RE_dval > robot.accel[X] || robot.servoL.RE_dval < robot.accel[X])
		{
			slip_flag = TRUE;	/* slip	*/
			printf("SLIP Left! \r\n");
		}
		else
			slip_flag = FALSE;	/* no slip	*/
	}
}

