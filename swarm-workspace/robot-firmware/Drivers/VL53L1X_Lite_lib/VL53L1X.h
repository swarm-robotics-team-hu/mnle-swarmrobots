/**
  ******************************************************************************
  * @file           : VL53L1X.h
  * @brief          : Library for distance sensor readings in combination with servo control
  * Description		: This file provides code for the configuration of the VL53L1X instances.
  * @author			: Swarm Robotics Team
  * @date			: 17-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup VL53L1X
  * @brief	All code related to the VL53L1X
  * @{
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __VL53L1X_H
#define __VL53L1X_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "VL53L1X_API.h"
#include "VL53l1X_calibration.h"
#include "stm32f4xx_hal.h"

/* Defines -------------------------------------------------------------------*/
#define SWEEP_STEPSIZE		1		/**< Step size of the sweep	*/
#define RANGEMARGE_RS		2000	/**< range marge 			*/

/* Constants -----------------------------------------------------------------*/
/* Array of all the offset for the distance measurement */
static const int Distance_offset[]= {5, 5, 18, 48, 55, 48, 54, 53, 48, 51, 43, 50, 67, 62, 64, 62, 72, 75, 83, 83, 104, 120};

/* Array of all the ranges for the offset for the distance measurement */
static const int Distance_ranges[][2]= {{0, 50}, {50, 100}, {100, 200}, {200, 500}, {500, 1000},
								{1000, 1100}, {1100, 1200}, {1200, 1300}, {1300, 1400},
								{1400, 1500}, {1500, 1600}, {1600, 1700}, {1700, 1800},
								{1800, 1900}, {1900, 2000}, {2000, 2100}, {2100, 2200},
								{2200, 2300}, {2300, 2400}, {2400, 2500}, {2500, 3000}};

/* Prototypes ----------------------------------------------------------------*/
void VL53L1X_Init(int amount);
void VL53L1X_Calibrate(uint8_t I2C_h, uint32_t Real_distance, uint8_t samples, uint8_t angle);
void VL53L1X_Read(uint8_t I2C_h, uint8_t angle);
void VL53L1X_Ranger(uint8_t I2C_h, uint8_t angle);

void SensorModuleRead(int amount, uint8_t pos);
void SensorModuleSweep(uint8_t min, uint8_t max, uint8_t stepsize);
void SensorModuleSetAngleRead(uint8_t angle);
void SensorModule_360_Print(void);


#endif
/**
  * @}
  */
/**
  * @}
  */
