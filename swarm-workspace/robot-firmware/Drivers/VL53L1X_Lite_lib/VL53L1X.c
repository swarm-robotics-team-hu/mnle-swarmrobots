/**
  ******************************************************************************
  * @file           : VL53L1X.h
  * @brief          : Library for distance sensor readings in combination with servo control
  * Description		: This file provides code for the configuration of the VL53L1X instances.
  * @author			: Swarm Robotics Team
  * @date			: 17-12-2019
  *
  * Copyright (C) 2019 - Swarm Robotics Team
  * This is a free software under the GNU license, you can redistribute it and/or modify it under the terms
  * of the GNU General Public License version 3 as published by the Free Software Foundation.
  * This software library is shared with puplic for educational purposes, without WARRANTY and Author is not liable for any damages caused directly
  * or indirectly by this software, read more about this on the GNU General Public License.
  ******************************************************************************
  */

/** @addtogroup Robot_Firmware
  * @brief	All the software for the RobotFirmware
  * @{
  */

/** @addtogroup VL53L1X
  * @brief	All code related to the VL53L1X
  * @{
  */

/* Includes ------------------------------------------------------------------*/
#include "VL53L1X.h"

/* Global Variables ----------------------------------------------------------*/
uint16_t			Dev_Default=0x52;				/**< Default Device I2C address						*/
uint8_t				Dev_Sensor[2]= {0x48,0x50};		/**< New Device addresses	[address1][address2]	*/
int 				status=0;						/**< VL53L1X error status							*/


/*******************************************************************************************************//**
 * @brief 	This function prints the 360 view of the sensor module
 *
 *******************************************************************************************************/
void SensorModule_360_Print(void)
{
	/* counter */
	int i,j;
	/* for loop to switch between sensors */
	for(i=0;i<2;i++)
	{
		/* for loop to switch between positions */
		for(j=0;j<180;j++)
		{
			/* Replaces the values with an error to -1
			 * this is defined in the mapping program
			 */
//			if(robot.sensor_module[i].RangeStatus[j] == 2 || robot.sensor_module[i].RangeStatus[j] == 4)
//			{
				if(robot.sensor_module[i].Distance[j] > RANGEMARGE_RS)
				{
					robot.sensor_module[i].Distance[j] = -1500;
				}
//			}
			printf("dis : %d rs : %d \n\r",robot.sensor_module[i].Distance[j], robot.sensor_module[i].RangeStatus[j]);
		}
	}

}

/*******************************************************************************************************//**
 * @brief 	This function allows the user to sweep the sensor module to get a ROV of 360 degrees
 * @note 	Please keep the range between 0 and 180.
 *
 * @param	min 		:	The minimum of the servo sweep.
 * @param	max 		:	The maximum of the servo sweep.
 * @param	stepsize	:	The stepsize of the sweep. The bigger the steps, the faster the sweep.
 *
 *******************************************************************************************************/
void SensorModuleSweep(uint8_t min, uint8_t max, uint8_t stepsize)
{
	/* flag to enable sensor reading */
	if(robot.sensor_read_flag == 1)
	{
		/* Counters */
		uint8_t i;

		/* set limiters */
		if(max > MAX_ANGLE)
			max = MAX_ANGLE;
		if(min < MIN_ANGLE)
			min = MIN_ANGLE;

		/* Save the stepsize */
		robot.sensor_module[0].stepsize = stepsize;
		robot.sensor_module[1].stepsize = stepsize;

		/* Sweep from 0 to 179 */
		for(i=min; i<=max; i+=stepsize)
		{
			/* set servo position and read VL53L1X data */
			SensorModuleSetAngleRead(i);
		}

		/* fix bad values */
		SensorModule_360_Print();

		/* set servo back */
		ServoSetAngle(SENSOR_SERVO, 0);
		HAL_Delay(750);

		robot.sensor_read_flag = 1;
		// --------------------------- //
		//	Sub function to sweep back //
		// --------------------------- //
		//
		//	/* Sweep from 180 to 0 */
		//	for(i=max; i>0; i-=stepsize)
		//	{
		//		/* set servo position and read VL53L1X data */
		//		SensorModuleSetPosRead(i-1);
		//	}
		//    SensorModule_360_Print(SWEEP_R);
	}
}

/*******************************************************************************************************//**
 * @brief 	This function sets the servo positions and calls the function to read the VL53L1X data
 * @note 	Please keep the pos between 0 and 180.
 *
 * @param	angle	:	The position of the servo.
 *
 *******************************************************************************************************/
void SensorModuleSetAngleRead(uint8_t angle)
{
	/* Set servo position */
	ServoSetAngle(SENSOR_SERVO, angle);

	/* Save the new position */
	robot.sensor_module[0].ServoAngle[angle] = angle;
	robot.sensor_module[1].ServoAngle[angle] = angle;

	/* Read out the two VL53L1X data, send the amount with the servo position */
	SensorModuleRead(2, angle);
}

/*******************************************************************************************************//**
 * @brief 	This function reads the data of multiple VL53L1X sensors
 * @note 	This function is currently made with an array which provides two different
 * 			I2C addresses. When 3 or more devices/sensors are connected, make sure to add new
 * 			I2C addresses to the array.
 *
 * @param	Amount	:	Amount of devices.
 * @param	angle	:	The position of the servo.
 *
 *******************************************************************************************************/
void SensorModuleRead(int amount, uint8_t angle)
{
	/* Counters */
	uint8_t i;

	for(i=0; i<amount; i++)
	{
		/* read the sensor */
		VL53L1X_Read(i, angle);
	}

//	printf("%u %u\n\r", robot.sensor_module[1].Distance[pos], robot.sensor_module[1].RangeStatus[pos]);
}

/*******************************************************************************************************//**
 * @brief 	This function reads the data of one VL53L1X sensor
 *
 * @param	I2C_h	:	The handler of the I2C device
 * @param	angle	:	Position of the servo
 *
 *******************************************************************************************************/
void VL53L1X_Read(uint8_t I2C_h, uint8_t angle)
{
	/* Read and display data */
	while (robot.sensor_module[I2C_h].dataReady == 0){
	  status = VL53L1X_CheckForDataReady(robot.sensor_module[I2C_h].Address, &robot.sensor_module[I2C_h].dataReady);
	  HAL_Delay(2);
	}
	robot.sensor_module[I2C_h].dataReady = 0;

	/* Get VL53L1X data */
	status = VL53L1X_GetRangeStatus(robot.sensor_module[I2C_h].Address, &robot.sensor_module[I2C_h].RangeStatus[angle]);
	status = VL53L1X_GetDistance(	robot.sensor_module[I2C_h].Address, &robot.sensor_module[I2C_h].Distance[angle]);

	/* apply offset */
	VL53L1X_Ranger(I2C_h, angle);

	/* clear interrupt has to be called to enable next interrupt*/
	status = VL53L1X_ClearInterrupt(Dev_Sensor[I2C_h]);
//	printf("%u %u\n\r", robot.sensor_module[I2C_h].Distance[pos], robot.sensor_module[I2C_h].RangeStatus[pos]);

}

/*******************************************************************************************************//**
  * @brief	This function corrects the values of VL53L1X with an offset
  *
  * @param	I2C_h	:	Device handler
  * @param	angle	:	angle of the servo 0-179
  *
 *******************************************************************************************************/
void VL53L1X_Ranger(uint8_t I2C_h, uint8_t angle)
{
	/* counters */
	int i;

	for(i=0;i<21;i++)
	{
		if (robot.sensor_module[I2C_h].Distance[angle] >= Distance_ranges[i][0] && robot.sensor_module[I2C_h].Distance[angle] < Distance_ranges[i][1])
		{
			robot.sensor_module[I2C_h].Distance[angle] += Distance_offset[i];
			break;
		}
	}
}

/*******************************************************************************************************//**
 * @brief 	This function calibrates one VL53L1X sensor.
 * 		it it measured some distances and compares its values to the real
 * 		values to make an offset (times 1024 to avoid floats)
 * @note 	The offset is times 1024 to avoid float calculations
 *
 * @param	I2C_h			:	The handler of the I2C device
 * @param	Real_distance	:	Input of the user
 * @param	samples			:	The resolution of the calibration
 * @param	angle			:	Position of the servo
 *
 *******************************************************************************************************/
void VL53L1X_Calibrate(uint8_t I2C_h, uint32_t Real_distance, uint8_t samples, uint8_t angle)
{
	/* local variable */
	uint32_t Sum_distance 		= 0;
	uint32_t Average_distance 	= 0;

	/* Counters */
	uint8_t i;

	/* For loop: read the sensor + return the sum of all saved values */
	for(i = 0; i<samples; i++)
	{
		/* read the sensor */
		VL53L1X_Read(I2C_h, angle);

		/* save and sum */
		Sum_distance += robot.sensor_module[I2C_h].Distance[angle];
	}

	/* Get the average distance by dividing the sum by the amount of samples */
	Average_distance = (Sum_distance / samples);
//	printf("%d \n\r", Average_distance);

	/* Get the offset factor by dividing by whole */
	robot.sensor_module[I2C_h].offset = (1024 * Real_distance) / Average_distance;

//	printf("%d \n\r", robot.sensor_module[I2C_h].offset);
	HAL_Delay(5000);
}

/*******************************************************************************************************//**
 * @brief 	This function initializes multiple VL53L1X sensors
 * @note 	This function is currently made with an array which provides two different
 * 			I2C addresses. When 3 or more devices/sensors are connected, make sure to add new
 * 			I2C addresses to the array.
 *
 * @param	amount	:	Amount of sensors.
 *
 *******************************************************************************************************/
void VL53L1X_Init(int amount)
{
	/* local variable */
	uint8_t byteData, sensorState=0;
	uint16_t wordData;

	/* Counters */
	uint8_t i;

    /**XSHUT GPIO Configuration
    PB13     ------> XSHUT_Device1
    PB14     ------> XSHUT_Device2
    */

	/* Disconnect the 2nd I2C device from the 3.3v */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_14, GPIO_PIN_SET);	/* device 1 */
	HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_RESET);	/* device 2 */


	for(i=0; i<amount; i++)
	{
		/* Those basic I2C read functions can be used to check your own I2C functions */
		status = VL53L1_RdByte(Dev_Default, 0x010F, &byteData);
		printf("VL53L1X Model_ID: %X\n\r", byteData);
		status = VL53L1_RdByte(Dev_Default, 0x0110, &byteData);
		printf("VL53L1X Module_Type: %X\n\r", byteData);
		status = VL53L1_RdWord(Dev_Default, 0x010F, &wordData);
		printf("VL53L1X: %X\n\r", wordData);

		while(sensorState==0){
			status = VL53L1X_BootState(Dev_Default, &sensorState);
			HAL_Delay(2);
		}
		printf("Chip %u booted\n\r", i);

		/* This function must to be called to initialize the sensor with the default setting  */
		status = VL53L1X_SensorInit(Dev_Default);
		/* Optional functions to be used to change the main ranging parameters
		 * according the application requirements to get the best ranging performances */

		/* Change address of the I2C device */
		status = VL53L1X_SetI2CAddress(Dev_Default, Dev_Sensor[i]);

		robot.sensor_module[i].Address = Dev_Sensor[i];

		/* Settings */
		status = VL53L1X_SetDistanceMode(robot.sensor_module[i].Address, 2); 			/* 1=short, 2=long */
		status = VL53L1X_SetTimingBudgetInMs(robot.sensor_module[i].Address, 20); 		/* in ms possible values [20, 50, 100, 200, 500] */
		status = VL53L1X_SetInterMeasurementInMs(robot.sensor_module[i].Address, 20); 	/* in ms, IM must be > = TB */
		status = VL53L1X_SetROI(robot.sensor_module[i].Address, 16, 16); 				/* minimum ROI 4,4 */

		/* Set the offset */
		robot.sensor_module[i].offset = 1024;

		/* This function has to be called to enable the ranging */
		status = VL53L1X_StartRanging(robot.sensor_module[i].Address);

		/* Connect the 2nd I2C device on the 3.3v */
		if(i==0)HAL_GPIO_WritePin(GPIOB, GPIO_PIN_13, GPIO_PIN_SET);
		printf("VL53L1X init done\n\r");

		/* Set servo position to position 0 */
		ServoSetAngle(SENSOR_SERVO, 0);
	}

}

