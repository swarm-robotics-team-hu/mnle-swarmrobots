/**
  ******************************************************************************
  * @file           : interpreter.c
  * @brief          : Library for interpreting commands
  * @author			: Swarm Robotics Team
  * @date			: 08-11-2019
  ******************************************************************************
  */

#include "interpreter.h"

void (*itrp_ptr)(uint8_t);	/* Interpreter function pointer */
uint8_t cmd_mask[] = {DRIVE, SENSOR, POSITION, MISC};
uint8_t ackpayload[32] = "Acknowledge by ROBOT";
uint8_t filled_sensordata = 0;
runtime scan_time;
runtime current_time;


/*******************************************************************************************************
 * @brief Start the right function with the command
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
void ITRP_CmdNumber(void)
{
	/* Store function addresses in void pointer */
	void *itrp_address[CMD_NUM_AMOUNT];
	itrp_address[0] = &ITRP_Drive;
	itrp_address[1] = &ITRP_Sensor;
	itrp_address[2] = &ITRP_Position;
	itrp_address[3] = &ITRP_Miscellaneous;

	if(filled_sensordata == 1)
	{
		current_time = get_runtime();
		if(scan_time.s + 5 < current_time.s)
		{
			filled_sensordata = 0;
		}
	}
	/* If a command has been received */
	if(nrf24_cmd_flag == 1)
	{
		NRF24_WriteAckPayload(1, "goedzo", sizeof("goedzo"));	/* for testing purposes */
		printf("Started interpreter \r\n");

		printf("BUFFER: ");
		int i;
		for(i=0; i<32; i++)
		{
			printf("%d,", cmd_buf[i]);
		}
		printf("\r\n");

		uint8_t cmd_nr = cmd_buf[0] >> 4;	/* Get most significant nibble for cmd num */

		/* Break if supported command num has been found */
		for(i=0; i<sizeof(cmd_mask); i++)
			if(cmd_nr == cmd_mask[i])
				break;

		itrp_ptr = itrp_address[i];	/* Point to function with found command num			    */
		printf("COMMAND: 0x%x \r\n", cmd_buf[0]);
		itrp_ptr(cmd_buf[0]);				/* Execute interpret function for specified command num */

		nrf24_cmd_flag = 0;			/* Set receiver flag to 0 */
	}
}

/*******************************************************************************************************
 * @brief This function is a switch case structure for the Drive commands (0x00)
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
uint8_t ITRP_Drive(uint8_t cmd)
{
	uint8_t error = 0;
	uint8_t val_1 = cmd_buf[1];																	/* Velocity or angle value	*/
	int32_t val_2 = (cmd_buf[2] << 24) | (cmd_buf[3] << 16) | (cmd_buf[4] << 8) | (cmd_buf[5]);	/* Duration or x-position	*/
	int32_t val_3 = (cmd_buf[6] << 24) | (cmd_buf[7] << 16) | (cmd_buf[8] << 8) | (cmd_buf[9]);	/* y-position				*/

	/* Go through each command and set servo parameters */
	/* Also set duration functionality (decides how long the servos will rotate) */
	switch(cmd)
	{
		case AHEAD:			ServoSetSpeed(LEFT_SERVO,  val_1);
							ServoSetSpeed(RIGHT_SERVO, val_1);
							servo_duration.enable_dur = true;
							break;

		case BACK:			ServoSetSpeed(LEFT_SERVO,  -val_1);
							ServoSetSpeed(RIGHT_SERVO, -val_1);
				  			servo_duration.enable_dur = true;
				  			break;

		case STANDSTILL:	ServoSetSpeed(LEFT_SERVO,  0);
							ServoSetSpeed(RIGHT_SERVO, 0);
						 	servo_duration.enable_dur = false;
						 	break;

		case LEFT_AXIS:		ServoSetSpeed(LEFT_SERVO, -val_1);
							ServoSetSpeed(RIGHT_SERVO, val_1);
		 	 	 	 		servo_duration.enable_dur = true;
		 	 	 	 		break;

		case RIGHT_AXIS:	ServoSetSpeed(LEFT_SERVO,   val_1);
							ServoSetSpeed(RIGHT_SERVO, -val_1);
	 	 					servo_duration.enable_dur = true;
	 	 					break;

		case ANGLE_AXIS:	RobotRotate(val_1, STD_ROT_SPEED);
							servo_duration.enable_dur = false;
							break;

		case GOTO_XY:		printf("val 3: %d \t val 2: %d \r\n", (int)val_3, (int)val_2);
							robot.new_pos[X] = (float)val_3 / 1000;
							robot.new_pos[Y] = (float)val_2 / 1000;
							printf("X: %f \t Y: %f \r\n", robot.new_pos[X], robot.new_pos[Y]);
							CalculateTravel(robot.new_pos);
							RobotTravel(robot.travel_dis, val_1);
							servo_duration.enable_dur = false;
							ITRP_TransmitPacket(2, 0x0A); /* Transmit a message when finished */
							break;

		case LEFT_SV: 		servo_duration.enable_dur = true;
							ServoSetSpeed(LEFT_SERVO, val_1);
							break;

		case RIGHT_SV: 		servo_duration.enable_dur = true;
							ServoSetSpeed(RIGHT_SERVO, val_1);
							break;

		default:			/* SET RETURN VALUE TO NOT OK */
							error = 1;
							break;
	}

	runtime current_time      = get_runtime();					/* Get current run time for servo duration functionality	*/
	servo_duration.run_dur    = val_2;							/* Set running duration of servos (0 ms = infinite)			*/
	servo_duration.end_time   = current_time.total_ms + val_2;	/* Set time that servos should stop rotating				*/
	servo_flag = 1;												/* Set flag for servo task									*/

	if(error!=1)
	{
		ITRP_TransmitPacket(2, cmd, robot.error);
	}

	return error;
}

/*******************************************************************************************************
 * @brief This function is a switch case structure for the Sensor commands (0x10)
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
uint8_t ITRP_Sensor(uint8_t cmd)
{
	int i,j;
	uint8_t error = 0;
	printf("entered sensor interpreter \r\n");

	/* Switch case for Sensor */
	switch(cmd)
	{
		case SET_ANGLE:		printf("Set angle of sensor \r\n");
							printf("Servo angle received: %d \r\n", cmd_buf[1]);
							ServoSetAngle(SENSOR_SERVO, cmd_buf[1]);
							ITRP_TransmitPacket(2, cmd, robot.error);
							break;

		case READ_SINGLE:;
							int32_t angle = (cmd_buf[1] << 24) | (cmd_buf[2] << 16) | (cmd_buf[3] << 8) | (cmd_buf[4]);
							angle = angle/1000;
							SensorModuleSetAngleRead(angle);
							ITRP_TransmitPacket(9, cmd,  (int16_t) angle,
							robot.sensor_module[0].Status,
							robot.sensor_module[0].Distance[angle],
							robot.sensor_module[0].RangeStatus[angle],
							robot.sensor_module[1].Status,
							robot.sensor_module[1].Distance[angle],
							robot.sensor_module[1].RangeStatus[angle],
							robot.error);
							break;

		case START_SENSOR:	/* enable sensor reading	*/
							/* Build packet				*/
							robot.sensor_read_flag = 1;
							ITRP_TransmitPacket(2, cmd, robot.error);
							break;

		case STOP_SENSOR:	/* disable sensor reading	*/
							/* Build packet				*/
							robot.sensor_read_flag = 0;
							ITRP_TransmitPacket(2, cmd, robot.error);
							break;

		case SENSOR_STATUS: /* Build packet			*/
							ITRP_TransmitPacket(4, cmd,
							robot.sensor_module[0].Status,
							robot.sensor_module[1].Status,
							robot.error);
							break;

		case READ_ALL:		robot.sensor_read_flag = 1;
							printf("Read All \n\r");
							SensorModuleSweep(MIN_ANGLE, MAX_ANGLE, 1);
							scan_time = get_runtime();
							filled_sensordata = 1;

							for(i=0, j=0; i<180; i+=15, j++)
							{
								ITRP_TransmitPacket(17,  0x21 + j,
								robot.sensor_module[0].Distance[i],    robot.sensor_module[0].Distance[i+1],  robot.sensor_module[0].Distance[i+2],  robot.sensor_module[0].Distance[i+3],
								robot.sensor_module[0].Distance[i+4],  robot.sensor_module[0].Distance[i+5],  robot.sensor_module[0].Distance[i+6],  robot.sensor_module[0].Distance[i+7],
								robot.sensor_module[0].Distance[i+8],  robot.sensor_module[0].Distance[i+9],  robot.sensor_module[0].Distance[i+10], robot.sensor_module[0].Distance[i+11],
								robot.sensor_module[0].Distance[i+12], robot.sensor_module[0].Distance[i+13], robot.sensor_module[0].Distance[i+14], robot.error);
							}

							for(i=0; i<180; i+=15, j++)
							{
								ITRP_TransmitPacket(17, 0x21 + j,
								robot.sensor_module[1].Distance[i],    robot.sensor_module[1].Distance[i+1],  robot.sensor_module[1].Distance[i+2],  robot.sensor_module[1].Distance[i+3],
								robot.sensor_module[1].Distance[i+4],  robot.sensor_module[1].Distance[i+5],  robot.sensor_module[1].Distance[i+6],  robot.sensor_module[1].Distance[i+7],
								robot.sensor_module[1].Distance[i+8],  robot.sensor_module[1].Distance[i+9],  robot.sensor_module[1].Distance[i+10], robot.sensor_module[1].Distance[i+11],
								robot.sensor_module[1].Distance[i+12], robot.sensor_module[1].Distance[i+13], robot.sensor_module[1].Distance[i+14], robot.error);
							}


							break;

		case RETRANSMIT:	printf("Retransmit \n\r");

							if (filled_sensordata == 0)
								break;

							for(i=0, j=0; i<180; i+=15, j++)
							{
								ITRP_TransmitPacket(17, 0x21 + j,
								robot.sensor_module[0].Distance[i],    robot.sensor_module[0].Distance[i+1],  robot.sensor_module[0].Distance[i+2],  robot.sensor_module[0].Distance[i+3],
								robot.sensor_module[0].Distance[i+4],  robot.sensor_module[0].Distance[i+5],  robot.sensor_module[0].Distance[i+6],  robot.sensor_module[0].Distance[i+7],
								robot.sensor_module[0].Distance[i+8],  robot.sensor_module[0].Distance[i+9],  robot.sensor_module[0].Distance[i+10], robot.sensor_module[0].Distance[i+11],
								robot.sensor_module[0].Distance[i+12], robot.sensor_module[0].Distance[i+13], robot.sensor_module[0].Distance[i+14], robot.error);
							}

					 		for(i=0; i<180; i+=15, j++)
							{
								ITRP_TransmitPacket(17, 0x21 + j,
								robot.sensor_module[1].Distance[i],    robot.sensor_module[1].Distance[i+1],  robot.sensor_module[1].Distance[i+2],  robot.sensor_module[1].Distance[i+3],
								robot.sensor_module[1].Distance[i+4],  robot.sensor_module[1].Distance[i+5],  robot.sensor_module[1].Distance[i+6],  robot.sensor_module[1].Distance[i+7],
								robot.sensor_module[1].Distance[i+8],  robot.sensor_module[1].Distance[i+9],  robot.sensor_module[1].Distance[i+10], robot.sensor_module[1].Distance[i+11],
								robot.sensor_module[1].Distance[i+12], robot.sensor_module[1].Distance[i+13], robot.sensor_module[1].Distance[i+14], robot.error);
							}



							break;

		default:			/* SET RETURN VALUE TO NOT OK */
							error = 1;
							break;
	}

	return error;
}

/*******************************************************************************************************
 * @brief This function is a switch case structure for the Position commands (0x40)
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
uint8_t ITRP_Position(uint8_t cmd)
{
	uint8_t error = 0;

	/* Go through each command and set position parameters */
	/* Also set duration functionality (decides how long the servos will rotate) */
	switch(cmd)
	{
		case GET_REL_POS:	ITRP_TransmitPacket(5, cmd, (int)(robot.pos[Y] * 1000), (int)(robot.pos[X] * 1000), (int)(robot.ori[Z] * 1000), robot.error);
							break;

		case SET_REL_POS:	robot.pos[Y] = (float)((cmd_buf[1] << 24) | (cmd_buf[2] << 16) | (cmd_buf[3] << 8) | (cmd_buf[4]))    / 1000;
						 	robot.pos[X] = (float)((cmd_buf[5] << 24) | (cmd_buf[6] << 16) | (cmd_buf[7] << 8) | (cmd_buf[8]))	  / 1000;
							robot.ori[Z] = (float)((cmd_buf[9] << 24) | (cmd_buf[10] << 16) | (cmd_buf[11] << 8) | (cmd_buf[12])) / 1000;

							robot.q[0] = sin(robot.ori[X]/2) * cos(robot.ori[Y]/2) * cos(robot.ori[Z]/2) - cos(robot.ori[X]/2) * sin(robot.ori[Y]/2) * sin(robot.ori[Z]/2);
							robot.q[1] = cos(robot.ori[X]/2) * sin(robot.ori[Y]/2) * cos(robot.ori[Z]/2) + sin(robot.ori[X]/2) * cos(robot.ori[Y]/2) * sin(robot.ori[Z]/2);
							robot.q[2] = cos(robot.ori[X]/2) * cos(robot.ori[Y]/2) * sin(robot.ori[Z]/2) - sin(robot.ori[X]/2) * sin(robot.ori[Y]/2) * cos(robot.ori[Z]/2);
							robot.q[3] = cos(robot.ori[X]/2) * cos(robot.ori[Y]/2) * cos(robot.ori[Z]/2) + sin(robot.ori[X]/2) * sin(robot.ori[Y]/2) * sin(robot.ori[Z]/2);
							IMU_EstimateOri();

							printf("x: %f y: %f ori: %f \r\n", robot.pos[Y], robot.pos[X], robot.ori[Z]);
							ITRP_TransmitPacket(2, cmd, error);
							break;

		case GET_SPEED:		ITRP_TransmitPacket(4, cmd, (int)(robot.vel[0] * 1000), (int)(robot.vel[1] * 1000), robot.error);
							break;

		case GET_ACCEL:		ITRP_TransmitPacket(5, cmd, (int)(robot.accel[0] * 1000), (int)(robot.accel[1] * 1000), (int)(robot.accel[2] * 1000), robot.error);
							break;

		case GET_GYRO:		ITRP_TransmitPacket(5, cmd, (int)(robot.gyro[0] * 1000), (int)(robot.gyro[1] * 1000), (int)(robot.gyro[2] * 1000), robot.error);
							break;

		case IMU_STATUS:	ITRP_TransmitPacket(3, cmd, robot.imu_status, error);
							break;

		default:			/* SET RETURN VALUE TO NOT OK */
							error = 1;
							break;
	}

	return error;
}

/*******************************************************************************************************
 * @brief This function is a switch case structure for the Miscellaneous commands (0x50)
 *
 * @param cmd 	: The incoming command from the HUB python code
 *******************************************************************************************************/
uint8_t ITRP_Miscellaneous(uint8_t cmd)
{

		uint8_t error = 0;
		/* Switch case for Miscellaneous*/
		switch(cmd)
		{
			case GET_FAILS:		/* NOT SUPPORTED YET */
								ITRP_TransmitPacket(1, cmd);
								break;

			default:			/* SET RETURN VALUE TO NOT OK */
								error = 1;

								break;
		}

	printf("miscellaneous \r\n");
	return error;
}

/*********************************************************************************************************
 * @brief This function will build a packet and when that is complete it will sent it with the NRF module
 *
 * @param args_num	 	: Size of arguments which need to create the right package for each functioncode
 * @param ...			: Unlimited arguments with datat that need to be transmit
 *********************************************************************************************************/
void ITRP_TransmitPacket(int args_num, ...)
{
	uint8_t cmd_ret[32] = {0};			/* Transmit buffer for commands			*/
	va_list arguments;					/* List of arguments					*/
	va_start(arguments, args_num);

	/* Build the data packet and sent it with NRF module*/
	ITRP_BuildPacket(cmd_ret, args_num, arguments);
	NRF24_StopListening();
	NRF_Transmit(cmd_ret);
	//HAL_Delay(2);
	NRF24_StartListening();
	va_end(arguments);
}

/***************************************************************************************************
 * @brief This function will start the wireless communication and it will read the incoming package
 *
 * @param unpacked_data 			: Pointer to array with the data that needs to be unpacked
 * @param cmd						: Function code which tells the code what to do
 *
 * @retval succesfull_transmission	: Return if all data has received correctly
 **************************************************************************************************/
int ITRP_ReceivePacket(int *unpacked_data, uint8_t cmd)
{
	uint8_t succesfull_transmission = 1;
	runtime start_time = get_runtime();
	runtime get_time;

	/* Start with listening on the NRF modules */
	NRF24_StartListening();
	while(1)
	{
		/* Check if there is data and if this data is right for getting sensordata */
		if(nrf24_cmd_flag == 1 && cmd_buf[0]==cmd)
		{
			NRF24_WriteAckPayload(1, ackpayload, 32);	/* Write Acknowledge to other device */
			ITRP_ReadPacket(unpacked_data, cmd);				/* Read the packet */
			nrf24_cmd_flag = 0;
			break;
		}
		/* Timeout code after 5 seconds */
		get_time = get_runtime();
		if(start_time.s + 5 < get_time.s)
		{
			succesfull_transmission = 0;
			break;
		}
	}
	HAL_Delay(1);
	return succesfull_transmission;
}

/************************************************************************************************************************
 * @brief This function will read the package that needs to be sent over the wireless communication
 *
 * @param cmd_ret			: Pointer to array, the data will be saved in this array untill it got sent
 * @param args_num			: Size of arguments which need to create the right package for each functioncode
 * @param arguments			: Unlimited list of arguments
 ************************************************************************************************************************/
void ITRP_ReadPacket(int *unpacked_data, uint8_t cmd)
{
	printf("Unpacking data...\r\n");
	uint8_t i,j;
	union int_to_bytes convert; /* Union for converting the int32_t type to bytes		*/
	uint8_t	param_size;			/* Stores paramater data size (1 byte, 2 bytes etc.)	*/
	uint8_t	cmd_type;			/* Stores CMD type	*/
	uint8_t index = 0;

	cmd_type = cmd >> 4;		/* Shift command number to get index for cmd parameter size array (most significant nibble)	*/

	/* Clear the unpacked_data */
	for(i=0; i<32; i++)
		unpacked_data[i] = 0;

//	printf("incomming data:\n");
//	for(i=0; i<32; i++)
//			printf("%x.", cmd_buf[i]);
//		printf("\r\n");

	/* Read all the 32 incoming bytes */
	for(i=0; i<32; i++)
		{
			param_size = cmd_param_size_rx[cmd_type][i];		/* Get amount of bytes used by the command parameter	*/
			/* Break when all the bytes were read */
			if(param_size == 0)
			{
				for(j=0; j<32; j++)
						printf("%d.", unpacked_data[j]);
					printf("\r\n");
				break;
			}
			/* Add to command return buffer	*/
			for(j=0; j<4; j++)
				convert.bytes[j] = 0;	/* LSB contains first byte of the int32_t */

			/* Make bytes to ints */
			for(j=param_size; j>0; j--)
			{
				convert.bytes[j-1] = cmd_buf[index];	/* LSB contains first byte of the int32_t */
				index++;
			}
			unpacked_data[i]= convert.int_var;
		}
}

/************************************************************************************************************************//**
 * @brief This function will create the package that needs to be sent over the wireless communication
 *
 * @param cmd_ret	: Pointer to array, the data will be saved in this array untill it got sent
 * @param args_num	: Size of arguments which need to create the right package for each functioncode
 * @param arguments	: Unlimited list of arguments
 ************************************************************************************************************************/
void ITRP_BuildPacket(uint8_t *cmd_ret, int args_num, va_list arguments)
{
	printf("Bulding package...\r\n");
	union int_to_bytes convert; /* Union for converting the int32_t type to bytes		*/
	int		param_size;			/* Stores paramater data size (1 byte, 2 bytes etc.)	*/
	int		cmd_type;			/* Stores CMD type										*/


	convert.int_var = va_arg(arguments, int32_t);	/* Read command number																		*/
	cmd_ret[0] = convert.int_var;					/* Add command number as first byte of the return command									*/
	cmd_type = convert.int_var >> 4;				/* Shift command number to get index for cmd parameter size array (most significant nibble)	*/

	/* Read argument, find amount of bytes and fill the command return buffer	*/
	int i, j;
	int index = 1;	/* start at index 1, because index 0 is already filled		*/
	for (i=1;i<32;i++)
		cmd_ret[i] = 0;

	for(i=1; i<args_num; i++)
	{
		convert.int_var  = va_arg(arguments, int32_t);	/* Get argument variable and save as int32_t			*/
		param_size = cmd_param_size_tx[cmd_type][i];		/* Get amount of bytes used by the command parameter	*/

		/* Add to command return buffer	*/
		for(j=param_size; j>0; j--)
		{
			cmd_ret[index] = convert.bytes[j-1];	/* LSB contains first byte of the int32_t */
			index++;
		}
	}

	va_end(arguments);

	for(i=0; i<32; i++)
		printf("%d.", cmd_ret[i]);
	printf("\r\n");
}
