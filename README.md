<img align="right" width="247" height="143" src="Images/logo.jpeg">
<br/><br/><br/><br/>

# **Swarm Robotics project HU**
This project is an attempt at creating a swarm system where multiple robots can map an unknown area.
The system currently consists of 3 robots and a HUB (Home Unit Base). Possibility to add more robots will be added in the future. The robots can be operated autonomously by using the mapping software and pathfinding algorithm. The robots can also be operated manually by using the the WASD keys on the keyboard (Required to use Goto_demo branch mapping software). A [user manual](User manual Swarm Robotics system.pdf) of the system can also be found in the repository.

## *Table of contents*
1.  [Mapping software](#map)
2.  [Mapping simulator](#sim)
3.  [Robot firmware](#robot)
4.  [HUB firmware](#hub)
5.  [Hardware](#hardware)

<a name="map"></a>
## **Mapping software**

The mapping software for the project was written entirely by hand and uses [occupancy grid mapping](https://www.youtube.com/playlist?list=PLgnQpQtFTOGQrZ4O5QzbIHgl3b1JHimN_) as a base for its mapping algorithm. [Particle filtering](https://www.youtube.com/playlist?list=PLgnQpQtFTOGQrZ4O5QzbIHgl3b1JHimN_) was added on top of the base to help with localization of the robot. This essentialy creates a type of SLAM (Simultaneous Localization And Mapping) algorithm. To help the robots figure out what to do, a pathfinding algorithm was also introduced. Several pathfinding algorithms were tested. Out of the test came that the [Dijkstra algorithm](https://github.com/AtsushiSakai/PythonRobotics#dijkstra-algorithm) proved most prominent.

Mapping software ran using particle filtering and mapping simulator with distance sensor and IMU (Accelerometer + Gyroscope) errors:
![](Images/mapping_software_example.gif)

Interaction between the system and the user is accomplished through a serial port. The user connects through a COM port to the HUB module. Through the software, commands can be sent to the robot manually or automatically. The mapping software translates the sensor data from the robots to a visible image representing the map. Each pixel in the image represents a set amount of distance in the real world. This "resolution" can be adjusted in the code to change the precision of the map. 

The [Doxygen documentation](Doxygen) can be consulted for further information about the code. Explanation of the mapping software can be found in the [design document]() of the project. This design document is written in Dutch though.

<a name="sim"></a>
## **Mapping simulator**
The mapping simulator is a program written in Unity to simulate the scanning and driving around of the robots. The simulator was used for testing the mapping software. The simulator also simulates the potential errors of the distance sensors and IMU (Accelerometer + Gyroscope) to recreate a more accurate representation of a real situation. This software was made open source but does not have any documentation as it was not initially meant for release to the public.

Image of the simulator below. The red dots represent the measured distances using the distance sensors. The blue dots represent the locations where the robot has scanned. It can be seen there are errors in the readings of the distance sensors. 
<div style="width: 977px; height: 621px;"><img src="Images/simulator.png" width="50%" height="50%"></div>

<a name="robot"></a>
## **Robot firmware**

The robot firmware can be found in the [swarm-workspace/robot-firmware folder](swarm-workspace/robot-firmware).
The peripheral configuration of the microcontroller was generated using STM32CubeMX. To see this configuration, the .ioc file can be opened.
Furthermore, the firmware uses several libraries. These libraries can be found in the [Drivers folder](swarm-workspace/robot-firmware/Drivers). This folder also includes the CMSIS and HAL drivers.
The libraries included in the robot firmware are:

*  ICM20948 IMU (Accelerometer + Gyroscope with sensor fusion library)
*  Rotary encoders
*  Motion tracking
*  Servos
*  VL53L1X (IR range sensor)
*  Command interpreter
*  nRF24L01 wireless communication

**Robot testing:** The different parts of the robot can be tested using the `RobotTest()` function. This function returns an error message through UART if there are any issues.

**Command interpreter:** The robot constantly checks to see if it has received any new commands from the HUB. It does this using the *EXTI0* interrupt. This interrupt is connected to the wireless nRF24L01 module. Commands are interpreted using the `ITRP_CmdNumber()` when a command is received. This function is ran constantly in the `while(1)` loop of the main function. The commands, used for communication, are based on the [command structure document](Swarm system command structure.xlsx)

**Going to a coordinate:** This is one of the most important abilities of the robot. The robot navigates to a coordinate using the `CalculateTravel()` and `RobotTravel()` functions. First, `CalculateTravel()` calculates the amount of the degrees the robot should rotate and the distance it should travel are calculated. The `RobotTravel()` function actually takes the robot to the desired XY-coordinate.

**Motion Tracking:** The tracking of position and orientation is done [using odometry](https://www.youtube.com/watch?v=XbXhA4k7Ur8) (rotary encoders) and IMU (Accelerometer & Gyroscope) sensor fusion. The [Madgwick algorithm](https://x-io.co.uk/open-source-imu-and-ahrs-algorithms/) was used to fuse the data of the IMU (Accelerometer and Gyroscope).

Robot driving around and avoiding obstacles:
<div align="left" style="width: 462px; height: 534px;"><img src="Images/robot_drive.gif" width="30%" height="30%"></div>

The [Doxygen documentation](Doxygen) can be consulted for further information about the code. Explanation of the robot firmware can be found in the [design document](Design Document Swarm Robotics V1.0.pdf) of the project. This design document is written in Dutch though. For the latest version of the firmware, the [binaries folder](Binaries) can be checked.

<a name="hub"></a>
## **HUB firmware**

The HUB firmware can be found in the [swarm-workspace/hub-firmware folder](swarm-workspace/hub-firmware).
The HUB module uses the same microcontroller and wireless module like the robot. The HUB firmware is therefore a stripped down version of the robot firmware. It only uses two libraries from the robot firmware. These libraries are:

*  Command interpreter
*  nRF24L01 wireless communication

The command interpreter for the HUB is modified to support the correct command structure (Robot commands tab in [command structure document](Swarm system command structure.xlsx)). The [Doxygen documentation](Doxygen) can be consulted for further information about the code. Explanation of the HUB firmware can be found in the [design document](Design Document Swarm Robotics V1.0.pdf) of the project. This design document is written in Dutch though. For the latest version of the firmware, the [binaries folder](Binaries) can be checked.

<a name="hardware"></a>
## **Hardware**

The physical frame and hardware for the robots was also designed by the Swarm robotics team HU. All the parts for the robot are 3D-printable excluding the tracks. The tracks were bought from an external supplier (can be found [here](https://www.banggood.com/4PcsPack-116-Caterpillar-Chain-Chassis-Tank-Car-Track-for-Arduino-T100-T400-DIY-Part-p-1366901.html?rmmds=detail-left-hotproducts__7&cur_warehouse=CN)). A custom PCB was also designed for the robots. The PCB acts as a motherboard and connects the different parts of the robot to each other through headers (see image below). The design files for the 3D-printable parts and PCB can be found on the gitlab page.

| **3D printed parts (in solidworks)** | **Robot PCB (3D model in KiCad)** |
| ------ | ------ |
| ![](Images/frame.png) | <div align="center" style="width: 464px; height: 501px;"><img src="Images/pcb_3d_design.jpeg" width="70%" height="70%"></div> |

The PCB has a 14-pin header to directly connect an ST-Link V3 mini without using any jumper wires. This can be used for programming and debugging through UART Serial communication. For the latest version of the firmware, the [binaries folder](Binaries) can be checked.

