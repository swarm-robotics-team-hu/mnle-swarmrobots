## @file   HUB_Software.py
# @author  Swarmrobots team
# @version V2.0.0
# @date	   9-Jan-2020
# @brief   This file provides al the logical functions for the HUB. 
#		   - Map the enviroment with sensor data from the robot.
#		   - Localise the robot in the enviroment based on Motion data and sensor data (including previous). 
#		   - Pathfinding to unknown area's

## @addtogroup HUB
#  @brief	All the software for the HUB
#  @{ 
  
## @addtogroup Python
#  @brief 	All the Python software
#  @{

## @addtogroup HUB_Software
#  @brief	All the Python software for the HUB
#  @{

# Import all necessary library's
import math
import csv
from PIL import Image
import numpy
import scipy.stats
import keyboard
import cv2
import serial
import time
import os
from pathfinding.core.diagonal_movement import DiagonalMovement
from pathfinding.core.grid import Grid
from pathfinding.finder.dijkstra import DijkstraFinder

## Define the standard size of the gridmap (-STANDARD_GRID_MAP_SIZE t/m STANDARD_GRID_MAP_SIZE on both axis)
STANDARD_GRID_MAP_SIZE = 25
## Define the default value of the gridmap
DEFAULT_VALUE_IN_GRID = 0

## @{ Global variables for tracking the grid map size
grid_map_x_size = STANDARD_GRID_MAP_SIZE*2+1
grid_map_y_size = STANDARD_GRID_MAP_SIZE*2+1
## @}

## @{ Global variables for tracking the center of the grid map
x_offset = STANDARD_GRID_MAP_SIZE
y_offset = STANDARD_GRID_MAP_SIZE
## @}

## Size of grid cells in mm
GRID_CELL_SIZE = 30

## Grid map array starting with default values and size
grid_map_array = [[DEFAULT_VALUE_IN_GRID for y in range(grid_map_y_size)] for x in range(grid_map_x_size)]

## Value added when sensor data determines that grid cell is occupied
NOT_FREE = 25
## Value added when sensor data determines that grid cell is free
IS_FREE = -20
## Max value (and min value *-1) of chance in grid cells
LIMITER_GRID = 100

## Value to determine in pathfinding if grid cell counts as a wall
PATHFINDING_WALL_VALUE = 30
## Value to determine in pathfinding if grid cell is unknown (-value up to value)
PATHFINDING_UNKNOWN_VALUE = 30
## Value to determine wall thickness (not counting robot thickness) for pathfinding
PATHFINDING_WALL_THICKNESS = 150

## @{ Commands for wireless comunication
COMMAND_DRIVE_FORWARD 	  = 0x01
COMMAND_DRIVE_BACKWARDS   = 0x02
COMMAND_DRIVE_ROT_LEFT 	  = 0x04
COMMAND_DRIVE_ROT_RIGHT   = 0x05
COMMAND_DRIVE_GOTO_COORD  = 0x07
COMMAND_DRIVE_GOTO_DONE   = 0x0A

COMMAND_SENSOR_READ_ALL   = 0x16

COMMAND_POSITION_GET_POSE = 0x41
COMMAND_POSITION_SET_POSE = 0x42
## @}

##
	# This function can be used to add a value to a cell in the gridArray
	#
	# @param 	x,y: 	The given position
	# @param 	data:	The value which need to be changed in the gridcell
def ChangeGridArray(x,y,data):
	global grid_map_x_size, grid_map_y_size, x_offset, y_offset

	# If x and y are not inside array
	if not ( (-x_offset <= x < grid_map_x_size-x_offset) and (-y_offset <= y < grid_map_y_size-y_offset) ):

		# Check if outsize array (left)
		if (x < -x_offset):
			for i in range(-x_offset - x):
				grid_map_array.insert(0, [DEFAULT_VALUE_IN_GRID for j in range(len(grid_map_array[0]))])
			grid_map_x_size += (-x_offset - x)
			x_offset += (-x_offset - x)

		# Check if outsize array (right)
		elif (grid_map_x_size-x_offset-1 < x):
			for i in range(x - (grid_map_x_size-x_offset-1)):
				grid_map_array.append([DEFAULT_VALUE_IN_GRID for j in range(len(grid_map_array[0]))])
			grid_map_x_size += (x - (grid_map_x_size-x_offset-1))

		# Check if outsize array (down)
		if (y < -y_offset):
			for i in range(-y_offset - y):
				for row_counter in range(len(grid_map_array)):
					grid_map_array[row_counter].insert(0, DEFAULT_VALUE_IN_GRID)
			grid_map_y_size += (-y_offset - y)
			y_offset += (-y_offset - y)

		# Check if outsize array (up)
		elif (grid_map_y_size-y_offset-1 < y):
			for i in range(y - (grid_map_y_size-y_offset-1)):
				for row_counter in range(len(grid_map_array)):
					grid_map_array[row_counter].append(DEFAULT_VALUE_IN_GRID)
			grid_map_y_size += (y - (grid_map_y_size-y_offset-1))

	# Array resized
	changed_data = grid_map_array[x+x_offset][y+y_offset] + data

	if (changed_data<-LIMITER_GRID):
		grid_map_array[x + x_offset][y + y_offset] = -LIMITER_GRID

	elif (changed_data>LIMITER_GRID):
		grid_map_array[x + x_offset][y + y_offset] = LIMITER_GRID

	else:
		grid_map_array[x + x_offset][y + y_offset] = changed_data


##
	# This function calculates the obstacle point based on the sensor position,
	# the rotation of the sensor and the distance.
	#
	# @param	position:	The position of the sensor that made the measurement
	# @param	rotation:	The rotation of the sensor in the world (0° = N, 90° = E)
	# @param	distance:	The measured distance from the sensor
	#
	# @retval	(x,y):		The coordinate of the measured obstacle as integers
def CalculateObstPoint(position, rotation, distance):
	# Split coordinate
	x1, y1 = position

	# Calculate point with trigonometry
	x2 = x1 + (math.sin(rotation/180*math.pi)*distance)
	y2 = y1 + (math.cos(rotation/180*math.pi)*distance)

	return int(x2), int(y2)


##
	# This function will convert coordinates to corresponding gricell
	#
	# @param	position:	The coordinate that need to be converted
	#
	# @retval	(x,y):		The corresponding gridcell	
def CoordToGridCell(position):
	x, y = position
	return ( round(x/GRID_CELL_SIZE), round(y/GRID_CELL_SIZE) )


##
	# This function calculates all the grid cells that need to be changed
	# based on a line between two coordinates. The line is calculated with
	# Bresenham's Line Algorithm.
	#
	# @param	point_1:	The first coordinate of the line (normaly robot position)
	# @param	point_2:	The second coordinate of the line (normaly obstacle position)
	#
	# @retval	points:		A list with all the grid cells that are part of the line.
	#						There will be duplicates of the same grid cell.
def CalculateGridLine(point_1, point_2, input_is_mm = True):
	# Setup initial conditions
	if (input_is_mm):
		x1, y1 = CoordToGridCell(point_1)
		x2, y2 = CoordToGridCell(point_2)
	else:
		x1, y1 = point_1
		x2, y2 = point_2
	dx = x2 - x1
	dy = y2 - y1
 
	# Determine how steep the line is
	is_steep = abs(dy) > abs(dx)

	# Rotate line
	if is_steep:
		x1, y1 = y1, x1
		x2, y2 = y2, x2
 
	# Swap start and end points if necessary and store swap state
	swapped = False
	if x1 > x2:
		x1, x2 = x2, x1
		y1, y2 = y2, y1
		swapped = True
 
	# Recalculate differentials
	dx = x2 - x1
	dy = y2 - y1
 
	# Calculate error
	error = int(dx / 2.0)
	y_step = 1 if y1 < y2 else -1
 
	# Iterate over bounding box generating points between start and end
	y = y1
	points = []
	for x in range(x1, x2 + 1):
		coord = (y, x) if is_steep else (x, y)
		# Before appending the point, convert the coordinate to the corresponding grid cell
		points.append(coord)
		error -= abs(dy)
		if error < 0:
			y += y_step
			error += dx
 
	# Reverse the list if the coordinates were swapped
	if swapped:
		points.reverse()
	return points
	

##
	# This function calculates all the grid cells that need to be changed
	# based on a line between two coordinates. The line is calculated with
	# Bresenham's Line Algorithm.
	#
	# @param	point_1:	The first coordinate of the line (normaly robot position)
	# @param	point_2:	The second coordinate of the line (normaly obstacle position)
	# @param	obstacle:	This variable tells the function if there is a obstacle
	#
	# @retval	points:		A list with all the grid cells that are part of the line.
	#						There will be duplicates of the same grid cell.
def CalculateGridLineObstacle(point_1, point_2, obstacle):
	# Setup initial conditions
	x1, y1 = CoordToGridCell(point_1)
	x2, y2 = CoordToGridCell(point_2)
	dx = x2 - x1
	dy = y2 - y1
 
	# Determine how steep the line is
	is_steep = abs(dy) > abs(dx)

	# Rotate line
	if is_steep:
		x1, y1 = y1, x1
		x2, y2 = y2, x2
 
	# Swap start and end points if necessary and store swap state
	swapped = False
	if x1 > x2:
		x1, x2 = x2, x1
		y1, y2 = y2, y1
		swapped = True
 
	# Recalculate differentials
	dx = x2 - x1
	dy = y2 - y1
 
	# Calculate error
	error = int(dx / 2.0)
	y_step = 1 if y1 < y2 else -1
 
	# Iterate over bounding box generating points between start and end
	y = y1
	points = []
	for x in range(x1, x2 + 1):
		obst = -1
		if (x==x2 and y==y2):
			obst = obstacle
		coord = (y, x, obst) if is_steep else (x, y, obst)
		# Before appending the point, convert the coordinate to the corresponding grid cell		points.append(coord)
		points.append(coord)
		error -= abs(dy)
		if error < 0:
			y += y_step
			error += dx
 
	# Reverse the list if the coordinates were swapped
	# if swapped:
		# points.reverse()
	
	return points


##
	# This function calculates cell occupancies between the pose of the
	# the robot and the measured obstacle points calculated from 
	# the sensor data.
	# 
	#
	# @param	sensor_data: 	The sensordata containing 360 distances
	#							Data is expressed in milimeters 
	# @param	pose:	 		The pose used as reference
	#							Pose format [x, y, rotation]	
	#
	# @retval	points:			A list of points that are occupied or not	
	#							x and y are expressed in cells
	#							Occupancy is either 1 or -1
def CalculatePoseDistances(pose, sensor_data):
	points = []
	obstacle = 1
	reduced_dist = 0.9
	# total_length = 0
	position = (pose[0], pose[1])
	# print("position = ", CoordToGridCell(position))
	for i in range(len(sensor_data)):
		# skip angle if sensor_data = -1
		if (sensor_data[i] == -1):
			continue

		minimal_range = 50

		# if the sensor measured no wall (noted with negative value)
		if (sensor_data[i] < 0):
			sensor_data[i] = sensor_data[i] * -1
			obstacle = -1
		if (sensor_data[i] < minimal_range):
			start_line_point = position
		else:
			start_line_point = CalculateObstPoint(position, pose[2] + i, sensor_data[i]-minimal_range)


		# determine the points subject to change between the robot position and sensor_data
		sensor_point = CalculateObstPoint(position, pose[2]+i, sensor_data[i])
		#line_points =
		#length = len(line_points)
		points.extend(CalculateGridLineObstacle(start_line_point, sensor_point, obstacle))
		#sensor_point = CoordToGridCell(sensor_point)
		#points.append((sensor_point[0],sensor_point[1],obstacle))
		# print("i = ",i,"|| sensor_data = ",sensor_data[i],"|| rotation = ", pose[2]+i, "|| end point = ", CoordToGridCell(sensor_point))
		# print(line_points)
		# print("i = ",i,"|| sensor point = ",CoordToGridCell(sensor_point))
		
		# total_length +=length
		# # add occupancy value to the list of line_points and add those to the points array
		# for j in range(length):
			# x,y = line_points[j]
			# obst = -1
			# if (j == length-1):
				# obst = obstacle
			# point = (y, x, obst)
			# points.append(point)				
			
		# change the value back to the default for the next measurement
		if (obstacle==-1):
			sensor_data[i] = sensor_data[i] * -1
			obstacle = 1 	
	
	# print("total_length = ",total_length)
	return points
	

##
	# This function changes the probabilty of the grid map cells according
	# to the given grid cells, the coordinate of the obstacle and if there
	# even is a obstacle to map.
	#
	# @param	obst_position:	The coordinate of the obstacle (if there is one)
	# @param	grid_cells: 	A list of grid cells that overlap with the measurement line
	# @param	obstacle:		A bool that determins if there was a obstacle (default is True)
def CalculateOccupancy(obst_position, grid_cells, obstacle = True):
	# Check if there is a obstacle in the measurement. If there is one, change the probability at the corresponding cell
	if (obstacle):
		obst_grid_cell = CoordToGridCell(obst_position)
		ChangeGridArray(obst_grid_cell[0], obst_grid_cell[1], NOT_FREE)
	else:
		obst_grid_cell = (None, None)

	# Go through all grid cells in the list that are not the cell of the obstacle
	for cell_nr in range(len(grid_cells)):
		if not (grid_cells[cell_nr] == obst_grid_cell):
			ChangeGridArray(grid_cells[cell_nr][0], grid_cells[cell_nr][1], IS_FREE)


##
	# This function start the PIL library and displays the grid in an grayscale image
	#
	# @param	grid:		Array to make a image of
	# @param	number:		Optional variable to add a number to the saved image (default = None)
	# @param	gray_scale:	Optional variable to set image color to grayscale spectrum or Black/Gray/White (default = True)
def ShowGridMapImagePIL(grid, number = None, gray_scale = True):
	# Set up image
	DEFAULT_IMAGE_COLOR = 127

	img = Image.new("L", (len(grid), len(grid[0])), DEFAULT_IMAGE_COLOR)
	#img.show()
	pixels = img.load()

	if (gray_scale):
		for i in range(img.size[0]):
			for j in range(img.size[1]):
				# Select color
				chance_value = grid[i][j]
				if (chance_value <= LIMITER_GRID * -1):
					chance_value = 255
				elif (chance_value >= LIMITER_GRID):
					chance_value = 0
				else:
					chance_value = (((chance_value*-1)+LIMITER_GRID) / (LIMITER_GRID*2) ) * 255
				pixels[i, j] = int(chance_value)
	else:
		for i in range(img.size[0]):
			for j in range(img.size[1]):
				# Select color
				chance_value = grid[i][j]
				if (chance_value < 0):
					pixels[i, j] = 255
				elif (chance_value > 0):
					pixels[i, j] = 0
				else:
					pixels[i, j] = 127

	# Flip the image
	img = img.transpose(Image.FLIP_TOP_BOTTOM)
	#img = img.rotate(180)
	#basewidth = 1000

	#percent = (basewidth / float(img.size[0]))
	#hsize = int((float(img.size[1]) * float(percent)))
	#img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
	#img.save("resized_image.jpg")

	# Check if it needs to be saved
	if (number == None):
		#img.show()
		img.save("output_image.png")
	else:
		img.save("images/image_" + str(number) + ".png")


##
	# This function start the PIL library and displays the pathfinding grid in an color image
	#
	# @param	grid:		Array to make a image of
def ShowPathfindingPIL(grid):
	# Set up image
	DEFAULT_IMAGE_COLOR = 127

	img = Image.new("RGB", (len(grid), len(grid[0])), (DEFAULT_IMAGE_COLOR,DEFAULT_IMAGE_COLOR,DEFAULT_IMAGE_COLOR))
	#img.show()
	pixels = img.load()

	for i in range(img.size[0]):
		for j in range(img.size[1]):
			# Select color
			cell_value = grid[i][j]

			if (cell_value == 0): #Occupied pixel
				pixels[i, j] = (0,0,0) #Make black color

			elif (cell_value == 1): #Free/Unkown pixel
				pixels[i, j] = (255,255,255) #Make white

			elif (cell_value == 2): #RED
				pixels[i, j] = (255,0,0)

			elif (cell_value == 3): #GREEN
				pixels[i, j] = (0,255,0)

			elif (cell_value == 4): #BLUE
				pixels[i, j] = (0,0,255)


	# Flip the image
	img = img.transpose(Image.FLIP_TOP_BOTTOM)

	# Save image
	img.save("pathfinding_image.png")
	#img.show()


##
	# This function start the PIL library and saves the image and then displays it with openCV
	#
def ShowGridMapOpenCV(grid):
	ShowGridMapImagePIL(grid)
	
	img1 = cv2.imread("output_image.png",0)
	cv2.imshow('output image', img1)
	try:
		img2 = cv2.imread("pathfinding_image.png",0)	
		cv2.imshow('pathfinding image', img2)
	except:
		print("Pathfinding is not possible to show")
	cv2.waitKey(1000)


##
	# Gets data from external hardware via USB
	#
	# @param	sercom:		The port of the USB connection
	# @param	command:	Expected command to recieve data from
	# @param	debug:		Variable to show debug output in terminal (default = False)
	#
	# @retval	sensor_data_list:	A list of the expected data (Sensor data OR Pose data)
def GetDataFromUSB(sercom, connected_robot, command, debug = False):

	print("Waiting for data")
	counter = 0
	while(True):
		# Read data (and print to terminal)
		data_string = str(sercom.readline())
		if(debug):
			print(data_string) 
			print(data_string[2:-5])

		# Split the recieved data with ',' and remove [b'] and [\r\n'] from string (and print list)
		sensor_data_list = data_string[2:-5].split(',')
		if (debug):
			print(sensor_data_list)

		# Check if we recieved data
		if (sensor_data_list[0] != None and len(sensor_data_list) > 1):
			# Check if recieved data is the right expected command type
			if (command != int(sensor_data_list[0])):
				print("Wrong expected command recieved! (" + sensor_data_list[0] + ")")
				#return None
				continue

			# Check command types
			if (command == COMMAND_SENSOR_READ_ALL): 
				# Convert all data to ints (1st element is command)
				for i in range(0,360+1):
					sensor_data_list[i] = int(sensor_data_list[i])

			elif (command == COMMAND_POSITION_GET_POSE):
				sensor_data_list[0] = int(sensor_data_list[0]) 			# X position
				sensor_data_list[1] = int(sensor_data_list[1]) 			# X position
				sensor_data_list[2] = int(sensor_data_list[2]) 			# Y position
				sensor_data_list[3] = float(sensor_data_list[3])/1000 	# Rotation
			
			elif (command == COMMAND_DRIVE_GOTO_DONE):
				return True
				
			else:
				print("Command not found")
				return None
			print("returned sensor_data_list")
			#print(sensor_data_list)
			return sensor_data_list

		if keyboard.is_pressed('r'):
			print("Stop waiting for data")
			return None
	
		counter += 1
		if (counter > 10 and command != COMMAND_DRIVE_GOTO_DONE):
			break
		
		if (command == COMMAND_SENSOR_READ_ALL or command == COMMAND_POSITION_GET_POSE): 
			print("None recieved, trying again... (hold 'R' to quit)")
			WriteCommand(sercom, connected_robot, command)


##
	# Map the sensor data in the grid map
	#
	# @param	robot_identifier:	The robot name
	# @param	robot_position:		Tuple of x and y coord
	# @param	robot_rotation:		Rotation of the robot
	# @param	sensor_data:		List of sensor data
def OccupancyGridMapping(robot_identifier, robot_position, robot_rotation, sensor_data):
	obstacle = True
	# Go through all measurements
	for i in range(len(sensor_data)):
		# If the sensor measured no wall (noted with negative value), convert value and change the 'is_obstacle' value so the software won't calulate a wall.
		if (sensor_data[i] < 0):
			sensor_data[i] = sensor_data[i] * -1
			obstacle = False

		sensor_rotation = robot_rotation + float(i)
		if sensor_rotation > 360:
			sensor_rotation -= 360

		# Change the grid map based on the measurement
		obstacle_point = CalculateObstPoint(robot_position, sensor_rotation, sensor_data[i])
		CalculateOccupancy(obstacle_point, CalculateGridLine(robot_position, obstacle_point), obstacle)

		# Change the value back to the default for the next measurement
		if not (obstacle):
			obstacle = True


##
	# Generate samples based on IMU pose
	#
	# @param	IMU_pose:				Pose from IMU
	# @param	generated_size:			Amount of samples that needs to be created each input sample
	# @param	best_previous_poses:	The best previous poses from IMU, default is a 2D array with [0,0,0,1]
	#
	# @retval	generated_samples:		2D list with the coordinates, rotations and weight
def GenerateSamples(IMU_pose, generated_size, best_previous_poses):

	best_previous_poses_length = len(best_previous_poses)

	# Create output list
	generated_samples = []

	for count in range(best_previous_poses_length):
		# Calaculate the new place of the robot and its new rotation
		difference_x = IMU_pose[0] - best_previous_poses[count][0]
		difference_y = IMU_pose[1] - best_previous_poses[count][1]

		# Calculate the length the robot has driven
		driven_length = math.sqrt(difference_x**2 + difference_y**2)

		# Create normal devation for weigth calculation
		normal_devation_length		=	scipy.stats.norm(driven_length, driven_length*0.1)
		normal_devation_rotation	=	scipy.stats.norm(IMU_pose[2], 5)
		list_of_generated_lengths 	= 	numpy.random.normal(driven_length, driven_length*0.1, generated_size)
		list_of_generated_rotations =	numpy.random.normal(IMU_pose[2], 5, generated_size)

		# Calculate point with trigonometry
		for i in range(generated_size):
			x_pos = best_previous_poses[count][0] + (math.sin(list_of_generated_rotations[i]/180*math.pi)*list_of_generated_lengths[i])
			y_pos = best_previous_poses[count][1] + (math.cos(list_of_generated_rotations[i]/180*math.pi)*list_of_generated_lengths[i])
			
			# Calculate chances 
			chance_rotation = normal_devation_rotation.cdf((list_of_generated_rotations[i]))*2
			chance_length 	= normal_devation_length.cdf((list_of_generated_lengths[i]))*2

			if (chance_rotation > 1):
				chance_rotation = (2 - chance_rotation)
			if (chance_length > 1):
				chance_length	= (2 - chance_length)
			weight = chance_length * chance_rotation * best_previous_poses[count][3]

			# Store generated samples
			generated_samples.append([int(x_pos), int(y_pos), list_of_generated_rotations[i], weight])

	return generated_samples
	
	
##
	# Calculate correlation chance from points
	#
	# @param	point_list:				List of poses and reading(free/occupied) (x,y,reading)
	#
	# @retval	cummulative_chance:		Chance of correlation (0.0 - 1.0)
def PartFilterCalulateChance(point_list):
	cummulative_chance = 0
	chance = 0
	error_counter = 0
	# Get length of point list
	point_list_length = len(point_list)

	# If point is outside grid map, it raises an error and increments error_counter
	for i in range(point_list_length):
		try:
			# Add chance to cummulative variable
			chance = (grid_map_array[point_list[i][0]][point_list[i][1]] * point_list[i][2])
			cummulative_chance += chance
			# if (first == 1):
				# print("point", i, "=", point_list[i], "chance", i,"=",chance) 
		except:
			error_counter+=1

	# Calculate total chance from cummulative chance and return value
	return ( cummulative_chance / (LIMITER_GRID * point_list_length) )


##
	# Update samples, redistribute weights and return samples with best chances
	#
	# @param	pose_list:				List of poses with chances (x,y,rot,weight,correlation_chance)
	# @param	return_amount:			Amount of poses to return with weight for next irreration
	#
	# @retval	pose_list:				List of poses with weight (sorted with highest weight at index 0)
	#															  (x,y,rot,weight)
def UpdateSamples(pose_list,return_amount):
	# Calculate total chance with weight and correlation chance into the new weight
	
	for i in range(len(pose_list)):
		#print(pose_list[i])
		pose_list[i][3] *= pose_list[i][4]

	
	# Sort poses based on weight (4th element, highest first)
	pose_list.sort(key=lambda x: x[3], reverse=True)
	#print(pose_list)
	# Bases on the highest weight, calculate the multiplier for all samples to redistribute weight
	weight_multiplier = 1 / max([i[3] for i in pose_list[0:return_amount]])	

	# Only multiply the weight of relevant samples (samples getting returned), and delete correlation chance
	for i in range(return_amount):
		pose_list[i][3] *= weight_multiplier
		del pose_list[i][-1]

	# Return best n-amount of poses
	return pose_list[0:return_amount]


##
	# This function starts the serial port with the external device
	#
	# @param	port:			Port name of the serial connection
	# @param	arg_baudrate:	Baudrate of the connection (default: 115200)
	# @param	arg_timeout:	Timeout of the connection in seconds (default: 10s)
	#
	# @retval	sercom:			Serial port variable to be used by other functions
def BeginSerial(port, arg_baudrate = 115200, arg_timeout = 10):
	sercom = serial.Serial(port,baudrate = arg_baudrate, timeout = arg_timeout)
	return sercom


##
	# This funcion returns the keys W, S, A, D, F and Q as strings when pressed
	#
	# @retval	pressed_key:	The key pressed as a string
def ReadKeyboard():
	pressed_key = None
	if keyboard.is_pressed('w'):
		pressed_key = "W"
	elif keyboard.is_pressed('s'):
		pressed_key = "S"
	elif keyboard.is_pressed('a'):
		pressed_key = "A"
	elif keyboard.is_pressed('d'):
		pressed_key = "D"
	elif keyboard.is_pressed('f'):
		pressed_key = "F"
	elif keyboard.is_pressed('q'):
		pressed_key = "Q"
	elif keyboard.is_pressed('1'):
		pressed_key = "1"
	elif keyboard.is_pressed('2'):
		pressed_key = "2"
	elif keyboard.is_pressed('p'):
		pressed_key = "P"
	if (pressed_key != None):
		print("Pressed: " + pressed_key)
	time.sleep(0.1)
	return pressed_key


##
	# This function writes information to the external device to be wireless send 
	#
	# @param	serial_connection:	Serial port of the serial connection
	# @param	command:			Command type (int)
	# @param	*arguments:			Arguments for the command (can be ~infinite)
def WriteCommand(serial_connection, robot_address, command, *arguments):
	transmit_string = str(robot_address) + "," + str(command)
	for arg in arguments:
		transmit_string = transmit_string + "," + str(arg)

	print("Transmitting ("+hex(command)+"): \"" + transmit_string + "\"")
	serial_connection.write(bytes( transmit_string+"\r\n", encoding='utf8' ) )


##
	# This converts a grid cell to the corresponding mm
	#
	# @param	input_cell:		Grid cell to convert
	# @param	input_axis:		Axis of grid cell
	#
	# @retval	output_mm:		The corresponding mm of the grid cell in the chosen axis
def ArrayCellToMm(input_cell, input_axis):
	if (input_axis == 'x'):
		return (input_cell-x_offset)*GRID_CELL_SIZE
	elif (input_axis == 'y'):
		return (input_cell-y_offset)*GRID_CELL_SIZE
	else:
		print("Axis in function 'ArrayCellToMm()' was not 'x' or 'y'. The value 0 was returned.")
		return 0


##
	# This function generates a list of points of a circle around the given point and radius
	# @note 	This function is based on the Bresenham's Algorithm
	#
	# @param	xm:				X value of the point in the middle of the circle
	# @param	ym:				Y value of the point in the middle of the circle
	# @param	radius:			Radius of the circle (in grid cells)
	#
	# @retval	list_of_cells:	List of cells of the generated circle
def CreateCircle(xm, ym, radius, bypass = False):
	
	# Set mm to grids
	if not (bypass):
		xm, ym = CoordToGridCell((xm, ym))
		xm+= x_offset
		ym+= y_offset

	# Create other variables
	list_of_cells = []
	x = -radius
	y = 0
	err = 2-2*radius
	
	# Create cirkel with four quadrants
	while 1:
		list_of_cells.append([xm-x, ym+y])	 # I. Quadrant 
		list_of_cells.append([xm-y, ym-x])	 # II. Quadrant
		list_of_cells.append([xm+x, ym-y])	 # III. Quadrant
		list_of_cells.append([xm+y, ym+x])	 # IV. Quadrant
		radius = err

		if (radius <= y): 
			y+=1
			err += y*2+1		
		if (radius > x or err > y):
			x+=1
			err += x*2+1	
		if not (x < 0):
			break		
 
	return list_of_cells


##
	# This function seaches for a 'unknown' point in the grid map around the given point and plans a route to it
	#
	# @param	x_coord:		X value of the point (robot position)
	# @param	y_coord:		Y value of the point (robot position)
	#
	# @retval	path_point:		First point (waypoint) of the route to the unknown area
def SearchUnkownPoints(x_coord, y_coord):
	
	radius = 0
	unknown_point = [0.0]
	loop_flag = True
	counter = 0
	print("Coords: " + str((x_coord,y_coord)))
	
	# Repeat until a route has been made or there are no unknown areas left
	while  loop_flag:  
		# Generate points to check and increase radius
		list_with_circle_points = CreateCircle(x_coord, y_coord, radius)
		#print(radius)
		radius+=1

		# Go through all points in list
		for i in range(len(list_with_circle_points)):	
			try: # Error handeling (if point is outside grid map array)
				x_point, y_point = list_with_circle_points[i]
				# Check if point is unknown
				if (grid_map_array[x_point][y_point] >= -PATHFINDING_UNKNOWN_VALUE and grid_map_array[x_point][y_point] <= PATHFINDING_UNKNOWN_VALUE):
					# Check if point is inside array (Can be accesing other registers, this doesn't give a error)
					if (x_point >= 0 and y_point >= 0):
						# Check if point is inside array (Can be accesing other registers, this doesn't give a error)
						if (x_point < grid_map_x_size and y_point < grid_map_y_size):

							# Get points in mm and ty to find a route to point
							x_point_mm = ArrayCellToMm(x_point, 'x')
							y_point_mm = ArrayCellToMm(y_point, 'y')
							path_point, path_error = Pathfinding( (x_coord, y_coord), (x_point_mm, y_point_mm) )
							
							# If route is found no (None, None), then return the first point (waypoint) of the route
							if (path_point != (None, None) ):
								print("Correct Path point: " + str(path_point))
								return path_point
							elif (path_error):
								print("Robot to close to obstacle")





								return path_point
			# Check when error was given if the radius is greater than total map
			except:
				if radius > grid_map_x_size*2 and radius > grid_map_y_size*2:
					print("No paths found in map!")
					return None
			counter += 1
	

##
	# This function calculates a waypoint to go to the given position and returns it if posible
	#
	# @param	start_position:		Tuple (x,y) of the start position (normaly robot position)
	# @param	goal_position:		Tuple (x,y) of the goal position (normaly unexplored area)
	#
	# @retval	waypoint:				Tuple (x,y) of the first waypoint towards the end goal
def Pathfinding(start_position, goal_position):
	# Convert positions in mm to according grid cells in grid map array
	start_x_position, start_y_position = CoordToGridCell(start_position)
	goal_x_position,  goal_y_position  = CoordToGridCell(goal_position)
	start_x_position += x_offset
	start_y_position += y_offset
	goal_x_position  += x_offset
	goal_y_position  += y_offset

	# Initialize pathfinding grid map to use for pathfinding
	pathfinding_grid_map = [[1 for y in range(grid_map_y_size)] for x in range(grid_map_x_size)]

	# Initialize kerel for making the obstacles thicker (21x21 wide (100mm from wall))
	one_side = round(PATHFINDING_WALL_THICKNESS/GRID_CELL_SIZE)
	size = int(one_side*2+1)

	# Make waypoint default value
	final_point = (None,None)

	# Make kernel we will use with defined size
	kernel = [[0 for y in range(size)] for x in range(size)]
	# Make counter so we can use the try: except: functionality
	error_counter = 0

	# Make pathfinding map with original grid map and kernel
	for y in range(grid_map_y_size):
		for x in range(grid_map_x_size):
			if(grid_map_array[x][y] > PATHFINDING_WALL_VALUE):
				for y_kernel in range(size):
					for x_kernel in range(size):
						# Try to change pathfinding array, if index is out of limit the counter is added and no error occurs
						try:
							pathfinding_grid_map[x+x_kernel-one_side][y+y_kernel-one_side] *= kernel[y_kernel][x_kernel]
						except:
							error_counter += 1

	# Exit function if start- or goal point is inside a wall
	if (pathfinding_grid_map[start_x_position][start_y_position] == 0):
		pathfinding_grid_map[start_x_position][start_y_position] = 3 	#Green = Start posistion
		pathfinding_grid_map[goal_x_position][goal_y_position] = 4 		#Blue = Goal posistion
		ShowPathfindingPIL(pathfinding_grid_map)

		#print("zit erin hoor!")
		radius = 1
		while(True):
			#print("YO")
			list_with_circle_points = CreateCircle(start_x_position, start_y_position, radius, True)
			#print(list_with_circle_points)
			#print("Radius van nieuw: "+ str(radius))
			for i in range(len(list_with_circle_points)):
				#print(pathfinding_grid_map[list_with_circle_points[i][0]][list_with_circle_points[i][1]])
				if (pathfinding_grid_map[list_with_circle_points[i][0]][list_with_circle_points[i][1]] == 1):
					final_point = ( list_with_circle_points[i][0], list_with_circle_points[i][1] )
					print("Calulated path towards free space!")
					return ( (final_point[0]-x_offset)*GRID_CELL_SIZE, (final_point[1]-y_offset)*GRID_CELL_SIZE ), False
			radius = radius + 1
										



		#return final_point, True
	if (pathfinding_grid_map[goal_x_position][goal_y_position] == 0):
		return final_point, False
	

	# Define the grid to use
	grid = Grid(matrix=pathfinding_grid_map)

	# Load start and goal
	# X & Y Switched because we use [X][Y] notation for the grid map but pathfinding uses [Y][X] so visualization is not rotated
	try:
		start = grid.node(start_y_position, start_x_position) 
		end = grid.node(goal_y_position, goal_x_position)
	except:
		print("Error with input of start/end position")
		return final_point,True

	#Specify pathfinding algorithm and run it
	finder = DijkstraFinder(diagonal_movement=DiagonalMovement.always)
	path, runs = finder.find_path(start, end, grid)

	# Initialise break variable
	breaker = 0
	# Go through all points in the generated path
	for i in range(len(path)):
		# Generate line from start position to point in path
		line = CalculateGridLine( (start_x_position, start_y_position), (path[i][1], path[i][0]), input_is_mm=False )
		# Check line and if there are no obstacles in line (if so, we can skip previous points in path)
		for j in range(len(line)):
			if (pathfinding_grid_map[line[j][0]][line[j][1]] == 0):
				breaker = 1
				break
		if (breaker == 1):
			break
		final_point = (path[i][1], path[i][0])

	# Change values of the pathfinding grid map so it can be visualised
	for i in range(len(path)):
		pathfinding_grid_map[path[i][1]][path[i][0]] = 2 			#Green = Path (X & Y Switched)
	pathfinding_grid_map[start_x_position][start_y_position] = 3 	#Green = Start posistion
	pathfinding_grid_map[goal_x_position][goal_y_position] = 4 		#Blue = Goal posistion
	pathfinding_grid_map[final_point[0]][final_point[1]] = 4 		#Blue = Goal posistion

	# Make image of pathfinding grid map
	ShowPathfindingPIL(pathfinding_grid_map)

	# Return point in mm
	return ( (final_point[0]-x_offset)*GRID_CELL_SIZE, (final_point[1]-y_offset)*GRID_CELL_SIZE ), False


##
	# The main loop of the software
def MainLoop():
	return_amount_main = 5
	new_samples_per_old_one = 500
	main_pose_list = [[0, 0, 0, 1]] * return_amount_main
	connected_robot = 2
	x_pose = 0
	y_pose = 0
	rot_pose = 0
	print("*** BEGINNING HUB SOFTWARE ***\n")

	# Get avalible ports and print in terminal
	print("Avalible ports:")
	os.system("python -m serial.tools.list_ports")

	# Get port name from user and open serial port
	port_name = input("\nSelect serial port for wireless communication:\n")
		#port_name = "/dev/cu.usbmodem14203"
	serial_com = BeginSerial(port_name, arg_timeout = 1)
	serial_com_sensor = BeginSerial(port_name, arg_timeout = 10)

	# Set robot pose as 0,0,0
	WriteCommand(serial_com, COMMAND_POSITION_SET_POSE, 0, 0, 0)
	time.sleep(0.1)
	
	# Read sensor values for beginning
	WriteCommand(serial_com, connected_robot, COMMAND_SENSOR_READ_ALL)
	sensor_data = GetDataFromUSB(serial_com_sensor, connected_robot, COMMAND_SENSOR_READ_ALL, False)
	#print(sensor_data)

	# Map with sensordata
	OccupancyGridMapping("Robot #1", (0, 0), 0, sensor_data[1:])
	
	# Show image with OpenCV (initialization)
	cv2.namedWindow('output image', cv2.WINDOW_NORMAL)
	cv2.namedWindow('pathfinding image', cv2.WINDOW_NORMAL)
	ShowGridMapOpenCV(grid_map_array)
	cv2.waitKey(0)

	print("Done initializing!")
	
	print("Beginning software loop")

	while(True):
		"""
		while(True):
			pressed_key = ReadKeyboard()
			if (pressed_key == "W"):
				WriteCommand(serial_com, connected_robot, COMMAND_DRIVE_FORWARD, 100, 100)
			if (pressed_key == "S"):
				WriteCommand(serial_com, connected_robot, COMMAND_DRIVE_BACKWARDS, 100, 100)
			if (pressed_key == "A"):
				WriteCommand(serial_com, connected_robot, COMMAND_DRIVE_ROT_LEFT, 100, 100)
			if (pressed_key == "D"):
				WriteCommand(serial_com, connected_robot, COMMAND_DRIVE_ROT_RIGHT, 100, 100)
			if (pressed_key == "1"):
				connected_robot = 1
				time.sleep(0.1)	
			if (pressed_key == "2"):
				connected_robot = 2
				time.sleep(0.1)	
			if (pressed_key == "P"):
				waypoint_point = SearchUnkownPoints(x_pose, y_pose)
				if (waypoint_point == (None, None) ):
					print("No unknown points!")
				else:
					ShowGridMapOpenCV(grid_map_array)
			if (pressed_key == "F"):
				break
		"""

		waypoint_point = SearchUnkownPoints(main_pose_list[0][0],main_pose_list[0][1])
		if (waypoint_point == None):
			print("No unknown points!")
			sleep(3600)
			exit()

		WriteCommand(serial_com, connected_robot, COMMAND_DRIVE_GOTO_COORD, 100, waypoint_point[0], waypoint_point[1])
		GetDataFromUSB(serial_com, connected_robot, COMMAND_DRIVE_GOTO_DONE, False)
		#cv2.waitKey(0) #Wait for input

		# Get sensor data
		WriteCommand(serial_com, connected_robot, COMMAND_SENSOR_READ_ALL)
		sensor_data = GetDataFromUSB(serial_com_sensor, connected_robot, COMMAND_SENSOR_READ_ALL, False)

		# Get position data
		WriteCommand(serial_com, connected_robot, COMMAND_POSITION_GET_POSE)
		position_data = GetDataFromUSB(serial_com, connected_robot, COMMAND_POSITION_GET_POSE, False)

		# Generate samples from old ones
		main_pose_list = GenerateSamples( (position_data[1],position_data[2],position_data[3]), new_samples_per_old_one, main_pose_list)

		# Correlate all samples with grid map
		for i in range(len(main_pose_list)):
			main_pose_list[i].append(PartFilterCalulateChance(CalculatePoseDistances( main_pose_list[i], sensor_data[1:])))

		# Update sample list
		
		main_pose_list = UpdateSamples(main_pose_list, return_amount_main)
		#input("Please press enter!")
		#x_pose = int(input("Give the X-coord of the robot:"))
		#y_pose = int(input("Give the Y-coord of the robot:"))
		#rot_pose = float(input("Give the rotation of the robot:"))
		#print("Given pose of robot: ("+str(x_pose)+","+str(y_pose)+","+str(rot_pose)+")")

		# Add to grid map with position- and sensor data
		OccupancyGridMapping("Robot #1", (main_pose_list[0][0], main_pose_list[0][1]), main_pose_list[0][2], sensor_data[1:])

		# Send pose back to robot
		WriteCommand(serial_com, connected_robot, COMMAND_POSITION_SET_POSE, main_pose_list[0][0], main_pose_list[0][1], round(main_pose_list[0][2]*1000))

		# Show grid map
		ShowGridMapOpenCV(grid_map_array)


MainLoop()


##
# @}

##
# @}

##
# @}
